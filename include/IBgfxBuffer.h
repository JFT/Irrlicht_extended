// Copyright (C) 2017 Julius Tilly
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h
#pragma once
#ifndef __IRR_I_BGFX_BUFFER_H_INCLUDED__
#define __IRR_I_BGFX_BUFFER_H_INCLUDED__
#include <stdint.h>
#include "IReferenceCounted.h"
#include "SVertexIndex.h"

namespace irr
{
namespace video
{
	//we use the same attribute sequence as bgfx::Attrib::enum
	enum E_BGFX_VERTEX_ATTRIBUTE
	{
		POSITION,  //!< a_position
		NORMAL,    //!< a_normal
		TANGENT,   //!< a_tangent
		BITANGENT, //!< a_bitangent
		COLOR0,    //!< a_color0
		COLOR1,    //!< a_color1
		INDICES,   //!< a_indices
		WEIGHT,    //!< a_weight
		TEXCOORD0, //!< a_texcoord0
		TEXCOORD1, //!< a_texcoord1
		TEXCOORD2, //!< a_texcoord2
		TEXCOORD3, //!< a_texcoord3
		TEXCOORD4, //!< a_texcoord4
		TEXCOORD5, //!< a_texcoord5
		TEXCOORD6, //!< a_texcoord6
		TEXCOORD7, //!< a_texcoord7
		COUNT
	};

	enum E_BGFX_ATTRIBUTE_TYPE
	{
		Uint8,  //!< Uint8
		Uint10, //!< Uint10, availability depends on: `BGFX_CAPS_VERTEX_ATTRIB_UINT10`.
		Int16,  //!< Int16
		Half,   //!< Half, availability depends on: `BGFX_CAPS_VERTEX_ATTRIB_HALF`.
		Float,  //!< Float
		Count
	};



	class IBgfxBuffer : public virtual IReferenceCounted
	{
	public:
		virtual ~IBgfxBuffer(){};


		virtual void addVertexAttribute(E_BGFX_VERTEX_ATTRIBUTE attrib, uint8_t numberOfElements, E_BGFX_ATTRIBUTE_TYPE attribType, bool normalize = false, bool asInt = false) = 0;

		virtual void processVertexAttributes() = 0;
		
		///@brief set the vertex buffer after setting all attributes with addVertexAttribute
		///you can safely delete the buffer after setting it - it will be copied
		///@param buffer array of vertices (pointer to the first element)
		///@param vertexCount number of vertices in the buffer
		///@param vertexSize size of on vertex
		///@param computeBuffer enable ComputeReadWrite to buffer
		virtual void setVertexBuffer(const void* buffer, size_t vertexCount, size_t vertexSize,bool computeBuffer) = 0;
		
		virtual void setVertexBuffer(const size_t bufferSize, bool computeBuffer) = 0;

		virtual void setIndexBuffer(const void* buffer, size_t indexCount, E_INDEX_TYPE idxType,bool computeBuffer) = 0;

		virtual void setIndexBuffer(const size_t bufferSize, E_INDEX_TYPE idxType, bool computeBuffer) = 0;

		virtual void destroyVertexBuffer() = 0;
		virtual void destroyIndexBuffer() = 0;

		virtual void destroyBuffers() = 0;

		virtual bool isDynamic() = 0;
	};


}//video
}//irr


#endif //BGFXBUFFER
