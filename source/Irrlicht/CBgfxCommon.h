// Copyright (C) 2016 Julius Tilly
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_BGFX_COMMON_H_INCLUDED__
#define __C_BGFX_COMMON_H_INCLUDED__

#include "IrrCompileConfig.h"

#include "irrList.h"
#if defined(_IRR_WINDOWS_API_)
	#define WIN32_LEAN_AND_MEAN
	#include <windows.h>
#endif // _IRR_WINDOWS_API
namespace irr
{
namespace video
{

	// Forward declarations.

	class CBgfxCoreFeature;

	template <class TBgfxDriver>
	class CBgfxCoreTexture;

	template <class TBgfxDriver, class TBgfxTexture>
	class CBgfxCoreRenderTarget;

	template <class TBgfxDriver, class TBgfxTexture>
	class CBgfxCoreCacheHandler;

	class CBgfxDriver;
	typedef CBgfxCoreTexture<CBgfxDriver> CBgfxTexture;
	typedef CBgfxCoreRenderTarget<CBgfxDriver, CBgfxTexture> CBgfxRenderTarget;
	class CBgfxCacheHandler;

	typedef core::list<CBgfxTexture*> CBgfxTextureList;

}
}

#endif
