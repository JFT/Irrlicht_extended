// Copyright (C) 2016 Julius Tilly
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef IRR_TO_BGFX_
#define IRR_TO_BGFX_

#include <bgfx/bgfx.h>
#include <bgfx/defines.h>
#include "S3DVertex.h"

namespace irr
{
namespace video
{
struct IrrToBgfx
{

	void init()
	{
		standardVDecl = new bgfx::VertexLayout();

		(*standardVDecl)
			.begin()
			.add(bgfx::Attrib::Position, 3, bgfx::AttribType::Float)
			.add(bgfx::Attrib::Normal, 3, bgfx::AttribType::Float)
			.add(bgfx::Attrib::Color0, 4, bgfx::AttribType::Uint8, true)
			.add(bgfx::Attrib::TexCoord0, 2, bgfx::AttribType::Float)
			.end();

		twoTCoordVDecl = new bgfx::VertexLayout();

		(*twoTCoordVDecl)
			.begin()
			.add(bgfx::Attrib::Position, 3, bgfx::AttribType::Float)
			.add(bgfx::Attrib::Normal, 3, bgfx::AttribType::Float)
			.add(bgfx::Attrib::Color0, 4, bgfx::AttribType::Uint8, true)
			.add(bgfx::Attrib::TexCoord0, 2, bgfx::AttribType::Float)
			.add(bgfx::Attrib::TexCoord1, 2, bgfx::AttribType::Float)
			.end();

		tangentsVDecl = new bgfx::VertexLayout();

		(*tangentsVDecl)
            .begin()
			.add(bgfx::Attrib::Position, 3, bgfx::AttribType::Float)
			.add(bgfx::Attrib::Normal, 3, bgfx::AttribType::Float)
			.add(bgfx::Attrib::Color0, 4, bgfx::AttribType::Uint8, true)
			.add(bgfx::Attrib::TexCoord0, 2, bgfx::AttribType::Float)
			.add(bgfx::Attrib::Tangent, 3, bgfx::AttribType::Float)
			.add(bgfx::Attrib::Bitangent, 3, bgfx::AttribType::Float) //Called Binormal in Irrlicht
			.end();

	};

	bgfx::VertexLayout* getBgfxVertexDeclFromIrrVertexType(E_VERTEX_TYPE type)
	{
		bgfx::VertexLayout* vDecl = nullptr;
		switch(type)
		{
		case E_VERTEX_TYPE::EVT_STANDARD:
			{
				return standardVDecl;
			}
			break;
		case E_VERTEX_TYPE::EVT_2TCOORDS:
			{
				return twoTCoordVDecl;

			}
			break;
		case E_VERTEX_TYPE::EVT_TANGENTS:
			{
				return tangentsVDecl;
			}
			break;
		default:
			{
				//DO nothing
			}
			break;
		}
		return vDecl;
	};

	static uint32_t getBgfxCompareFlag(E_BGFX_TEXTURE_COMPARE_FLAGS compareFlag)
	{
		switch (compareFlag)
		{
		case E_BGFX_TEXTURE_COMPARE_NONE:
			return 0;
		case E_BGFX_TEXTURE_COMPARE_LESS:
			return BGFX_SAMPLER_COMPARE_LESS;
		case E_BGFX_TEXTURE_COMPARE_LEQUAL:
			return BGFX_SAMPLER_COMPARE_LEQUAL;
		case E_BGFX_TEXTURE_COMPARE_EQUAL:
			return BGFX_SAMPLER_COMPARE_EQUAL;
		case E_BGFX_TEXTURE_COMPARE_GEQUAL:
			return BGFX_SAMPLER_COMPARE_GEQUAL;
		case E_BGFX_TEXTURE_COMPARE_GREATER:
			return BGFX_SAMPLER_COMPARE_GREATER;
		case E_BGFX_TEXTURE_COMPARE_NOTEQUAL:
			return BGFX_SAMPLER_COMPARE_NOTEQUAL;
		case E_BGFX_TEXTURE_COMPARE_NEVER:
			return BGFX_SAMPLER_COMPARE_NEVER;
		case E_BGFX_TEXTURE_COMPARE_ALWAYS:
			return BGFX_SAMPLER_COMPARE_ALWAYS;
		default:
			return 0;
		}

	};


	static bgfx::TextureFormat::Enum getBgfxTextureFormatFromIrrlichtColorFormat(ECOLOR_FORMAT format)
	{

		switch(format)
		{
		case ECF_A1R5G5B5:
			return bgfx::TextureFormat::RGB5A1;
			break;
		case ECF_R5G6B5:
			return bgfx::TextureFormat::R5G6B5;
			break;
		case ECF_R8G8B8:
			return bgfx::TextureFormat::RGB8;
			break;
		case ECF_A8R8G8B8:
			return bgfx::TextureFormat::RGBA8;
			break;
		case ECF_DXT1:
			return bgfx::TextureFormat::BC1;
			break;
		case ECF_DXT2:
		case ECF_DXT3:
			return bgfx::TextureFormat::BC2;
			break;
		case ECF_DXT4:
		case ECF_DXT5:
			return bgfx::TextureFormat::BC3;
			break;
		case ECF_PVRTC_RGB2:
			return bgfx::TextureFormat::PTC12;
			break;
		case ECF_PVRTC_ARGB2:
			return bgfx::TextureFormat::PTC12A;
			break;
		case ECF_PVRTC_RGB4:
			return bgfx::TextureFormat::PTC14;
			break;
		case ECF_PVRTC_ARGB4:
			return bgfx::TextureFormat::PTC14A;
			break;
		case ECF_PVRTC2_ARGB2:
			return bgfx::TextureFormat::PTC22;
			break;
		case ECF_PVRTC2_ARGB4:
			return bgfx::TextureFormat::PTC24;
			break;
		case ECF_ETC1:
			return bgfx::TextureFormat::ETC1;
			break;
		case ECF_ETC2_RGB:
			return bgfx::TextureFormat::ETC2;
			break;
		case ECF_ETC2_ARGB:
			return bgfx::TextureFormat::ETC2A;
			break;
		case ECF_R16F:
			return bgfx::TextureFormat::R16F;
			break;
		case ECF_G16R16F:
			return bgfx::TextureFormat::RG16F;
			break;
		case ECF_A16B16G16R16F:
			return bgfx::TextureFormat::RGBA16F;
			break;
		case ECF_R32F:
			return bgfx::TextureFormat::R32F;
			break;
		case ECF_G32R32F:
			return bgfx::TextureFormat::RG32F;
			break;
		case ECF_A32B32G32R32F:
			return bgfx::TextureFormat::RGBA32F;
			break;
		case ECF_R8:
			return bgfx::TextureFormat::R8;
			break;
		case ECF_R8G8:
			return bgfx::TextureFormat::RG8;
			break;
		case ECF_R16:
			return bgfx::TextureFormat::R16;
			break;
		case ECF_R16G16:
			return bgfx::TextureFormat::RG16;
			break;
		case ECF_D16:
			return bgfx::TextureFormat::D16;
			break;
		case ECF_D32:
			return bgfx::TextureFormat::D32;
			break;
		case ECF_D24S8:
			return bgfx::TextureFormat::D24S8;
			break;
		case ECF_UNKNOWN:
		default:
			return bgfx::TextureFormat::Unknown;
			break;
		}
	};

	/// get the Bgfx ClampingMode
	/// @param uvw 0-u, 1-v,2-w
	/// @param clamp irrlicht clampmode E_TEXTURE_CLAMP
	/// @return bgfxClampFlag
	static u32 getBgfxClampModeFromIrrlichtClampMode(u8 uvw,const u8 clamp)
	{
		u32 bgfxFlag = 0;
		switch(clamp)
		{
			case ETC_REPEAT:
				bgfxFlag = 0;
				break;

			case ETC_CLAMP:
				bgfxFlag |= (uvw == 0 ? BGFX_SAMPLER_U_CLAMP : (uvw == 1 ? BGFX_SAMPLER_V_CLAMP : (uvw == 2 ? BGFX_SAMPLER_W_CLAMP : 0 )) );
				break;

			case ETC_CLAMP_TO_EDGE:
				bgfxFlag |= (uvw == 0 ? BGFX_SAMPLER_U_CLAMP : (uvw == 1 ? BGFX_SAMPLER_V_CLAMP : (uvw == 2 ? BGFX_SAMPLER_W_CLAMP : 0 )) );
				break;

			case ETC_CLAMP_TO_BORDER:
				bgfxFlag |= (uvw == 0 ? BGFX_SAMPLER_U_BORDER : (uvw == 1 ? BGFX_SAMPLER_V_BORDER : (uvw == 2 ? BGFX_SAMPLER_W_BORDER : 0 )) );
				break;

			case ETC_MIRROR:
				bgfxFlag |= (uvw == 0 ? BGFX_SAMPLER_U_MIRROR : (uvw == 1 ? BGFX_SAMPLER_V_MIRROR : (uvw == 2 ? BGFX_SAMPLER_W_MIRROR : 0 )) );
				break;

			case ETC_MIRROR_CLAMP:
				bgfxFlag |= (uvw == 0 ? BGFX_SAMPLER_U_MIRROR | BGFX_SAMPLER_U_CLAMP :
							(uvw == 1 ? BGFX_SAMPLER_V_MIRROR | BGFX_SAMPLER_V_CLAMP :
							(uvw == 2 ? BGFX_SAMPLER_W_MIRROR | BGFX_SAMPLER_W_CLAMP: 0 )));
				break;

			case ETC_MIRROR_CLAMP_TO_EDGE:
				bgfxFlag |= (uvw == 0 ? BGFX_SAMPLER_U_MIRROR | BGFX_SAMPLER_U_CLAMP :
							(uvw == 1 ? BGFX_SAMPLER_V_MIRROR | BGFX_SAMPLER_V_CLAMP :
							(uvw == 2 ? BGFX_SAMPLER_W_MIRROR | BGFX_SAMPLER_W_CLAMP: 0 )));
				break;

			case ETC_MIRROR_CLAMP_TO_BORDER:
				bgfxFlag |= (uvw == 0 ? BGFX_SAMPLER_U_MIRROR | BGFX_SAMPLER_U_BORDER :
							(uvw == 1 ? BGFX_SAMPLER_V_MIRROR | BGFX_SAMPLER_V_BORDER :
							(uvw == 2 ? BGFX_SAMPLER_W_MIRROR | BGFX_SAMPLER_W_BORDER: 0 )));
				break;
			default:
				bgfxFlag = 0;
		}
		return bgfxFlag;
	};

	void destroy()
	{
		if(standardVDecl)
			delete standardVDecl;
		if(twoTCoordVDecl)
			delete twoTCoordVDecl;
		if(tangentsVDecl)
			delete tangentsVDecl;
	};

	bgfx::VertexLayout* standardVDecl;
	bgfx::VertexLayout* twoTCoordVDecl;
	bgfx::VertexLayout* tangentsVDecl;

};

} //namespace video
} //namespace irr
#endif
