// Copyright (C) 2016 Julius Tilly
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_BGFX_SHADER_MATERIAL_RENDERER_H_INCLUDED__
#define __C_BGFX_SHADER_MATERIAL_RENDERER_H_INCLUDED__

#include "IrrCompileConfig.h"

#ifdef _IRR_COMPILE_WITH_BGFX_

#include "IMaterialRenderer.h"
#include "CBgfxMaterialRenderer.h" // defines base material renderer class

#include "CBgfxCommon.h"

#include "bgfx/bgfx.h"

namespace irr
{
namespace video
{

class CBgfxDriver;
class IShaderConstantSetCallBack;

//! Class for using vertex and pixel shaders with BGFX
class CBgfxShaderMaterialRenderer : public CBgfxMaterialRenderer_BASE, public IMaterialRendererServices
{
public:

	//! Constructor
	CBgfxShaderMaterialRenderer(CBgfxDriver* driver,
		s32& outMaterialTypeNr, const void* vertexShaderProgram, const u32 vertexShaderProgramSize, const void* pixelShaderProgram, const u32 pixelShaderProgramSize,
		IShaderConstantSetCallBack* callback, E_MATERIAL_TYPE baseMaterial, s32 userData, const core::array<core::stringc> constantNames, const core::array<u32> constantSizes);

	//! Destructor
	virtual ~CBgfxShaderMaterialRenderer();

    virtual IVideoDriver* getVideoDriver() _IRR_OVERRIDE_;

	virtual void OnSetMaterial(const SMaterial& material, const SMaterial& lastMaterial,
		bool resetAllRenderstates, IMaterialRendererServices* services) _IRR_OVERRIDE_;

	virtual bool OnRender(IMaterialRendererServices* service, E_VERTEX_TYPE vtxtype) _IRR_OVERRIDE_;

	virtual void OnUnsetMaterial() _IRR_OVERRIDE_;

	//! Returns if the material is transparent.
	virtual bool isTransparent() const _IRR_OVERRIDE_;

    // implementations for the render services
	virtual void setBasicRenderStates(const SMaterial& material, const SMaterial& lastMaterial, bool resetAllRenderstates) _IRR_OVERRIDE_;
	virtual s32 getVertexShaderConstantID(const c8* name) _IRR_OVERRIDE_;
	virtual s32 getPixelShaderConstantID(const c8* name) _IRR_OVERRIDE_;
	virtual void setVertexShaderConstant(const f32* data, s32 startRegister, s32 constantAmount=1) _IRR_OVERRIDE_;
	virtual void setPixelShaderConstant(const f32* data, s32 startRegister, s32 constantAmount=1) _IRR_OVERRIDE_;
	virtual bool setVertexShaderConstant(s32 index, const f32* floats, int count) _IRR_OVERRIDE_;
	virtual bool setVertexShaderConstant(s32 index, const s32* ints, int count) _IRR_OVERRIDE_;
	virtual bool setPixelShaderConstant(s32 index, const f32* floats, int count) _IRR_OVERRIDE_;
	virtual bool setPixelShaderConstant(s32 index, const s32* ints, int count) _IRR_OVERRIDE_;

protected:

	//! constructor only for use by derived classes who want to
	//! create a fall back material for example.
	CBgfxShaderMaterialRenderer(CBgfxDriver* driver,
					IShaderConstantSetCallBack* callback,
					E_MATERIAL_TYPE baseMaterial, s32 userData=0);

	// must not be called more than once!
	void init(s32& outMaterialTypeNr, const void* vertexShaderProgram, const u32 vertexShaderProgramSize,
		const void* pixelShaderProgram, const u32 pixelShaderProgramSize, E_VERTEX_TYPE type);

	void createShaderUniforms(const core::array<core::stringc> constantNames, const core::array<u32> constantSizes);

	IShaderConstantSetCallBack* CallBack;

	bool Alpha;
	bool Blending;
	bool FixedBlending;
	bool AlphaTest;

	s32 UserData;

	// the shaders also have an array of the uniforms they use. This allows shaders to 'subscribe' to certain uniforms and for uniforms which are no longer needed to be deleted.
    core::array<core::stringc> UsedUniforms;
};


} // end namespace video
} // end namespace irr

#endif//#ifdef _IRR_COMPILE_WITH_BGFX_
#endif
