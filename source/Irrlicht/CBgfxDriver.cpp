// Copyright (C) 2002-2016 Julius Tilly/ Hannes Franke
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CBgfxDriver.h"
#include "os.h"
#include "CNullDriver.h"
#include <IContextManager.h>

#ifdef _IRR_COMPILE_WITH_BGFX_
#include <bgfx/platform.h>
#include "CBgfxCacheHandler.h"
#include "CBgfxMaterialRenderer.h"
#include "CBgfxShaderMaterialRenderer.h"
//#include "CBgfxGLSLMaterialRenderer.h"
#include "CBgfxNormalMapRenderer.h"
#include "CBgfxParallaxMapRenderer.h"

#include "CBgfxCoreTexture.h"
#include "CBgfxCoreRenderTarget.h"
#include "CBgfxBuffer.h"
#include "CBgfxComputeShader.h"

#include "CBgfxAndIrrlichtTypes.h"

#include "IAnimatedMeshSceneNode.h"


namespace irr
{
namespace video
{


// Static variables
const u16 CBgfxDriver::Quad2DIndices[6] = {0, 1, 2, 0, 2, 3};

CBgfxDriver::CBgfxDriver(const SIrrlichtCreationParameters& params, io::IFileSystem* io, IContextManager* contextManager)
    : CNullDriver(io, params.WindowSize), CacheHandler(0), CurrentRenderMode(ERM_NONE), ResetRenderStates(true),
	Transformation3DChanged(true), AntiAlias(params.AntiAlias), ColorFormat(ECF_R8G8B8), Params(params),
	ContextManager(contextManager),Feature(new CBgfxCoreFeature()),BgfxResetOptionFlags(0),
#if defined(_IRR_COMPILE_WITH_WINDOWS_DEVICE_)
	DeviceType(EIDT_WIN32)
#elif defined(_IRR_COMPILE_WITH_X11_DEVICE_)
	DeviceType(EIDT_X11)
#else
	DeviceType(EIDT_OSX)
#endif
{
#ifdef _DEBUG
	setDebugName("CBgfxDriver");
#endif
}

bool CBgfxDriver::initDriver()
{
	ContextManager->generateSurface();
	ContextManager->generateContext();
	ExposedData = ContextManager->getContext();
	ContextManager->activateContext(ExposedData);

	genericDriverInit();

	indexBufferQuad2DHandle = bgfx::createIndexBuffer(bgfx::makeRef(Quad2DIndices,sizeof(Quad2DIndices)),BGFX_BUFFER_NONE);

	return true;
}

//! destructor
CBgfxDriver::~CBgfxDriver()
{
	RequestedLights.clear();

	deleteMaterialRenders();

	CacheHandler->getTextureCache().clear();
	// I get a blue screen on my laptop, when I do not delete the
	// textures manually before releasing the dc. Oh how I love this.
	removeAllRenderTargets();
	deleteAllTextures();
	removeAllOcclusionQueries();
	removeAllHardwareBuffers();

	irrToBgfx.destroy();

    bgfx::shutdown();

	delete CacheHandler;

	if (ContextManager)
	{
		ContextManager->destroyContext();
		ContextManager->destroySurface();
		ContextManager->terminate();
		ContextManager->drop();
	}

    delete this->Feature;
}


// -----------------------------------------------------------------------
// METHODS
// -----------------------------------------------------------------------

bool CBgfxDriver::genericDriverInit()
{
	if (ContextManager)
		ContextManager->grab();

#ifdef _DEBUG
	 bgfx::renderFrame();
	 os::Printer::log("Running with no seperate Renderthread ",ELL_INFORMATION);
#endif

    //here we can select all kinds of renderer apis
	//TODO: add D3D12 and Vulkan..
	bool ret = false;
	bgfx::Init bgfxInitInfo;
	if(Params.DriverType == EDT_BGFX_OPENGL)
    {
		bgfxInitInfo.type = bgfx::RendererType::OpenGL;
    }
	else if(Params.DriverType == EDT_BGFX_D3D9)
	{
		bgfxInitInfo.type = bgfx::RendererType::Direct3D9;
	}
	else if(Params.DriverType == EDT_BGFX_D3D11)
	{
		bgfxInitInfo.type = bgfx::RendererType::Direct3D11;
	}
	else if(Params.DriverType == EDT_BGFX_METAL)
	{
		bgfxInitInfo.type = bgfx::RendererType::Metal;
	}
    else 
	{
        os::Printer::log("unknown bgfx driver type", irr::core::stringc(static_cast<uint32_t>(Params.DriverType)).c_str());
    	std::exit(1);
	}

	bgfx::Resolution bgfxResolution;
	bgfxResolution.width = Params.WindowSize.Width;
	bgfxResolution.height = Params.WindowSize.Height;
	
    BgfxResetOptionFlags = 0|((Params.AntiAlias>=16)? BGFX_RESET_MSAA_X16 : ((Params.AntiAlias>=8)? BGFX_RESET_MSAA_X8 : ((Params.AntiAlias>=4)? BGFX_RESET_MSAA_X4:
    						((Params.AntiAlias>=2)? BGFX_RESET_MSAA_X2 : 0))));
    BgfxResetOptionFlags |= 0 | Params.Vsync? BGFX_RESET_VSYNC : 0;

	bgfxResolution.reset = BgfxResetOptionFlags;
	bgfxInitInfo.resolution = bgfxResolution;

	bgfx::init(bgfxInitInfo);
    
//for debugging
#ifdef _DEBUG
    bgfx::setDebug(BGFX_DEBUG_STATS);
#endif
    
    bgfx::reset(Params.WindowSize.Width,Params.WindowSize.Height, BgfxResetOptionFlags);
    Name=L"BGFX ";
	Name.append(core::stringw(BGFX_API_VERSION));
	/*s32 pos=Name.findNext(L' ', 5);
	if (pos != -1)
		Name=Name.subString(0, pos);*/
	printVersion();
	// print renderer information
	const char* renderer = "BGFX";
    this->VendorName = core::stringc(bgfx::getCaps()->vendorId);
	const char* adapter = "Unknown";
	if (renderer)
	{
		os::Printer::log("Renderer", renderer, ELL_INFORMATION);
        os::Printer::log("Graphics Card Vendor", this->VendorName.c_str(), ELL_INFORMATION);
		os::Printer::log("Graphics Card", adapter, ELL_INFORMATION);
		//VendorName = reinterpret_cast<const c8*>(vendor);
	}

	u32 i;
	Feature->ColorAttachment = bgfx::getCaps()->limits.maxFBAttachments;
	Feature->MultipleRenderTarget = bgfx::getCaps()->limits.maxViews;
	Feature->BlendOperation = (bgfx::getCaps()->supported & BGFX_CAPS_BLEND_INDEPENDENT) ? true : false;
	Feature->TextureUnit = _IRR_MATERIAL_MAX_TEXTURES_; //TODO: here what is textureunit?? max set Textures per material or maximum textures from hardware
	Feature->MaxViews = bgfx::getCaps()->limits.maxViews;

	CacheHandler = new CBgfxCacheHandler(this);

	DriverAttributes->setAttribute("MaxTextures", (s32)Feature->TextureUnit);
	DriverAttributes->setAttribute("MaxSupportedTextures", (s32)Feature->TextureUnit);
	//DriverAttributes->setAttribute("MaxLights", MaxLights);
	//DriverAttributes->setAttribute("MaxAnisotropy", MaxAnisotropy);
	//DriverAttributes->setAttribute("MaxUserClipPlanes", MaxUserClipPlanes);
	//DriverAttributes->setAttribute("MaxAuxBuffers", MaxAuxBuffers);
	DriverAttributes->setAttribute("MaxMultipleRenderTargets", (s32)Feature->MultipleRenderTarget);
	//DriverAttributes->setAttribute("MaxIndices", (s32)MaxIndices);
	DriverAttributes->setAttribute("MaxTextureSize", static_cast<s32>(bgfx::getCaps()->limits.maxTextureSize));
	//DriverAttributes->setAttribute("MaxGeometryVerticesOut", (s32)MaxGeometryVerticesOut);
	//DriverAttributes->setAttribute("MaxTextureLODBias", MaxTextureLODBias);
	//DriverAttributes->setAttribute("Version", Version);
	//DriverAttributes->setAttribute("ShaderLanguageVersion", ShaderLanguageVersion);
	DriverAttributes->setAttribute("AntiAlias", AntiAlias);

	/*UserClipPlanes.reallocate(MaxUserClipPlanes);
	for (i=0; i<MaxUserClipPlanes; ++i)
		UserClipPlanes.push_back(SUserClipPlane());*/

	for (i=0; i<ETS_COUNT; ++i)
		setTransform(static_cast<E_TRANSFORMATION_STATE>(i), core::IdentityMatrix);

	setAmbientLight(SColorf(0.0f,0.0f,0.0f,0.0f));

	Params.HandleSRGB &= true;

	// Create built-in 2D quad for 2D rendering (both quads and lines).
	Quad2DVertices[0] = S3DVertex(core::vector3df(-1.0f, 1.0f, 0.0f), core::vector3df(0.0f, 0.0f, 0.0f), SColor(255,255,255,255), core::vector2df(0.0f, 1.0f));
	Quad2DVertices[1] = S3DVertex(core::vector3df(1.0f, 1.0f, 0.0f), core::vector3df(0.0f, 0.0f, 0.0f), SColor(255,255,255,255), core::vector2df(1.0f, 1.0f));
	Quad2DVertices[2] = S3DVertex(core::vector3df(1.0f, -1.0f, 0.0f), core::vector3df(0.0f, 0.0f, 0.0f), SColor(255,255,255,255), core::vector2df(1.0f, 0.0f));
	Quad2DVertices[3] = S3DVertex(core::vector3df(-1.0f, -1.0f, 0.0f), core::vector3df(0.0f, 0.0f, 0.0f), SColor(255,255,255,255), core::vector2df(0.0f, 0.0f));

	irrToBgfx.init();

	// create material renderers
	createMaterialRenderers();

	// set the renderstates
	setRenderStates3DMode();

	// set fog mode
	//setFog(FogColor, FogType, FogStart, FogEnd, FogDensity, PixelFog, RangeFog);

	// create matrix for flipping textures
	TextureFlipMatrix.buildTextureTransform(0.0f, core::vector2df(0,0), core::vector2df(0,1.0f), core::vector2df(1.0f,-1.0f));

	// We need to reset once more at the beginning of the first rendering.
	// This fixes problems with intermediate changes to the material during texture load.
	ResetRenderStates = true;

	bgfx::setViewMode(CacheHandler->get2DView(),bgfx::ViewMode::Sequential);

	return ret;
}

bool CBgfxDriver::isBgfxDriverType(const video::E_DRIVER_TYPE dType) const
{
    switch (dType) {
        case video::EDT_BGFX_OPENGL:
        case video::EDT_BGFX_METAL:
        case video::EDT_BGFX_D3D9:
        case video::EDT_BGFX_D3D11:
            return true;
        default:
            return false;
    }
}

bgfx::RendererType::Enum CBgfxDriver::getBgfxRenderType() const {
    switch (this->Params.DriverType) {
        case video::EDT_BGFX_OPENGL:
            return bgfx::RendererType::OpenGL;
        case video::EDT_BGFX_METAL:
            return bgfx::RendererType::Metal;
        case video::EDT_BGFX_D3D9:
            return bgfx::RendererType::Direct3D9;
        case video::EDT_BGFX_D3D11:
            return bgfx::RendererType::Direct3D11;
        default:
            os::Printer::log("Fatal Error: Couldnt get bgfx::RendererType for the irrlicht renderer type.", ELL_ERROR);
            return bgfx::RendererType::Noop;
    }
}


void CBgfxDriver::createMaterialRenderers()
{
    addAndDropMaterialRenderer(new CBgfxMaterialRenderer_SOLID(this));

    addAndDropMaterialRenderer(new CBgfxMaterialRenderer_SOLID_2_LAYER(this));

	// add the same renderer for all lightmap types
	CBgfxMaterialRenderer_LIGHTMAP* lmr = new CBgfxMaterialRenderer_LIGHTMAP(this);
	addMaterialRenderer(lmr); // for EMT_LIGHTMAP:
	addMaterialRenderer(lmr); // for EMT_LIGHTMAP_ADD:
	addMaterialRenderer(lmr); // for EMT_LIGHTMAP_M2:
	addMaterialRenderer(lmr); // for EMT_LIGHTMAP_M4:
	addMaterialRenderer(lmr); // for EMT_LIGHTMAP_LIGHTING:
	addMaterialRenderer(lmr); // for EMT_LIGHTMAP_LIGHTING_M2:
	addMaterialRenderer(lmr); // for EMT_LIGHTMAP_LIGHTING_M4:
	lmr->drop();

	// add remaining material renderer
	addAndDropMaterialRenderer(new CBgfxMaterialRenderer_DETAIL_MAP(this));
	addAndDropMaterialRenderer(new CBgfxMaterialRenderer_SPHERE_MAP(this));
	addAndDropMaterialRenderer(new CBgfxMaterialRenderer_REFLECTION_2_LAYER(this));
	addAndDropMaterialRenderer(new CBgfxMaterialRenderer_TRANSPARENT_ADD_COLOR(this));
	addAndDropMaterialRenderer(new CBgfxMaterialRenderer_TRANSPARENT_ALPHA_CHANNEL(this));
	addAndDropMaterialRenderer(new CBgfxMaterialRenderer_TRANSPARENT_ALPHA_CHANNEL_REF(this));
	addAndDropMaterialRenderer(new CBgfxMaterialRenderer_TRANSPARENT_VERTEX_ALPHA(this));
	addAndDropMaterialRenderer(new CBgfxMaterialRenderer_TRANSPARENT_REFLECTION_2_LAYER(this));

	// add normal map renderers
	s32 tmp = 0;
	addAndDropMaterialRenderer(new CBgfxNormalMapRenderer(this, tmp, EMT_SOLID));
	addAndDropMaterialRenderer(new CBgfxNormalMapRenderer(this, tmp, EMT_TRANSPARENT_ADD_COLOR));
	addAndDropMaterialRenderer(new CBgfxNormalMapRenderer(this, tmp, EMT_TRANSPARENT_VERTEX_ALPHA));

	// add parallax map renderers
	addAndDropMaterialRenderer(new CBgfxParallaxMapRenderer(this, tmp, EMT_SOLID));
	addAndDropMaterialRenderer(new CBgfxParallaxMapRenderer(this, tmp, EMT_TRANSPARENT_ADD_COLOR));
	addAndDropMaterialRenderer(new CBgfxParallaxMapRenderer(this, tmp, EMT_TRANSPARENT_VERTEX_ALPHA));

	// add basic 1 texture blending

	addAndDropMaterialRenderer(new CBgfxMaterialRenderer_ONETEXTURE_BLEND(this));
	//this is quick fix for testing only
	CBgfxMaterialRenderer_BASE* basicRenderer = new CBgfxMaterialRenderer_BASIC(this);
	BasicProgramHandle = basicRenderer->getProgramHandle();
	addAndDropMaterialRenderer(basicRenderer);

	//TODO: set the basicProgramhandle somewhere it makes sense
	StandardVertexProgramHandle = static_cast<CBgfxMaterialRenderer_BASE*>(MaterialRenderers[0].Renderer)->getProgramHandle();
}

bool CBgfxDriver::beginScene(u16 clearFlag, SColor clearColor, f32 clearDepth, u8 clearStencil, const SExposedVideoData& videoData, core::rect<s32>* sourceRect)
{
    this->inScene = true;
	//std::cerr << std::endl << std::endl << "CBgfxDriver::beginScene" << std::endl << std::endl << std::endl;
	CNullDriver::beginScene(clearFlag, clearColor, clearDepth, clearStencil, videoData, sourceRect);

	if (ContextManager)
		ContextManager->activateContext(videoData);

#ifdef _DEBUG
    // if compiled in debug mode bgfx renders its debug text after this driver is done rendering. This sets it's own textures and renderStates without them being changed in the CacheHandler. This might lead to the situation that a material thinks its texture is already set even though it has been replaced and it will use the texture set by the bgfx debug text.
    // -> reset all renderstates and textures if in debug mode to force them to be set again.
    this->ResetRenderStates = true;
    for (u32 i = 0; i < _IRR_MATERIAL_MAX_TEXTURES_; i++) {
        this->CacheHandler->getTextureCache().set(i, nullptr);
    }
#endif

//#if defined(_IRR_COMPILE_WITH_SDL_DEVICE_)
//	glFrontFace(GL_CW);
//#endif

	clearBuffers(clearFlag, clearColor, clearDepth, clearStencil);
	bgfx::touch(CacheHandler->get2DView());
	bgfx::touch(CacheHandler->getActiveView());

	bgfx::setViewRect(CacheHandler->get2DView(),0,0,ViewPort.getWidth(),ViewPort.getHeight());
	
	bgfx::setViewRect(CacheHandler->getActiveView(),0,0,ViewPort.getWidth(),ViewPort.getHeight());

	return true;
}

bool CBgfxDriver::endScene()
{
    //std::cerr << "CBgfxDriver::endScene() ";

	currFrame = bgfx::frame();
    //std::cerr << "frame = " << currFrame << std::endl;
	CNullDriver::endScene();

	bool status = false;

	//!HERE BGFX has a contextbuffer itself?? - delete
	if (ContextManager)
		status = ContextManager->swapBuffers();
	CacheHandler->updateQueuedObjects(currFrame);
//#ifdef _IRR_COMPILE_WITH_SDL_DEVICE_
//	SDL_GL_SwapBuffers();
//	status = true;
//#endif

	// todo: console device present

    this->inScene = false;
    
    // update time counters for dynamic hardware buffers and delete them if they haven't been used in a long time
	core::map<const scene::IMeshBuffer*,SHWBufferLink*>::ParentFirstIterator Iterator=HWBufferMap.getParentFirstIterator();
	for (;!Iterator.atEnd();Iterator++)
	{
		SHWBufferLink_bgfx* const Link = static_cast<SHWBufferLink_bgfx*>(Iterator.getNode()->getValue());
        // Link->printStats();  // useful for debugging the HWLinks
        Link->DestroyUnusedBuffers();
        Link->resetNextBuffersToUse();
	}
	///We set the NextRenderTargetView to 0 here instead of begin scene - so calls to setRenderTarget are managed correctly for next frame
	CacheHandler->setNextRenderTargetView(0);

	return status;
}

//! Returns the transformation set by setTransform
const core::matrix4& CBgfxDriver::getTransform(E_TRANSFORMATION_STATE state) const
{
    if (state == E_TRANSFORMATION_STATE::ETS_PROJECTION && this->ProjectionMatrixIsFlipped) {
        return this->unFlippedProjection;
    }
	return Matrices[state];
}

//! sets transformation
void CBgfxDriver::setTransform(E_TRANSFORMATION_STATE state, const core::matrix4& mat)
{
	Matrices[state] = mat;
	Transformation3DChanged = true;

	switch (state)
	{
	case ETS_WORLD:
		{
			//World transform for one submit
			bgfx::setTransform(mat.pointer());
		}
		break;
	case ETS_VIEW:
		{
			// now the real model-view matrix
			bgfx::setViewTransform(CacheHandler->getActiveView(),Matrices[ETS_VIEW].pointer(), Matrices[ETS_PROJECTION].pointer());
			bgfx::setViewTransform(CacheHandler->get2DView(),Matrices[ETS_VIEW].pointer(), Matrices[ETS_PROJECTION].pointer());
		}
break;
	case ETS_PROJECTION:
		{
            if (this->getCurrentRenderTarget() != nullptr && this->getDriverType() == video::E_DRIVER_TYPE::EDT_BGFX_OPENGL) {
                // opengl renders to a framebuffer vertically flipped. Solution -> vertically flip the camera.
                // this is done the same way openGL would if using the 'glScale' command
                this->unFlippedProjection = mat;
                Matrices[ETS_PROJECTION][1] *= -1.0f;
                Matrices[ETS_PROJECTION][5] *= -1.0f;
                Matrices[ETS_PROJECTION][9] *= -1.0f;
                Matrices[ETS_PROJECTION][13] *= -1.0f;
                this->ProjectionMatrixIsFlipped = true;
            }
            else {
                this->ProjectionMatrixIsFlipped = false;
            }
            bgfx::setViewTransform(CacheHandler->getActiveView(),Matrices[ETS_VIEW].pointer(), Matrices[ETS_PROJECTION].pointer());
            bgfx::setViewTransform(CacheHandler->get2DView(),Matrices[ETS_VIEW].pointer(), Matrices[ETS_PROJECTION].pointer());
        }
			break;
	default:
		break;
	}

}

bool CBgfxDriver::updateVertexHardwareBuffer(SHWBufferLink_bgfx *HWBuffer)
{
	if (!HWBuffer)
		return false;

    const scene::IMeshBuffer* mb = HWBuffer->MeshBuffer;
	const void* vertices=mb->getVertices();
	const u32 vertexCount=mb->getVertexCount();
	const E_VERTEX_TYPE vType=mb->getVertexType();
	const u32 vertexSize = getVertexPitchFromType(vType);

    switch (HWBuffer->Mapped_Vertex) {
        case scene::EHM_STATIC:
            {
                // static buffers only need to be set once
                if (!bgfx::isValid(HWBuffer->staticVHandle))
                {
                    // this should be the only time this buffer needs to be set!
                    HWBuffer->staticVHandle = bgfx::createVertexBuffer(bgfx::makeRef(vertices, vertexCount * vertexSize),
                            *irrToBgfx.getBgfxVertexDeclFromIrrVertexType(vType));
                    if (!bgfx::isValid(HWBuffer->staticVHandle)) {
                        return false;
                    }
                }
            }
        case scene::EHM_STREAM:
            if (vertexCount == bgfx::getAvailTransientVertexBuffer(vertexCount, *irrToBgfx.getBgfxVertexDeclFromIrrVertexType(vType))) {
                // this code is only called if the vertex buffer has changed -> create a new one
                bgfx::allocTransientVertexBuffer(&(HWBuffer->vTransient), vertexCount, *irrToBgfx.getBgfxVertexDeclFromIrrVertexType(vType));
                memcpy(HWBuffer->vTransient.data, vertices, vertexCount * vertexSize);

                //std::cerr << "new vTransientBuffer: " << static_cast<int>(HWBuffer->vTransient.handle.idx) << std::endl;

                if (!bgfx::isValid(HWBuffer->vTransient.handle)) {
                    return false;
                }
                break;
            }
        case scene::EHM_DYNAMIC:
            if (HWBuffer->vDynHandles.size() <= HWBuffer->nextVDynHandleToUse) {
                HWBuffer->vDynHandles.push_back(BGFX_INVALID_HANDLE);
                HWBuffer->vDynHandlesLastUsed.push_back(0);
            }
            // this code is only called if the vertex buffer has changed -> create a new one
            if (!bgfx::isValid(HWBuffer->vDynHandles[HWBuffer->nextVDynHandleToUse]))
            {
                HWBuffer->vDynHandles[HWBuffer->nextVDynHandleToUse] = bgfx::createDynamicVertexBuffer(vertexCount, *irrToBgfx.getBgfxVertexDeclFromIrrVertexType(vType), BGFX_BUFFER_ALLOW_RESIZE);
                //std::cerr << "new vDynBuffer: " << static_cast<int>(HWBuffer->vDynHandles[HWBuffer->nextVDynHandleToUse].idx) << std::endl;
            }
            bgfx::update(HWBuffer->vDynHandles[HWBuffer->nextVDynHandleToUse], 0, bgfx::copy(vertices, vertexCount * vertexSize));
			
            if (!bgfx::isValid(HWBuffer->vDynHandles[HWBuffer->nextVDynHandleToUse])) {
                return false;
            }
            // the next time this function is called a new dynamic vertex buffer needs to be used -> up the counter
            HWBuffer->nextVDynHandleToUse++;
            break;
    }

    return true;

}

bool CBgfxDriver::updateIndexHardwareBuffer(SHWBufferLink_bgfx *HWBuffer)
{
	if (!HWBuffer)
			return false;

	const scene::IMeshBuffer* mb = HWBuffer->MeshBuffer;

	const void* indices=mb->getIndices();
	u32 indexCount= mb->getIndexCount();

	if(indexCount <= 0)
	{
		return true;
	}

	u32 indexSize;
	switch (mb->getIndexType())
	{
		case EIT_16BIT:
		{
			indexSize=sizeof(u16);
			break;
		}
		case EIT_32BIT:
		{
			indexSize=sizeof(u32);
			break;
		}
		default:
		{
			return false;
		}
	}

    // this part completely analog to the hardware vertex buffer
    switch (HWBuffer->Mapped_Index) {
        case scene::EHM_STATIC:
            {
                if (!bgfx::isValid(HWBuffer->staticIHandle))
                {
                    HWBuffer->staticIHandle = bgfx::createIndexBuffer(bgfx::makeRef(indices, indexCount * indexSize));
                    if (!bgfx::isValid(HWBuffer->staticIHandle)) {
                        return false;
                    }
                }
                break;
            }
        case scene::EHM_STREAM:
#ifdef _DEBUG
            if (mb->getIndexType() != EIT_16BIT) {
                os::Printer::log("the bgfx driver only supports 16 bit EHM_STREAM Index-HardwareBuffers.", ELL_WARNING);
            }
#endif
            // bgfx transient buffers only support 16 bit indices
            // if not 16 bit indices or not enough transient bufferspace -> drop-through to dynamic buffer
            if (mb->getIndexType() == EIT_16BIT && (indexCount == bgfx::getAvailTransientIndexBuffer(indexCount))) {
                // this code is only called if the vertex buffer has changed -> create a new one
                bgfx::allocTransientIndexBuffer(&(HWBuffer->iTransient), indexCount);
                memcpy(HWBuffer->iTransient.data, indices, indexCount * indexSize);

                //std::cerr << "new iTransientBuffer: " << static_cast<int>(HWBuffer->iTransient.handle.idx) << std::endl;

                if (!bgfx::isValid(HWBuffer->iTransient.handle)) {
                    return false;
                }
                break;
            }
        case scene::EHM_DYNAMIC:
            if (HWBuffer->iDynHandles.size() <= HWBuffer->nextIDynHandleToUse) {
                HWBuffer->iDynHandles.push_back(BGFX_INVALID_HANDLE);
                HWBuffer->iDynHandlesLastUsed.push_back(0);
            }
            if (!bgfx::isValid(HWBuffer->iDynHandles[HWBuffer->nextIDynHandleToUse]))
            {
                HWBuffer->iDynHandles[HWBuffer->nextIDynHandleToUse] = bgfx::createDynamicIndexBuffer(indexCount, BGFX_BUFFER_ALLOW_RESIZE);
            }
            bgfx::update(HWBuffer->iDynHandles[HWBuffer->nextIDynHandleToUse], 0, bgfx::copy(indices, indexCount * indexSize));

            if (!bgfx::isValid(HWBuffer->iDynHandles[HWBuffer->nextIDynHandleToUse])) {
                return false;
            }

            HWBuffer->nextIDynHandleToUse++;
            break;
    }

	return true;

}

//! updates hardware buffer if needed
bool CBgfxDriver::updateHardwareBuffer(SHWBufferLink *HWBuffer)
{
	if (!HWBuffer)
		return false;

    SHWBufferLink_bgfx* HWBuffer_bgfx = static_cast<SHWBufferLink_bgfx*>(HWBuffer);

	if (HWBuffer->Mapped_Vertex!=scene::EHM_NEVER)
	{
        if (HWBuffer_bgfx->vBufferNeedsUpdate(HWBuffer->MeshBuffer->getChangedID_Vertex()))
		{
			HWBuffer->ChangedID_Vertex = HWBuffer->MeshBuffer->getChangedID_Vertex();

			if (!updateVertexHardwareBuffer(HWBuffer_bgfx))
				return false;
		}
	}

	if (HWBuffer->Mapped_Index!=scene::EHM_NEVER)
	{
        if (HWBuffer_bgfx->iBufferNeedsUpdate(HWBuffer->MeshBuffer->getChangedID_Index()))
		{
			HWBuffer->ChangedID_Index = HWBuffer->MeshBuffer->getChangedID_Index();

			if (!updateIndexHardwareBuffer(HWBuffer_bgfx))
				return false;
		}
	}

	return true;
}

//! Create hardware buffer from meshbuffer
CBgfxDriver::SHWBufferLink *CBgfxDriver::createHardwareBuffer(const scene::IMeshBuffer* mb)
{

	if (!mb || (mb->getHardwareMappingHint_Index()==scene::EHM_NEVER && mb->getHardwareMappingHint_Vertex()==scene::EHM_NEVER))
		return nullptr;

	SHWBufferLink_bgfx *HWBuffer=new SHWBufferLink_bgfx(mb);

	//add to map
	HWBufferMap.insert(HWBuffer->MeshBuffer, HWBuffer);

	HWBuffer->ChangedID_Vertex=HWBuffer->MeshBuffer->getChangedID_Vertex() - 1;  // force filling of the buffer (in updateHardwareBuffer) by initializing the ID one off
	HWBuffer->ChangedID_Index=HWBuffer->MeshBuffer->getChangedID_Index() - 1;
	HWBuffer->Mapped_Vertex=mb->getHardwareMappingHint_Vertex();
	HWBuffer->Mapped_Index=mb->getHardwareMappingHint_Index();
	HWBuffer->LastUsed=0;

	if (!updateHardwareBuffer(HWBuffer))
	{
		deleteHardwareBuffer(HWBuffer);
		return nullptr;
	}

	return HWBuffer;
}


void CBgfxDriver::deleteHardwareBuffer(SHWBufferLink *_HWBuffer)
{
	if (!_HWBuffer)
		return;

	SHWBufferLink_bgfx *HWBuffer=(SHWBufferLink_bgfx*)_HWBuffer;

	if (bgfx::isValid(HWBuffer->staticVHandle))
	{
		bgfx::destroy(HWBuffer->staticVHandle);
		HWBuffer->staticVHandle = BGFX_INVALID_HANDLE;
	}

    for (u32 i = 0; i < HWBuffer->vDynHandles.size(); i++) {
        if (bgfx::isValid(HWBuffer->vDynHandles[i])) {
		    bgfx::destroy(HWBuffer->vDynHandles[i]);
    		HWBuffer->vDynHandles[i] = BGFX_INVALID_HANDLE;
        }
	}

	if (bgfx::isValid(HWBuffer->staticIHandle))
	{
		bgfx::destroy(HWBuffer->staticIHandle);
		HWBuffer->staticIHandle = BGFX_INVALID_HANDLE;
	}
    for (u32 i = 0; i < HWBuffer->iDynHandles.size(); i++) {
        if (bgfx::isValid(HWBuffer->iDynHandles[i])) {
		    bgfx::destroy(HWBuffer->iDynHandles[i]);
    		HWBuffer->iDynHandles[i] = BGFX_INVALID_HANDLE;
        }
	}

	CNullDriver::deleteHardwareBuffer(_HWBuffer);
}

//TODO: add the transientbuffer to the mix
//! Draw hardware buffer
void CBgfxDriver::drawHardwareBuffer(SHWBufferLink *_HWBuffer)
{
	if (!_HWBuffer)
		return;

	if (!updateHardwareBuffer(_HWBuffer)) {
        os::Printer::log("Error updating hardware buffers!", ELL_ERROR);
    }
	_HWBuffer->LastUsed=0; //reset count

	SHWBufferLink_bgfx *HWBuffer=(SHWBufferLink_bgfx*)_HWBuffer;

	const scene::IMeshBuffer* mb = HWBuffer->MeshBuffer;
	const void *vertices = mb->getVertices();
	const void *indexList = mb->getIndices();
	const u32 indexCount = mb->getIndexCount();
	const u32 vertexCount = mb->getVertexCount();
	if (indexCount <= 0 || vertexCount <= 0)
	{
		return;
	}

    switch (HWBuffer->Mapped_Vertex) {
        case scene::EHM_NEVER:
            break;
        case scene::EHM_STATIC:
            if (bgfx::isValid(HWBuffer->staticVHandle))
            {
                bgfx::setVertexBuffer(0,HWBuffer->staticVHandle, 0, vertexCount);
                HWBuffer->staticVHandleLastUsed = 0;
                vertices = nullptr;
            }
            break;
        case scene::EHM_STREAM:
            if (bgfx::isValid(HWBuffer->vTransient.handle)) {
                bgfx::setVertexBuffer(0,&(HWBuffer->vTransient), 0, vertexCount);
                vertices = nullptr;
            }
            break;
        case scene::EHM_DYNAMIC:
            u32 drawIndex = HWBuffer->nextVDynHandleToUse - 1;
            // corner case: last drawing might have been to the last index and mesh hasn't changed since last frame (when nextXHandleToUse gets reset to 0) ->
            // the correct data is at the last handle in the array
            if (HWBuffer->nextVDynHandleToUse == 0) {
                drawIndex = HWBuffer->vDynHandles.size() - 1;
            }
            if (bgfx::isValid(HWBuffer->vDynHandles[drawIndex])) {
                bgfx::setVertexBuffer(0,HWBuffer->vDynHandles[drawIndex], 0, vertexCount);
                HWBuffer->vDynHandlesLastUsed[drawIndex] = 0;
                vertices = nullptr;
            }
            break;
    }

    switch (HWBuffer->Mapped_Index) {
        case scene::EHM_NEVER:
            break;
        case scene::EHM_STATIC:
            if (bgfx::isValid(HWBuffer->staticIHandle))
            {
                bgfx::setIndexBuffer(HWBuffer->staticIHandle, 0, indexCount);
                HWBuffer->staticIHandleLastUsed = 0;
                indexList = nullptr;
            }
            break;
        case scene::EHM_STREAM:
            if (bgfx::isValid(HWBuffer->iTransient.handle)) {
                bgfx::setIndexBuffer(&(HWBuffer->iTransient), 0, indexCount);
                indexList = nullptr;
            }
            break;
        case scene::EHM_DYNAMIC:
            u32 drawIndex = HWBuffer->nextIDynHandleToUse - 1;
            if (HWBuffer->nextIDynHandleToUse == 0) {
                drawIndex = HWBuffer->iDynHandles.size() - 1;
            }
            if (bgfx::isValid(HWBuffer->iDynHandles[drawIndex])) {
                bgfx::setIndexBuffer(HWBuffer->iDynHandles[drawIndex], 0, indexCount);
                HWBuffer->iDynHandlesLastUsed[drawIndex] = 0;
                indexList = nullptr;
            }
            break;
    }

	//Need to check if that is working
	drawVertexPrimitiveList(vertices, mb->getVertexCount(), indexList, mb->getIndexCount()/3, mb->getVertexType(), scene::EPT_TRIANGLES, mb->getIndexType());
}

//! Create occlusion query.
/** Use node for identification and mesh for occlusion test. */
void CBgfxDriver::addOcclusionQuery(scene::ISceneNode* node,const scene::IMesh* mesh)
{
	if (!node)
		return;
	if (!mesh)
	{
		if ((node->getType() != scene::ESNT_MESH) && (node->getType() != scene::ESNT_ANIMATED_MESH))
			return;
		else if (node->getType() == scene::ESNT_MESH)
			mesh = static_cast<scene::IMeshSceneNode*>(node)->getMesh();
		else
			mesh = static_cast<scene::IAnimatedMeshSceneNode*>(node)->getMesh()->getMesh(0);
		if (!mesh)
			return;
	}

	//search for query
	s32 index = OcclusionQueries.linear_search(SOccQuery(node));
	if (index != -1)
	{
		if (OcclusionQueries[index].Mesh != mesh)
		{
			OcclusionQueries[index].Mesh->drop();
			OcclusionQueries[index].Mesh = mesh;
			mesh->grab();
		}
	}
	else
	{
		index = OcclusionQueries.size();
		OcclusionQueries.push_back(SOccQuery(node, mesh));
		node->setAutomaticCulling(node->getAutomaticCulling() | scene::EAC_OCC_QUERY);
		bgfx::OcclusionQueryHandle hnd = bgfx::createOcclusionQuery();
		OcclusionQueries[index].UID = hnd.idx;
	}

}

//! Remove occlusion query.
void CBgfxDriver::removeOcclusionQuery(scene::ISceneNode* node)
{
	const s32 index = OcclusionQueries.linear_search(SOccQuery(node));
	if (index != -1)
	{
		if (OcclusionQueries[index].UID != bgfx::kInvalidHandle)
		{
			bgfx::OcclusionQueryHandle hnd;
			hnd.idx = OcclusionQueries[index].UID;
			bgfx::destroy(hnd);
		}
		node->setAutomaticCulling(node->getAutomaticCulling() & ~scene::EAC_OCC_QUERY);
		OcclusionQueries.erase(index);
	}
}

//! Run occlusion query. Draws mesh stored in query.
/** If the mesh shall not be rendered visible, use
overrideMaterial to disable the color and depth buffer. */
void CBgfxDriver::runOcclusionQuery(scene::ISceneNode* node, bool visible)
{
	if(!node)
		return;
	s32 index = OcclusionQueries.linear_search(SOccQuery(node));
	if (index==-1)
		return;
	OcclusionQueries[index].Run=0;
	if (!visible)
	{
		SMaterial mat;
		mat.Lighting=false;
		mat.AntiAliasing=0;
		mat.ColorMask=ECP_NONE;
		mat.GouraudShading=false;
		mat.ZWriteEnable=false;
		setMaterial(mat);
	}
	setTransform(video::ETS_WORLD, node->getAbsoluteTransformation());
	const scene::IMesh* mesh = OcclusionQueries[index].Mesh;
	for (u32 i=0; i<mesh->getMeshBufferCount(); ++i)
	{
		if (visible)
			setMaterial(mesh->getMeshBuffer(i)->getMaterial());
		drawMeshBuffer(mesh->getMeshBuffer(i));
	}
	if (OcclusionQueries[index].UID != bgfx::kInvalidHandle)
	{
		bgfx::OcclusionQueryHandle hnd;
		hnd.idx = OcclusionQueries[index].UID;
		//bgfx::setCondition(hnd, true); //-> this should be used at the actual rendering call
		bgfx::setState(0| BGFX_STATE_DEPTH_TEST_LEQUAL| BGFX_STATE_CULL_CW);
		bgfx::submit(CacheHandler->getActiveView(),BasicProgramHandle, hnd); //need to find the right spot to use this..
	}
	bgfx::setState(BGFX_STATE_DEFAULT);

}

//! Update occlusion query. Retrieves results from GPU.
/** If the query shall not block, set the flag to false.
Update might not occur in this case, though */
void CBgfxDriver::updateOcclusionQuery(scene::ISceneNode* node, bool block)
{
	const s32 index = OcclusionQueries.linear_search(SOccQuery(node));
	if (index != -1)
	{
		// not yet started
		if (OcclusionQueries[index].Run==u32(~0))
			return;

		if(OcclusionQueries[index].UID != bgfx::kInvalidHandle)
		{
			bgfx::OcclusionQueryHandle hnd;
			hnd.idx = OcclusionQueries[index].UID;
			OcclusionQueries[index].Result = bgfx::getResult(hnd);
		}
	}
}

//! Return query result.
/** Return value is bgfx::OcclusionQueryResult
 * Invisible = 0,  < Query failed test. -> object not visible
 *	Visible = 1,   < Query passed test.
 * 	NoResult = 2,  < Query result is not available yet.
*/
u32 CBgfxDriver::getOcclusionQueryResult(scene::ISceneNode* node) const
{
	const s32 index = OcclusionQueries.linear_search(SOccQuery(node));
	if (index != -1)
		return OcclusionQueries[index].Result;
	else
	{
		return bgfx::OcclusionQueryResult::NoResult;
	}
}

//! Create render target.
IRenderTarget* CBgfxDriver::addRenderTarget()
{
	CBgfxRenderTarget* renderTarget = new CBgfxRenderTarget(this);
	RenderTargets.push_back(renderTarget);

	return renderTarget;
}

//! Remove render target.
void CBgfxDriver::removeRenderTarget(IRenderTarget* renderTarget)
{
	//handling the deprecated setRendertarget(Itexture*...) case
	auto tex = renderTarget->getTexture()[0];
	auto found = textureToRT.find(tex);
	if(found != textureToRT.end())
	{
		unusedRTBufferIdx.push(found->second);
		textureToRT.erase(found);
	}

	CNullDriver::removeRenderTarget(renderTarget);

}

//! Remove all render targets.
void CBgfxDriver::removeAllRenderTargets()
{
	//handling deprecated setRenderTarget(Itexture*...) case
	textureToRT.clear();
	rtInfoBuffer.clear();
	while(!unusedRTBufferIdx.empty())
		unusedRTBufferIdx.pop();

	CNullDriver::removeAllRenderTargets();
}

// small helper function to create vertex buffer object adress offsets
//static inline u8* buffer_offset(const long offset)
//{
//	return ((u8*)0 + offset);
//}

//! set the Active View
void CBgfxDriver::setActiveView(s32 viewID, const bool sequential)
{
    if (viewID < 0) {
        viewID = bgfx::getCaps()->limits.maxViews + viewID;
    }
    if (viewID < 0 || viewID >= bgfx::getCaps()->limits.maxViews) {
        os::Printer::log("Error: setActiveView: invalid viewID. Make sure viewID is in the range [-255, 255]", ELL_ERROR);
        return;
    }
    s32 nextRenderTargetView = static_cast<s32>(CacheHandler->getNextRenderTargetView());
    if (viewID <= nextRenderTargetView - 1) { // currentRenderTargetView == nextRenderTargetView - 1
        //std::cerr << "viewID = " << viewID << " but nextRenderTargetView = " << nextRenderTargetView << std::endl;
        os::Printer::log("Error: setActiveView: selected view is already used by a RenderTarget!", ELL_ERROR);
        return;
    }

    CacheHandler->setActiveView(viewID);
    // bgfx has a viewPort per view but this driver has only one viewPort. This call sets the bgfx viewport for the new active view to the one that was used before.
    this->setViewPort(this->getViewPort());
	if (sequential)
	{
		bgfx::setViewMode(viewID, bgfx::ViewMode::Sequential);
	}
	
	bgfx::touch(viewID);
}

//TODO: when we know that we are manipulating the buffers -> use dynamic buffers instead of static ones!!
//! draws a vertex primitive list
void CBgfxDriver::drawVertexPrimitiveList(const void* vertices, u32 vertexCount,
		const void* indexList, u32 primitiveCount,
		E_VERTEX_TYPE vType, scene::E_PRIMITIVE_TYPE pType, E_INDEX_TYPE iType)
{
	static bool firstDraw = true;
	static uint16_t lastDrawView = 0;

	if (!primitiveCount || !vertexCount)
		return;

	if (!checkPrimitiveCount(primitiveCount))
		return;

	CNullDriver::drawVertexPrimitiveList(vertices, vertexCount, indexList, primitiveCount, vType, pType, iType);


	// draw everything
	setRenderStates3DMode();

	const u32 vertexSize = getVertexPitchFromType(vType);
	bgfx::VertexBufferHandle vbh;
	bgfx::IndexBufferHandle ibh;
	const u32 indexSize = iType==EIT_16BIT ? sizeof(u16) : sizeof(u32);
	uint64_t tempStateFlag = CacheHandler->getBgfxStateFlag(CacheHandler->getActiveView());
	uint32_t indexCount = 0;
	if (vertices)
	{
		//TODO:
		//find the right type for the buffer -> are the vertices send to the driver static or do they only exist temporarily
		//this is probably slow
		vbh = bgfx::createVertexBuffer(bgfx::copy(vertices, vertexCount*vertexSize),*irrToBgfx.getBgfxVertexDeclFromIrrVertexType(vType),BGFX_BUFFER_NONE);
		if(!bgfx::isValid(vbh))
		{
			std::exit(33);
		}
		bgfx::setVertexBuffer(0,vbh, 0, vertexCount);

	}
	switch (pType)
	{
		case scene::EPT_POINTS:
		case scene::EPT_POINT_SPRITES:
			tempStateFlag |= BGFX_STATE_PT_POINTS;
			indexCount = primitiveCount;
			break;
		case scene::EPT_LINE_STRIP:
			tempStateFlag |= BGFX_STATE_PT_LINESTRIP;
			indexCount = primitiveCount + 1;
			break;
		case scene::EPT_LINE_LOOP:
			tempStateFlag |= BGFX_STATE_PT_LINES;
			indexCount = primitiveCount;
			break;
		case scene::EPT_LINES:
			tempStateFlag |= BGFX_STATE_PT_LINES;
			indexCount = primitiveCount * 2;
			break;
		case scene::EPT_TRIANGLE_STRIP:
			tempStateFlag |= BGFX_STATE_PT_TRISTRIP;
			indexCount = primitiveCount + 2;
			break;
		case scene::EPT_TRIANGLE_FAN: //Is deprecated won't work here anymore
			//bgfx::setState(BGFX_STATE_DEFAULT);
			indexCount = primitiveCount + 2;
			break;
		case scene::EPT_TRIANGLES:
			//bgfx::setState(BGFX_STATE_DEFAULT); // TODO: go through flags comparing the settings
			indexCount = primitiveCount * 3;
			break;
		case scene::EPT_QUAD_STRIP:
			//bgfx::setState(BGFX_STATE_DEFAULT); // TODO: go through flags comparing the settings
			indexCount = primitiveCount * 2 + 2;
			break;
		case scene::EPT_QUADS:
			//bgfx::setState(BGFX_STATE_DEFAULT); // TODO: go through flags comparing the settings
			indexCount = primitiveCount*4;
			break;
		case scene::EPT_POLYGON:
			//bgfx::setState(BGFX_STATE_DEFAULT); // TODO: go through flags comparing the settings
			indexCount = primitiveCount;
			break;
	}

	if (indexList)
	{
		ibh = bgfx::createIndexBuffer(bgfx::copy(indexList, indexCount*indexSize),
									  iType == EIT_16BIT ? BGFX_BUFFER_NONE : BGFX_BUFFER_INDEX32);
		
		bgfx::setIndexBuffer(ibh, 0, indexCount);
	}
		
    bgfx::setState(tempStateFlag);

	switch (vType)
	{
		case EVT_STANDARD:
			//Submit to activeView for rendering
			bgfx::submit(CacheHandler->getActiveView(),static_cast<CBgfxMaterialRenderer_BASE*>(MaterialRenderers[Material.MaterialType].Renderer)->getProgramHandle());
			break;
		case EVT_2TCOORDS:
			bgfx::submit(CacheHandler->getActiveView(), static_cast<CBgfxMaterialRenderer_BASE*>(MaterialRenderers[Material.MaterialType].Renderer)->getProgramHandle());
			break;
		case EVT_TANGENTS:
			bgfx::submit(CacheHandler->getActiveView(), static_cast<CBgfxMaterialRenderer_BASE*>(MaterialRenderers[Material.MaterialType].Renderer)->getProgramHandle());
			break;
	}

	if (firstDraw || lastDrawView != CacheHandler->getActiveView())
	{
		firstDraw = false;
		lastDrawView = CacheHandler->getActiveView();
	}

	if(vertices)
		bgfx::destroy(vbh);
	if(indexList)
		bgfx::destroy(ibh);
}

void CBgfxDriver::renderCall()
{

	if (ResetRenderStates || LastMaterial != Material)
	{
		// unset old material

		if (LastMaterial.MaterialType != Material.MaterialType &&
			static_cast<u32>(LastMaterial.MaterialType) < MaterialRenderers.size())
			MaterialRenderers[LastMaterial.MaterialType].Renderer->OnUnsetMaterial();

		// set new material.
		if (static_cast<u32>(Material.MaterialType) < MaterialRenderers.size())
			MaterialRenderers[Material.MaterialType].Renderer->OnSetMaterial(
				Material, LastMaterial, ResetRenderStates, this);

		LastMaterial = Material;
		ResetRenderStates = false;
	}

	bgfx::setState(CacheHandler->getBgfxStateFlag(CacheHandler->getActiveView()));
	bgfx::submit(CacheHandler->getActiveView(), static_cast<CBgfxMaterialRenderer_BASE*>(MaterialRenderers[Material.MaterialType].Renderer)->getProgramHandle());
}


inline static void convertToBgfxColor(const SColor& color, u8* dest)
{
	*dest = color.getAlpha();
	*++dest = color.getBlue();
	*++dest = color.getGreen();
	*++dest = color.getRed();
}

void CBgfxDriver::getColorBuffer(const void* vertices, u32 vertexCount, E_VERTEX_TYPE vType)
{
	// convert colors to bgfx color format.
	vertexCount *= 4; //reused as color component count
	ColorBuffer.set_used(vertexCount);
	u32 i;

	switch (vType)
	{
		case EVT_STANDARD:
		{
			const S3DVertex* p = static_cast<const S3DVertex*>(vertices);
			for (i=0; i<vertexCount; i+=4)
			{
				convertToBgfxColor(p->Color,&ColorBuffer[i]);
				++p;
			}
		}
		break;
		case EVT_2TCOORDS:
		{
			const S3DVertex2TCoords* p = static_cast<const S3DVertex2TCoords*>(vertices);
			for (i=0; i<vertexCount; i+=4)
			{
				convertToBgfxColor(p->Color,&ColorBuffer[i]);
				++p;
			}
		}
		break;
		case EVT_TANGENTS:
		{
			const S3DVertexTangents* p = static_cast<const S3DVertexTangents*>(vertices);
			for (i=0; i<vertexCount; i+=4)
			{
				convertToBgfxColor(p->Color,&ColorBuffer[i]);
				++p;
			}
		}
		break;
	}
}

//! draws a vertex primitive list in 2d
void CBgfxDriver::draw2DVertexPrimitiveList(const void* vertices, u32 vertexCount,
		const void* indexList, u32 primitiveCount,
		E_VERTEX_TYPE vType, scene::E_PRIMITIVE_TYPE pType, E_INDEX_TYPE iType)
{
	if (!primitiveCount || !vertexCount)
		return;

	if (!checkPrimitiveCount(primitiveCount))
		return;

	CNullDriver::draw2DVertexPrimitiveList(vertices, vertexCount, indexList, primitiveCount, vType, pType, iType);

	//getColorBuffer(vertices, vertexCount, vType);

	// draw everything
	CacheHandler->getTextureCache().set(0, Material.getTexture(0));
	if (Material.MaterialType==EMT_ONETEXTURE_BLEND)
	{
		E_BLEND_FACTOR srcFact;
		E_BLEND_FACTOR dstFact;
		E_MODULATE_FUNC modulo;
		u32 alphaSource;
		unpack_textureBlendFunc ( srcFact, dstFact, modulo, alphaSource, Material.MaterialTypeParam);
		setRenderStates2DMode(alphaSource&video::EAS_VERTEX_COLOR, (Material.getTexture(0) != 0), (alphaSource&video::EAS_TEXTURE) != 0);
	}
	else
		setRenderStates2DMode(Material.MaterialType==EMT_TRANSPARENT_VERTEX_ALPHA, (Material.getTexture(0) != 0), Material.MaterialType==EMT_TRANSPARENT_ALPHA_CHANNEL);

	const u32 vertexSize = getVertexPitchFromType(vType);
	bgfx::VertexBufferHandle vbh;
	bgfx::IndexBufferHandle ibh;
	const u32 indexSize = iType==EIT_16BIT ? sizeof(u16) : sizeof(u32);
	uint64_t tempStateFlag = CacheHandler->getBgfxStateFlag(CacheHandler->get2DView());

	if (vertices)
	{
		//this is probably slow
		vbh = bgfx::createVertexBuffer(bgfx::copy(vertices, vertexCount*vertexSize),*irrToBgfx.getBgfxVertexDeclFromIrrVertexType(vType),BGFX_BUFFER_NONE);
		bgfx::setVertexBuffer(0,vbh);

	}
	switch (pType)
	{
		case scene::EPT_POINTS:
		case scene::EPT_POINT_SPRITES:
			tempStateFlag |= BGFX_STATE_PT_POINTS;
			if (indexList)
				ibh = bgfx::createIndexBuffer(bgfx::copy(indexList, primitiveCount*indexSize),
											iType==EIT_16BIT ? BGFX_BUFFER_NONE : BGFX_BUFFER_INDEX32);
			break;
		case scene::EPT_LINE_STRIP:
			tempStateFlag |= BGFX_STATE_PT_LINESTRIP;
			if (indexList)
				ibh = bgfx::createIndexBuffer(bgfx::copy(indexList, (primitiveCount+1)*indexSize),
											iType==EIT_16BIT ? BGFX_BUFFER_NONE : BGFX_BUFFER_INDEX32);
			break;
		case scene::EPT_LINE_LOOP:
			tempStateFlag |= BGFX_STATE_PT_LINES;
			if (indexList)
				ibh = bgfx::createIndexBuffer(bgfx::copy(indexList, primitiveCount*indexSize),
											iType==EIT_16BIT ? BGFX_BUFFER_NONE : BGFX_BUFFER_INDEX32);
			break;
		case scene::EPT_LINES:
			tempStateFlag |= BGFX_STATE_PT_LINES;
			if (indexList)
				ibh = bgfx::createIndexBuffer(bgfx::copy(indexList, (primitiveCount*2)*indexSize),
											iType==EIT_16BIT ? BGFX_BUFFER_NONE : BGFX_BUFFER_INDEX32);
			break;
		case scene::EPT_TRIANGLE_STRIP:
			tempStateFlag |= BGFX_STATE_PT_TRISTRIP;
			if (indexList)
				ibh = bgfx::createIndexBuffer(bgfx::copy(indexList, (primitiveCount+2)*indexSize),
											iType==EIT_16BIT ? BGFX_BUFFER_NONE : BGFX_BUFFER_INDEX32);
			break;
		case scene::EPT_TRIANGLE_FAN: //Is deprecated
			//bgfx::setState(BGFX_STATE_DEFAULT);
			if (indexList)
				ibh = bgfx::createIndexBuffer(bgfx::copy(indexList, (primitiveCount+2)*indexSize),
											iType==EIT_16BIT ? BGFX_BUFFER_NONE : BGFX_BUFFER_INDEX32);
			break;
		case scene::EPT_TRIANGLES:
			//bgfx::setState(BGFX_STATE_DEFAULT); // TODO: go through flags comparing the settings
			if (indexList)
				ibh = bgfx::createIndexBuffer(bgfx::copy(indexList, (primitiveCount*3)*indexSize),
											iType==EIT_16BIT ? BGFX_BUFFER_NONE : BGFX_BUFFER_INDEX32);
			break;
		case scene::EPT_QUAD_STRIP:
			//bgfx::setState(BGFX_STATE_DEFAULT); // TODO: go through flags comparing the settings
			if (indexList)
				ibh = bgfx::createIndexBuffer(bgfx::copy(indexList, (primitiveCount*2+2)*indexSize),
											iType==EIT_16BIT ? BGFX_BUFFER_NONE : BGFX_BUFFER_INDEX32);
			break;
		case scene::EPT_QUADS:
			//bgfx::setState(BGFX_STATE_DEFAULT); // TODO: go through flags comparing the settings
			if (indexList)
				ibh = bgfx::createIndexBuffer(bgfx::copy(indexList, (primitiveCount*4)*indexSize),
											iType==EIT_16BIT ? BGFX_BUFFER_NONE : BGFX_BUFFER_INDEX32);
			break;
		case scene::EPT_POLYGON:
			//bgfx::setState(BGFX_STATE_DEFAULT); // TODO: go through flags comparing the settings
			if (indexList)
				ibh = bgfx::createIndexBuffer(bgfx::copy(indexList, primitiveCount*indexSize),
											iType==EIT_16BIT ? BGFX_BUFFER_NONE : BGFX_BUFFER_INDEX32);
			break;

	}
	bgfx::setState(tempStateFlag);

	if (indexList)
		bgfx::setIndexBuffer(ibh);


	switch (vType)
	{
		case EVT_STANDARD:
			//Submit to 2dView for rendering
			bgfx::submit(CacheHandler->get2DView(), static_cast<CBgfxMaterialRenderer_BASE*>(MaterialRenderers[Material.MaterialType].Renderer)->getProgramHandle());
			break;
		case EVT_2TCOORDS:
			bgfx::submit(CacheHandler->get2DView(), static_cast<CBgfxMaterialRenderer_BASE*>(MaterialRenderers[Material.MaterialType].Renderer)->getProgramHandle());
			break;
		case EVT_TANGENTS:
			bgfx::submit(CacheHandler->get2DView(), static_cast<CBgfxMaterialRenderer_BASE*>(MaterialRenderers[Material.MaterialType].Renderer)->getProgramHandle());
			break;
	}

	if(vertices)
		bgfx::destroy(vbh);
	if(indexList)
		bgfx::destroy(ibh);
}


void CBgfxDriver::draw2DImage(const video::ITexture* texture, const core::position2d<s32>& destPos,
	const core::rect<s32>& sourceRect, const core::rect<s32>* clipRect, SColor color,
	bool useAlphaChannelOfTexture)
{
	if (!texture)
		return;

	if (!sourceRect.isValid())
		return;

	core::position2d<s32> targetPos(destPos);
	core::position2d<s32> sourcePos(sourceRect.UpperLeftCorner);
	// This needs to be signed as it may go negative.
	core::dimension2d<s32> sourceSize(sourceRect.getSize());
	if (clipRect)
	{
		if (targetPos.X < clipRect->UpperLeftCorner.X)
		{
			sourceSize.Width += targetPos.X - clipRect->UpperLeftCorner.X;
			if (sourceSize.Width <= 0)
				return;

			sourcePos.X -= targetPos.X - clipRect->UpperLeftCorner.X;
			targetPos.X = clipRect->UpperLeftCorner.X;
		}

		if (targetPos.X + sourceSize.Width > clipRect->LowerRightCorner.X)
		{
			sourceSize.Width -= (targetPos.X + sourceSize.Width) - clipRect->LowerRightCorner.X;
			if (sourceSize.Width <= 0)
				return;
		}

		if (targetPos.Y < clipRect->UpperLeftCorner.Y)
		{
			sourceSize.Height += targetPos.Y - clipRect->UpperLeftCorner.Y;
			if (sourceSize.Height <= 0)
				return;

			sourcePos.Y -= targetPos.Y - clipRect->UpperLeftCorner.Y;
			targetPos.Y = clipRect->UpperLeftCorner.Y;
		}

		if (targetPos.Y + sourceSize.Height > clipRect->LowerRightCorner.Y)
		{
			sourceSize.Height -= (targetPos.Y + sourceSize.Height) - clipRect->LowerRightCorner.Y;
			if (sourceSize.Height <= 0)
				return;
		}
	}

	// clip these coordinates

	if (targetPos.X<0)
	{
		sourceSize.Width += targetPos.X;
		if (sourceSize.Width <= 0)
			return;

		sourcePos.X -= targetPos.X;
		targetPos.X = 0;
	}

	const core::dimension2d<u32>& renderTargetSize = getCurrentRenderTargetSize();

	if (targetPos.X + sourceSize.Width > (s32)renderTargetSize.Width)
	{
		sourceSize.Width -= (targetPos.X + sourceSize.Width) - renderTargetSize.Width;
		if (sourceSize.Width <= 0)
			return;
	}

	if (targetPos.Y<0)
	{
		sourceSize.Height += targetPos.Y;
		if (sourceSize.Height <= 0)
			return;

		sourcePos.Y -= targetPos.Y;
		targetPos.Y = 0;
	}

	if (targetPos.Y + sourceSize.Height >(s32)renderTargetSize.Height)
	{
		sourceSize.Height -= (targetPos.Y + sourceSize.Height) - renderTargetSize.Height;
		if (sourceSize.Height <= 0)
			return;
	}

	// ok, we've clipped everything.
	// now draw it.

	const core::dimension2d<u32>& ss = texture->getOriginalSize();
	const f32 invW = 1.f / static_cast<f32>(ss.Width);
	const f32 invH = 1.f / static_cast<f32>(ss.Height);
	const core::rect<f32> tcoords(
		sourcePos.X * invW,
		sourcePos.Y * invH,
		(sourcePos.X + sourceSize.Width) * invW,
		(sourcePos.Y + sourceSize.Height) * invH);

	const core::rect<s32> poss(targetPos, sourceSize);

	disableTextures(1);
	if (!CacheHandler->getTextureCache().set(0, texture))
		return;
	setRenderStates2DMode(color.getAlpha()<255, true, useAlphaChannelOfTexture);

	Quad2DVertices[0].Color = color;
	Quad2DVertices[1].Color = color;
	Quad2DVertices[2].Color = color;
	Quad2DVertices[3].Color = color;

	Quad2DVertices[0].Pos = core::vector3df((f32)poss.UpperLeftCorner.X, (f32)poss.UpperLeftCorner.Y, 0.0f);
	Quad2DVertices[1].Pos = core::vector3df((f32)poss.LowerRightCorner.X, (f32)poss.UpperLeftCorner.Y, 0.0f);
	Quad2DVertices[2].Pos = core::vector3df((f32)poss.LowerRightCorner.X, (f32)poss.LowerRightCorner.Y, 0.0f);
	Quad2DVertices[3].Pos = core::vector3df((f32)poss.UpperLeftCorner.X, (f32)poss.LowerRightCorner.Y, 0.0f);

	Quad2DVertices[0].TCoords = core::vector2df(tcoords.UpperLeftCorner.X, tcoords.UpperLeftCorner.Y);
	Quad2DVertices[1].TCoords = core::vector2df(tcoords.LowerRightCorner.X, tcoords.UpperLeftCorner.Y);
	Quad2DVertices[2].TCoords = core::vector2df(tcoords.LowerRightCorner.X, tcoords.LowerRightCorner.Y);
	Quad2DVertices[3].TCoords = core::vector2df(tcoords.UpperLeftCorner.X, tcoords.LowerRightCorner.Y);

	if(4 == bgfx::getAvailTransientVertexBuffer(4,*irrToBgfx.standardVDecl))
	{
		bgfx::TransientVertexBuffer transientVBuffer2DImage;
		bgfx::allocTransientVertexBuffer(&transientVBuffer2DImage, 4, *irrToBgfx.standardVDecl);
		memmove(transientVBuffer2DImage.data,Quad2DVertices,4*sizeof(S3DVertex));
		bgfx::setVertexBuffer(0,&transientVBuffer2DImage);
		bgfx::setIndexBuffer(indexBufferQuad2DHandle);
		bgfx::setState(CacheHandler->getBgfxStateFlag(CacheHandler->get2DView()));

	}
	bgfx::submit(CacheHandler->get2DView(),StandardVertexProgramHandle);

}

void CBgfxDriver::draw2DImage(const video::ITexture* texture, const core::rect<s32>& destRect,
	const core::rect<s32>& sourceRect, const core::rect<s32>* clipRect,
	const video::SColor* const colors, bool useAlphaChannelOfTexture)
{
	if (!texture)
		return;

	const core::dimension2d<u32>& ss = texture->getOriginalSize();
	const f32 invW = 1.f / static_cast<f32>(ss.Width);
	const f32 invH = 1.f / static_cast<f32>(ss.Height);
	const core::rect<f32> tcoords(
		sourceRect.UpperLeftCorner.X * invW,
		sourceRect.UpperLeftCorner.Y * invH,
		sourceRect.LowerRightCorner.X * invW,
		sourceRect.LowerRightCorner.Y *invH);

	const video::SColor temp[4] =
	{
		0xFFFFFFFF,
		0xFFFFFFFF,
		0xFFFFFFFF,
		0xFFFFFFFF
	};

	const video::SColor* const useColor = colors ? colors : temp;

	disableTextures(1);
	if (!CacheHandler->getTextureCache().set(0, texture))
		return;
	setRenderStates2DMode(useColor[0].getAlpha()<255 || useColor[1].getAlpha()<255 ||
		useColor[2].getAlpha()<255 || useColor[3].getAlpha()<255,
		true, useAlphaChannelOfTexture);

	if (clipRect)
	{
		if (!clipRect->isValid())
			return;

		const core::dimension2d<u32>& renderTargetSize = getCurrentRenderTargetSize();
		bgfx::setScissor(clipRect->UpperLeftCorner.X, renderTargetSize.Height - clipRect->LowerRightCorner.Y,
			clipRect->getWidth(), clipRect->getHeight());
	}

	Quad2DVertices[0].Color = useColor[0];
	Quad2DVertices[1].Color = useColor[3];
	Quad2DVertices[2].Color = useColor[2];
	Quad2DVertices[3].Color = useColor[1];

	Quad2DVertices[0].Pos = core::vector3df((f32)destRect.UpperLeftCorner.X, (f32)destRect.UpperLeftCorner.Y, 0.0f);
	Quad2DVertices[1].Pos = core::vector3df((f32)destRect.LowerRightCorner.X, (f32)destRect.UpperLeftCorner.Y, 0.0f);
	Quad2DVertices[2].Pos = core::vector3df((f32)destRect.LowerRightCorner.X, (f32)destRect.LowerRightCorner.Y, 0.0f);
	Quad2DVertices[3].Pos = core::vector3df((f32)destRect.UpperLeftCorner.X, (f32)destRect.LowerRightCorner.Y, 0.0f);

	Quad2DVertices[0].TCoords = core::vector2df(tcoords.UpperLeftCorner.X, tcoords.UpperLeftCorner.Y);
	Quad2DVertices[1].TCoords = core::vector2df(tcoords.LowerRightCorner.X, tcoords.UpperLeftCorner.Y);
	Quad2DVertices[2].TCoords = core::vector2df(tcoords.LowerRightCorner.X, tcoords.LowerRightCorner.Y);
	Quad2DVertices[3].TCoords = core::vector2df(tcoords.UpperLeftCorner.X, tcoords.LowerRightCorner.Y);

	if(4 == bgfx::getAvailTransientVertexBuffer(4,*irrToBgfx.standardVDecl))
	{
		bgfx::TransientVertexBuffer transientVBuffer2DImage;
		bgfx::allocTransientVertexBuffer(&transientVBuffer2DImage, 4, *irrToBgfx.standardVDecl);
		memmove(transientVBuffer2DImage.data,Quad2DVertices,4*sizeof(S3DVertex));
		bgfx::setVertexBuffer(0,&transientVBuffer2DImage);
		bgfx::setIndexBuffer(indexBufferQuad2DHandle);
		bgfx::setState(CacheHandler->getBgfxStateFlag(CacheHandler->get2DView()));


	}
	bgfx::submit(CacheHandler->get2DView(),StandardVertexProgramHandle);

}

void CBgfxDriver::draw2DImage(const video::ITexture* texture, u32 layer, bool flip)
{
	if (!texture || !CacheHandler->getTextureCache().set(0, texture))
		return;

	disableTextures(1);

	setRenderStates2DMode(false, true, true);

	Transformation3DChanged = true;

	bgfx::VertexLayout vDecl;


	if (texture && texture->getType() == ETT_CUBEMAP)
	{
		vDecl.begin()
			.add(bgfx::Attrib::Position, 3, bgfx::AttribType::Float)
			.add(bgfx::Attrib::TexCoord0, 3, bgfx::AttribType::Float)
			.end();

		S3DVertex3DTCoords vertices[4];

		const core::vector3df texcoordCubeData[6][4] = {

			// GL_TEXTURE_CUBE_MAP_POSITIVE_X
			{
				core::vector3df(1.f, 1.f, 1.f),
				core::vector3df(1.f, 1.f, -1.f),
				core::vector3df(1.f, -1.f, -1.f),
				core::vector3df(1.f, -1.f, 1.f)
			},

			// GL_TEXTURE_CUBE_MAP_NEGATIVE_X
			{
				core::vector3df(-1.f, 1.f, -1.f),
				core::vector3df(-1.f, 1.f, 1.f),
				core::vector3df(-1.f, -1.f, 1.f),
				core::vector3df(-1.f, -1.f, -1.f)
			},

			// GL_TEXTURE_CUBE_MAP_POSITIVE_Y
			{
				core::vector3df(-1.f, 1.f, -1.f),
				core::vector3df(1.f, 1.f, -1.f),
				core::vector3df(1.f, 1.f, 1.f),
				core::vector3df(-1.f, 1.f, 1.f)
			},

			// GL_TEXTURE_CUBE_MAP_NEGATIVE_Y
			{
				core::vector3df(-1.f, -1.f, 1.f),
				core::vector3df(-1.f, -1.f, -1.f),
				core::vector3df(1.f, -1.f, -1.f),
				core::vector3df(1.f, -1.f, 1.f)
			},

			// GL_TEXTURE_CUBE_MAP_POSITIVE_Z
			{
				core::vector3df(-1.f, 1.f, 1.f),
				core::vector3df(-1.f, -1.f, 1.f),
				core::vector3df(1.f, -1.f, 1.f),
				core::vector3df(1.f, 1.f, 1.f)
			},

			// GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
			{
				core::vector3df(1.f, 1.f, -1.f),
				core::vector3df(-1.f, 1.f, -1.f),
				core::vector3df(-1.f, -1.f, -1.f),
				core::vector3df(1.f, -1.f, -1.f)
			}
		};

		vertices[0].TCoords = texcoordCubeData[layer][(flip) ? 3 : 0];
		vertices[1].TCoords = texcoordCubeData[layer][(flip) ? 2 : 1];
		vertices[2].TCoords = texcoordCubeData[layer][(flip) ? 1 : 2];
		vertices[3].TCoords = texcoordCubeData[layer][(flip) ? 0 : 3];

		vertices[0].Pos = core::vector3df(-1.f, 1.f, 0.f);
		vertices[1].Pos	= core::vector3df(1.f, 1.f, 0.f);
		vertices[2].Pos = core::vector3df(1.f, -1.f, 0.f);
		vertices[3].Pos	= core::vector3df(-1.f, -1.f, 0.f);

		if(4 == bgfx::getAvailTransientVertexBuffer(4,vDecl))
		{
			bgfx::TransientVertexBuffer transientVBuffer2DImage;
			bgfx::allocTransientVertexBuffer(&transientVBuffer2DImage, 4, vDecl);
			memmove(transientVBuffer2DImage.data,vertices,4*sizeof(S3DVertex3DTCoords));
			bgfx::setVertexBuffer(0,&transientVBuffer2DImage);
			bgfx::setIndexBuffer(indexBufferQuad2DHandle);
			bgfx::setState(CacheHandler->getBgfxStateFlag(CacheHandler->get2DView()));
			bgfx::submit(CacheHandler->get2DView(),Vertex3DTCoordsProgramHandle);
		}

		}

	else
	{
		const video::SColor temp[4] =
			{
				0xFFFFFFFF,
				0xFFFFFFFF,
				0xFFFFFFFF,
				0xFFFFFFFF
			};

		S3DVertex vertices[4];
		vertices[0].Pos = core::vector3df(-1.f, 1.f, 0.f);
		vertices[1].Pos	= core::vector3df(1.f, 1.f, 0.f);
		vertices[2].Pos = core::vector3df(1.f, -1.f, 0.f);
		vertices[3].Pos	= core::vector3df(-1.f, -1.f, 0.f);

		f32 modificator = (flip) ? 1.f : 0.f;


		vertices[0].TCoords = core::vector2df(0.f, 0.f + modificator);
		vertices[0].TCoords = core::vector2df(1.f, 0.f + modificator);
		vertices[0].TCoords = core::vector2df(1.f, 1.f - modificator);
		vertices[0].TCoords = core::vector2df(0.f, 1.f - modificator);

		vertices[0].Color = temp[0];
		vertices[1].Color = temp[1];
		vertices[2].Color = temp[2];
		vertices[3].Color = temp[3];


		if(4 == bgfx::getAvailTransientVertexBuffer(4,*irrToBgfx.standardVDecl))
		{
			bgfx::TransientVertexBuffer transientVBuffer2DImage;
			bgfx::allocTransientVertexBuffer(&transientVBuffer2DImage, 4, *irrToBgfx.standardVDecl);
			memmove(transientVBuffer2DImage.data,vertices,4*sizeof(S3DVertex));
			bgfx::setVertexBuffer(0,&transientVBuffer2DImage);
			bgfx::setIndexBuffer(indexBufferQuad2DHandle);
			bgfx::setState(CacheHandler->getBgfxStateFlag(CacheHandler->get2DView()));
			bgfx::submit(CacheHandler->get2DView(),StandardVertexProgramHandle);
	}


	}
}

//! draws a set of 2d images, using a color and the alpha channel of the
//! texture if desired.
void CBgfxDriver::draw2DImageBatch(const video::ITexture* texture,
				const core::array<core::position2d<s32> >& positions,
				const core::array<core::rect<s32> >& sourceRects,
				const core::rect<s32>* clipRect,
				SColor color,
				bool useAlphaChannelOfTexture)
{
	if (!texture)
		return;

	const u32 drawCount = core::min_<u32>(positions.size(), sourceRects.size());

	const core::dimension2d<u32>& ss = texture->getOriginalSize();
	const f32 invW = 1.f / static_cast<f32>(ss.Width);
	const f32 invH = 1.f / static_cast<f32>(ss.Height);
	const core::dimension2d<u32>& renderTargetSize = getCurrentRenderTargetSize();

	disableTextures(1);
	if (!CacheHandler->getTextureCache().set(0, texture))
		return;
	setRenderStates2DMode(color.getAlpha()<255, true, useAlphaChannelOfTexture);

	Quad2DVertices[0].Color = color;
	Quad2DVertices[1].Color = color;
	Quad2DVertices[2].Color = color;
	Quad2DVertices[3].Color = color;

	for (u32 i=0; i<drawCount; ++i)
	{
		if (!sourceRects[i].isValid())
			continue;

		core::position2d<s32> targetPos(positions[i]);
		core::position2d<s32> sourcePos(sourceRects[i].UpperLeftCorner);
		// This needs to be signed as it may go negative.
		core::dimension2d<s32> sourceSize(sourceRects[i].getSize());
		if (clipRect)
		{
			if (targetPos.X < clipRect->UpperLeftCorner.X)
			{
				sourceSize.Width += targetPos.X - clipRect->UpperLeftCorner.X;
				if (sourceSize.Width <= 0)
					continue;

				sourcePos.X -= targetPos.X - clipRect->UpperLeftCorner.X;
				targetPos.X = clipRect->UpperLeftCorner.X;
			}

			if (targetPos.X + sourceSize.Width > clipRect->LowerRightCorner.X)
			{
				sourceSize.Width -= (targetPos.X + sourceSize.Width) - clipRect->LowerRightCorner.X;
				if (sourceSize.Width <= 0)
					continue;
			}

			if (targetPos.Y < clipRect->UpperLeftCorner.Y)
			{
				sourceSize.Height += targetPos.Y - clipRect->UpperLeftCorner.Y;
				if (sourceSize.Height <= 0)
					continue;

				sourcePos.Y -= targetPos.Y - clipRect->UpperLeftCorner.Y;
				targetPos.Y = clipRect->UpperLeftCorner.Y;
			}

			if (targetPos.Y + sourceSize.Height > clipRect->LowerRightCorner.Y)
			{
				sourceSize.Height -= (targetPos.Y + sourceSize.Height) - clipRect->LowerRightCorner.Y;
				if (sourceSize.Height <= 0)
					continue;
			}
		}

		// clip these coordinates

		if (targetPos.X<0)
		{
			sourceSize.Width += targetPos.X;
			if (sourceSize.Width <= 0)
				continue;

			sourcePos.X -= targetPos.X;
			targetPos.X = 0;
		}

		if (targetPos.X + sourceSize.Width > (s32)renderTargetSize.Width)
		{
			sourceSize.Width -= (targetPos.X + sourceSize.Width) - renderTargetSize.Width;
			if (sourceSize.Width <= 0)
				continue;
		}

		if (targetPos.Y<0)
		{
			sourceSize.Height += targetPos.Y;
			if (sourceSize.Height <= 0)
				continue;

			sourcePos.Y -= targetPos.Y;
			targetPos.Y = 0;
		}

		if (targetPos.Y + sourceSize.Height > (s32)renderTargetSize.Height)
		{
			sourceSize.Height -= (targetPos.Y + sourceSize.Height) - renderTargetSize.Height;
			if (sourceSize.Height <= 0)
				continue;
		}

		// ok, we've clipped everything.
		// now draw it.

		const core::rect<f32> tcoords(
				sourcePos.X * invW,
				sourcePos.Y * invH,
				(sourcePos.X + sourceSize.Width) * invW,
				(sourcePos.Y + sourceSize.Height) * invH);

		const core::rect<s32> poss(targetPos, sourceSize);

		Quad2DVertices[0].Pos = core::vector3df((f32)poss.UpperLeftCorner.X, (f32)poss.UpperLeftCorner.Y, 0.0f);
		Quad2DVertices[1].Pos = core::vector3df((f32)poss.LowerRightCorner.X, (f32)poss.UpperLeftCorner.Y, 0.0f);
		Quad2DVertices[2].Pos = core::vector3df((f32)poss.LowerRightCorner.X, (f32)poss.LowerRightCorner.Y, 0.0f);
		Quad2DVertices[3].Pos = core::vector3df((f32)poss.UpperLeftCorner.X, (f32)poss.LowerRightCorner.Y, 0.0f);

		Quad2DVertices[0].TCoords = core::vector2df(tcoords.UpperLeftCorner.X, tcoords.UpperLeftCorner.Y);
		Quad2DVertices[1].TCoords = core::vector2df(tcoords.LowerRightCorner.X, tcoords.UpperLeftCorner.Y);
		Quad2DVertices[2].TCoords = core::vector2df(tcoords.LowerRightCorner.X, tcoords.LowerRightCorner.Y);
		Quad2DVertices[3].TCoords = core::vector2df(tcoords.UpperLeftCorner.X, tcoords.LowerRightCorner.Y);

		if(4 == bgfx::getAvailTransientVertexBuffer(4,*irrToBgfx.standardVDecl))
		{
			bgfx::TransientVertexBuffer transientVBuffer2DImage;
			bgfx::allocTransientVertexBuffer(&transientVBuffer2DImage, 4, *irrToBgfx.standardVDecl);
			memmove(transientVBuffer2DImage.data,Quad2DVertices,4*sizeof(S3DVertex));
			bgfx::setVertexBuffer(0,&transientVBuffer2DImage);
			bgfx::setIndexBuffer(indexBufferQuad2DHandle);
			bgfx::setState(CacheHandler->getBgfxStateFlag(CacheHandler->get2DView()));
			bgfx::submit(CacheHandler->get2DView(),StandardVertexProgramHandle);
		}
	}
}

//! draws a set of 2d images, using a color and the alpha channel of the
//! texture if desired. The images are drawn beginning at pos and concatenated
//! in one line. All drawings are clipped against clipRect (if != 0).
//! The subtextures are defined by the array of sourceRects and are chosen
//! by the indices given.
void CBgfxDriver::draw2DImageBatch(const video::ITexture* texture,
				const core::position2d<s32>& pos,
				const core::array<core::rect<s32> >& sourceRects,
				const core::array<s32>& indices,
				s32 kerningWidth,
				const core::rect<s32>* clipRect, SColor color,
				bool useAlphaChannelOfTexture)
{
	if (!texture)
		return;

	disableTextures(1);
	if (!CacheHandler->getTextureCache().set(0, texture))
		return;
	setRenderStates2DMode(color.getAlpha()<255, true, useAlphaChannelOfTexture);

	if (clipRect)
	{
		if (!clipRect->isValid())
			return;

		const core::dimension2d<u32>& renderTargetSize = getCurrentRenderTargetSize();
		bgfx::setScissor(clipRect->UpperLeftCorner.X, renderTargetSize.Height-clipRect->LowerRightCorner.Y,
			clipRect->getWidth(),clipRect->getHeight());
	}

	const core::dimension2d<u32>& ss = texture->getOriginalSize();
	core::position2d<s32> targetPos(pos);
	const f32 invW = 1.f / static_cast<f32>(ss.Width);
	const f32 invH = 1.f / static_cast<f32>(ss.Height);

	Quad2DVertices[0].Color = color;
	Quad2DVertices[1].Color = color;
	Quad2DVertices[2].Color = color;
	Quad2DVertices[3].Color = color;


	for (u32 i=0; i<indices.size(); ++i)
	{
		const s32 currentIndex = indices[i];
		if (!sourceRects[currentIndex].isValid())
			break;

		const core::rect<f32> tcoords(
				sourceRects[currentIndex].UpperLeftCorner.X * invW,
				sourceRects[currentIndex].UpperLeftCorner.Y * invH,
				sourceRects[currentIndex].LowerRightCorner.X * invW,
				sourceRects[currentIndex].LowerRightCorner.Y * invH);

		const core::rect<s32> poss(targetPos, sourceRects[currentIndex].getSize());

		Quad2DVertices[0].Pos = core::vector3df((f32)poss.UpperLeftCorner.X, (f32)poss.UpperLeftCorner.Y, 0.0f);
		Quad2DVertices[1].Pos = core::vector3df((f32)poss.LowerRightCorner.X, (f32)poss.UpperLeftCorner.Y, 0.0f);
		Quad2DVertices[2].Pos = core::vector3df((f32)poss.LowerRightCorner.X, (f32)poss.LowerRightCorner.Y, 0.0f);
		Quad2DVertices[3].Pos = core::vector3df((f32)poss.UpperLeftCorner.X, (f32)poss.LowerRightCorner.Y, 0.0f);

		Quad2DVertices[0].TCoords = core::vector2df(tcoords.UpperLeftCorner.X, tcoords.UpperLeftCorner.Y);
		Quad2DVertices[1].TCoords = core::vector2df(tcoords.LowerRightCorner.X, tcoords.UpperLeftCorner.Y);
		Quad2DVertices[2].TCoords = core::vector2df(tcoords.LowerRightCorner.X, tcoords.LowerRightCorner.Y);
		Quad2DVertices[3].TCoords = core::vector2df(tcoords.UpperLeftCorner.X, tcoords.LowerRightCorner.Y);

		if(4 == bgfx::getAvailTransientVertexBuffer(4,*irrToBgfx.standardVDecl))
		{
			bgfx::TransientVertexBuffer transientVBuffer2DImage;
			bgfx::allocTransientVertexBuffer(&transientVBuffer2DImage, 4, *irrToBgfx.standardVDecl);
			memmove(transientVBuffer2DImage.data,Quad2DVertices,4*sizeof(S3DVertex));
			bgfx::setVertexBuffer(0,&transientVBuffer2DImage);
			bgfx::setIndexBuffer(indexBufferQuad2DHandle);
			bgfx::setState(CacheHandler->getBgfxStateFlag(CacheHandler->get2DView()));
			bgfx::submit(CacheHandler->get2DView(),StandardVertexProgramHandle);
		}


		targetPos.X += sourceRects[currentIndex].getWidth();
	}

}

//! draw a 2d rectangle
void CBgfxDriver::draw2DRectangle(SColor color, const core::rect<s32>& position,
		const core::rect<s32>* clip)
{
	disableTextures();
	setRenderStates2DMode(color.getAlpha() < 255, false, false);

	core::rect<s32> pos = position;

	if (clip)
		pos.clipAgainst(*clip);

	if (!pos.isValid())
		return;

	Quad2DVertices[0].Pos = core::vector3df((f32)pos.UpperLeftCorner.X, (f32)pos.UpperLeftCorner.Y, 0.0f);
	Quad2DVertices[1].Pos = core::vector3df((f32)pos.LowerRightCorner.X, (f32)pos.UpperLeftCorner.Y, 0.0f);
	Quad2DVertices[2].Pos = core::vector3df((f32)pos.LowerRightCorner.X, (f32)pos.LowerRightCorner.Y, 0.0f);
	Quad2DVertices[3].Pos = core::vector3df((f32)pos.UpperLeftCorner.X, (f32)pos.LowerRightCorner.Y, 0.0f);

	Quad2DVertices[0].Color = color;
	Quad2DVertices[1].Color = color;
	Quad2DVertices[2].Color = color;
	Quad2DVertices[3].Color = color;

	if(4 == bgfx::getAvailTransientVertexBuffer(4,*irrToBgfx.standardVDecl))
	{
		bgfx::TransientVertexBuffer transientVBuffer2DImage;
		bgfx::allocTransientVertexBuffer(&transientVBuffer2DImage, 4, *irrToBgfx.standardVDecl);
		memmove(transientVBuffer2DImage.data,Quad2DVertices,4*sizeof(S3DVertex));
		bgfx::setVertexBuffer(0,&transientVBuffer2DImage,0,4);
		bgfx::setIndexBuffer(indexBufferQuad2DHandle);
		bgfx::setState(CacheHandler->getBgfxStateFlag(CacheHandler->get2DView()));
		bgfx::submit(CacheHandler->get2DView(),BasicProgramHandle);
	}

}

//! draw an 2d rectangle
void CBgfxDriver::draw2DRectangle(const core::rect<s32>& position,
			SColor colorLeftUp, SColor colorRightUp, SColor colorLeftDown, SColor colorRightDown,
			const core::rect<s32>* clip)
{
	core::rect<s32> pos = position;

	if (clip)
		pos.clipAgainst(*clip);

	if (!pos.isValid())
		return;

	disableTextures();

	setRenderStates2DMode(colorLeftUp.getAlpha() < 255 ||
		colorRightUp.getAlpha() < 255 ||
		colorLeftDown.getAlpha() < 255 ||
		colorRightDown.getAlpha() < 255, false, false);

	Quad2DVertices[0].Color = colorLeftUp;
	Quad2DVertices[1].Color = colorRightUp;
	Quad2DVertices[2].Color = colorRightDown;
	Quad2DVertices[3].Color = colorLeftDown;

	Quad2DVertices[0].Pos = core::vector3df((f32)pos.UpperLeftCorner.X, (f32)pos.UpperLeftCorner.Y, 0.0f);
	Quad2DVertices[1].Pos = core::vector3df((f32)pos.LowerRightCorner.X, (f32)pos.UpperLeftCorner.Y, 0.0f);
	Quad2DVertices[2].Pos = core::vector3df((f32)pos.LowerRightCorner.X, (f32)pos.LowerRightCorner.Y, 0.0f);
	Quad2DVertices[3].Pos = core::vector3df((f32)pos.UpperLeftCorner.X, (f32)pos.LowerRightCorner.Y, 0.0f);

	if(4 == bgfx::getAvailTransientVertexBuffer(4,*irrToBgfx.standardVDecl))
	{	bgfx::TransientVertexBuffer transientVBuffer2DImage;
		bgfx::allocTransientVertexBuffer(&transientVBuffer2DImage, 4, *irrToBgfx.standardVDecl);
		memmove(transientVBuffer2DImage.data,Quad2DVertices,4*sizeof(S3DVertex));
		bgfx::setVertexBuffer(0,&transientVBuffer2DImage);
		bgfx::setIndexBuffer(indexBufferQuad2DHandle);
		bgfx::setState(CacheHandler->getBgfxStateFlag(CacheHandler->get2DView()));
		bgfx::submit(CacheHandler->get2DView(),BasicProgramHandle);
	}

}

//! Draws a 2d line.
void CBgfxDriver::draw2DLine(const core::position2d<s32>& start,
				const core::position2d<s32>& end, SColor color)
{
	if (start==end)
		drawPixel(start.X, start.Y, color);
	else
	{
		disableTextures();
		setRenderStates2DMode(color.getAlpha() < 255, false, false);

		Quad2DVertices[0].Color = color;
		Quad2DVertices[1].Color = color;

		Quad2DVertices[0].Pos = core::vector3df((f32)start.X, (f32)start.Y, 0.0f);
		Quad2DVertices[1].Pos = core::vector3df((f32)end.X, (f32)end.Y, 0.0f);


		const u16 IndxArr[2] = {0,1};


		if((2 == bgfx::getAvailTransientVertexBuffer(2,*irrToBgfx.standardVDecl)) && (2 == bgfx::getAvailTransientIndexBuffer(2)))
		{	bgfx::TransientVertexBuffer transientVBuffer2DImage;
			bgfx::allocTransientVertexBuffer(&transientVBuffer2DImage, 2, *irrToBgfx.standardVDecl);
			memmove(transientVBuffer2DImage.data,Quad2DVertices,2*sizeof(S3DVertex));
			bgfx::setVertexBuffer(0,&transientVBuffer2DImage);
			bgfx::TransientIndexBuffer transientIndexBuffer;
			bgfx::allocTransientIndexBuffer(&transientIndexBuffer,2);
			memmove(transientIndexBuffer.data,IndxArr,2*sizeof(u16));
			bgfx::setIndexBuffer(&transientIndexBuffer);
			bgfx::setState(0 | BGFX_STATE_PT_LINES | CacheHandler->getBgfxStateFlag(CacheHandler->get2DView()));
			bgfx::submit(CacheHandler->get2DView(),BasicProgramHandle);
		}
	}

}

//! Draws a pixel
void CBgfxDriver::drawPixel(u32 x, u32 y, const SColor &color)
{
	const core::dimension2d<u32>& renderTargetSize = getCurrentRenderTargetSize();
	if (x > (u32)renderTargetSize.Width || y > (u32)renderTargetSize.Height)
		return;

	disableTextures();
	setRenderStates2DMode(color.getAlpha() < 255, false, false);

	Quad2DVertices[0].Color = color;

	Quad2DVertices[0].Pos = core::vector3df((f32)x, (f32)y, 0.0f);

	const u16 IndxArr[1] = {0};


	if((1 == bgfx::getAvailTransientVertexBuffer(1,*irrToBgfx.standardVDecl)) && (1 == bgfx::getAvailTransientIndexBuffer(1)))
	{	bgfx::TransientVertexBuffer transientVBuffer2DImage;
		bgfx::allocTransientVertexBuffer(&transientVBuffer2DImage, 1, *irrToBgfx.standardVDecl);
		memmove(transientVBuffer2DImage.data,Quad2DVertices,1*sizeof(S3DVertex));
		bgfx::setVertexBuffer(0,&transientVBuffer2DImage);

		bgfx::TransientIndexBuffer transientIndexBuffer;
		bgfx::allocTransientIndexBuffer(&transientIndexBuffer,1);
		memmove(transientIndexBuffer.data,IndxArr,1*sizeof(u16));
		bgfx::setIndexBuffer(&transientIndexBuffer);
		bgfx::setState(0 | BGFX_STATE_PT_POINTS | CacheHandler->getBgfxStateFlag(CacheHandler->get2DView()));
		bgfx::submit(CacheHandler->get2DView(),BasicProgramHandle);
	}

}

//! disables all textures beginning with the optional fromStage parameter. Otherwise all texture stages are disabled.
//! Returns whether disabling was successful or not.
bool CBgfxDriver::disableTextures(u32 fromStage)
{
	bool result=true;
	for (u32 i=fromStage; i<Feature->TextureUnit; ++i)
	{
		result &= CacheHandler->getTextureCache().set(i, 0);
	}
	return result;
}

ITexture* CBgfxDriver::createDeviceDependentTexture(const io::path& name, IImage* image)
{
	core::array<IImage*> imageArray(1);
	imageArray.push_back(image);

	CBgfxTexture* texture = new CBgfxTexture(name, imageArray, ETT_2D, this);

	return texture;
}

ITexture* CBgfxDriver::createDeviceDependentTextureCubemap(const io::path& name, const core::array<IImage*>& image)
{
	CBgfxTexture* texture = new CBgfxTexture(name, image, ETT_CUBEMAP, this);

	return texture;
}

//! returns a texture array from textures
ITexture* CBgfxDriver::createDeviceDependentTextureArray(const io::path& name, const core::array<IImage*> &image)
{
	CBgfxTexture* texture = new CBgfxTexture(name, image, ETT_ARRAY, this);

	return texture;
}

//! Sets a material. All 3d drawing functions draw geometry now using this material.
void CBgfxDriver::setMaterial(const SMaterial& material)
{
	Material = material;
	OverrideMaterial.apply(Material);

	//TODO:
	//QUICK FIX to use another program if there is no texture -> use basicshader
	if(!material.getTexture(0) && (Material.MaterialType <= u32(EMT_ONETEXTURE_BLEND)))
	{
		Material.MaterialType = static_cast<E_MATERIAL_TYPE>(u32(EMT_ONETEXTURE_BLEND)+1);
	}

	for (u32 i = 0; i < Feature->TextureUnit; ++i)
	{
		CacheHandler->getTextureCache().set(i, material.getTexture(i), material.TextureLayer[i].TextureWrapU,
											material.TextureLayer[i].TextureWrapV, material.TextureLayer[i].TextureWrapW,
											material.TextureLayer[i].TextureCompareFlag, material.TextureLayer[i].BilinearFilter);
		setTransform((E_TRANSFORMATION_STATE)(ETS_TEXTURE_0 + i), material.getTextureMatrix(i));
	}

	CacheHandler->setActiveTexture(0);
}


//! sets the needed renderstates
void CBgfxDriver::setRenderStates3DMode()
{
	if (CurrentRenderMode != ERM_3D)
	{
		// Reset Texture Stages
		CacheHandler->setBlendIndexed(CacheHandler->getActiveView(),false); //Todo: setup cachehandler for bgfx
		CacheHandler->setAlphaTest(false); //TODO: Relict can probably be removed
		CacheHandler->setBlendFuncIndexed(CacheHandler->getActiveView(),BGFX_STATE_BLEND_SRC_ALPHA, BGFX_STATE_BLEND_INV_SRC_ALPHA);

		// switch back the matrices
		bgfx::setViewTransform(CacheHandler->getActiveView(),(Matrices[ETS_VIEW]).pointer(),Matrices[ETS_PROJECTION].pointer());
		ResetRenderStates = true;
	}
	bgfx::setTransform(Matrices[ETS_WORLD].pointer()); // while view and projection transforms stay between submits the world transform MUST be set before every submit. Because the driver can't be sure that every draw* call is preceded by a setTransform(ETS_WORLD,...) it is done here. If the world transform isn't set bgfx might produce errors.
	if (ResetRenderStates || LastMaterial != Material)
	{
		// unset old material

		if (LastMaterial.MaterialType != Material.MaterialType &&
				static_cast<u32>(LastMaterial.MaterialType) < MaterialRenderers.size())
			MaterialRenderers[LastMaterial.MaterialType].Renderer->OnUnsetMaterial();

		// set new material.
		if (static_cast<u32>(Material.MaterialType) < MaterialRenderers.size())
			MaterialRenderers[Material.MaterialType].Renderer->OnSetMaterial(
				Material, LastMaterial, ResetRenderStates, this);

		LastMaterial = Material;
		ResetRenderStates = false;
	}

	if (static_cast<u32>(Material.MaterialType) < MaterialRenderers.size())
		MaterialRenderers[Material.MaterialType].Renderer->OnRender(this, video::EVT_STANDARD);

	CurrentRenderMode = ERM_3D;
}

//! Get native wrap mode value
u32 CBgfxDriver::getTextureWrapMode(const u8 clamp)
{
	u32 mode = BGFX_SAMPLER_NONE;
	switch (clamp)
	{
		case ETC_REPEAT:
			mode=BGFX_SAMPLER_NONE;
			break;
		case ETC_CLAMP:
			mode=BGFX_SAMPLER_U_CLAMP | BGFX_SAMPLER_V_CLAMP | BGFX_SAMPLER_W_CLAMP;
			break;
		case ETC_CLAMP_TO_EDGE:
			mode=BGFX_SAMPLER_U_CLAMP | BGFX_SAMPLER_V_CLAMP | BGFX_SAMPLER_W_CLAMP;
			break;
		case ETC_CLAMP_TO_BORDER:
			mode=BGFX_SAMPLER_U_BORDER | BGFX_SAMPLER_V_BORDER | BGFX_SAMPLER_W_BORDER;
			break;
		case ETC_MIRROR:
			mode=BGFX_SAMPLER_U_MIRROR | BGFX_SAMPLER_V_MIRROR | BGFX_SAMPLER_W_MIRROR;
			break;
		case ETC_MIRROR_CLAMP:

			mode=	BGFX_SAMPLER_U_MIRROR | BGFX_SAMPLER_V_MIRROR | BGFX_SAMPLER_W_MIRROR |
					BGFX_SAMPLER_U_CLAMP | BGFX_SAMPLER_V_CLAMP | BGFX_SAMPLER_W_CLAMP;
			break;
		case ETC_MIRROR_CLAMP_TO_EDGE:
			mode=	BGFX_SAMPLER_U_MIRROR | BGFX_SAMPLER_V_MIRROR | BGFX_SAMPLER_W_MIRROR |
					BGFX_SAMPLER_U_CLAMP | BGFX_SAMPLER_V_CLAMP | BGFX_SAMPLER_W_CLAMP;
			break;
		case ETC_MIRROR_CLAMP_TO_BORDER:
			mode=	BGFX_SAMPLER_U_MIRROR | BGFX_SAMPLER_V_MIRROR | BGFX_SAMPLER_W_MIRROR |
					BGFX_SAMPLER_U_BORDER | BGFX_SAMPLER_V_BORDER | BGFX_SAMPLER_W_BORDER;
			break;
	}
	return mode;
}

//! Can be called by an IMaterialRenderer to make its work easier.
//! As Bgfx has no use for Material flags and only uses flags -> we do everything in the materialrender
void CBgfxDriver::setBasicRenderStates(const SMaterial& material, const SMaterial& lastmaterial,
	bool resetAllRenderStates)
{

	//TODO: setRenderstate for the views separately

		//you should use own shaders ;)
	if(resetAllRenderStates)
		CacheHandler->resetExtraFlags();

	if (resetAllRenderStates || (lastmaterial.Wireframe != material.Wireframe) || (lastmaterial.PointCloud != material.PointCloud))
	{
		CacheHandler->addExtraFlag(material.Wireframe ?  BGFX_STATE_PT_LINES: material.PointCloud? BGFX_STATE_PT_POINTS : 0);
		if(!material.Wireframe)
		{
			CacheHandler->removeExtraFlag(BGFX_STATE_PT_LINES);
		}
		if(!material.PointCloud)
		{
			CacheHandler->removeExtraFlag(BGFX_STATE_PT_POINTS);
		}
	}


	// ZBuffer
	switch (material.ZBuffer)
	{
	case ECFN_DISABLED:
		CacheHandler->setDepthTest(false);
		break;
	case ECFN_LESSEQUAL:
		CacheHandler->setDepthTest(true);
		CacheHandler->setDepthFunc(BGFX_STATE_DEPTH_TEST_LEQUAL);
		break;
	case ECFN_EQUAL:
		CacheHandler->setDepthTest(true);
		CacheHandler->setDepthFunc(BGFX_STATE_DEPTH_TEST_EQUAL);
		break;
	case ECFN_LESS:
		CacheHandler->setDepthTest(true);
		CacheHandler->setDepthFunc(BGFX_STATE_DEPTH_TEST_LESS);
		break;
	case ECFN_NOTEQUAL:
		CacheHandler->setDepthTest(true);
		CacheHandler->setDepthFunc(BGFX_STATE_DEPTH_TEST_NOTEQUAL);
		break;
	case ECFN_GREATEREQUAL:
		CacheHandler->setDepthTest(true);
		CacheHandler->setDepthFunc(BGFX_STATE_DEPTH_TEST_GEQUAL);
		break;
	case ECFN_GREATER:
		CacheHandler->setDepthTest(true);
		CacheHandler->setDepthFunc(BGFX_STATE_DEPTH_TEST_GREATER);
		break;
	case ECFN_ALWAYS:
		CacheHandler->setDepthTest(true);
		CacheHandler->setDepthFunc(BGFX_STATE_DEPTH_TEST_ALWAYS);
		break;
	case ECFN_NEVER:
		CacheHandler->setDepthTest(true);
		CacheHandler->setDepthFunc(BGFX_STATE_DEPTH_TEST_NEVER);
		break;
	default:
		break;
	}

	// ZWrite
	if (getWriteZBuffer(material))
	{
		CacheHandler->setDepthMask(true);
	}
	else
	{
		CacheHandler->setDepthMask(false);
	}

	//Culling
	uint64_t cullFront = BGFX_STATE_CULL_CW ;
	uint64_t cullBack = BGFX_STATE_CULL_CCW;

	if (CurrentRenderTarget != nullptr && this->getDriverType() == video::E_DRIVER_TYPE::EDT_BGFX_OPENGL)
	{
		// opengl renders to a framebuffer vertically flipped and the driver compensates this by changing the projection matrix in setTransform.
		// But this also affects culling (if enabled) -> flip culling, too frontface <-> backface
		cullFront = BGFX_STATE_CULL_CCW ;
		cullBack = BGFX_STATE_CULL_CW;
	}

	if ((material.FrontfaceCulling) && (material.BackfaceCulling))
	{
		CacheHandler->setCullFaceFunc(cullBack | cullFront);
		CacheHandler->setCullFace(true);
	}
	else if (material.BackfaceCulling)
	{
		CacheHandler->setCullFaceFunc(cullBack);
		CacheHandler->setCullFace(true);
	}
	else if (material.FrontfaceCulling)
	{
		CacheHandler->setCullFaceFunc(cullFront);
		CacheHandler->setCullFace(true);
	}
	else
	{
		CacheHandler->setCullFace(false);
	}

	// Color Mask
	CacheHandler->setColorMask(
		(material.ColorMask & ECP_RED)?true:false,
		(material.ColorMask & ECP_GREEN)?true:false,
		(material.ColorMask & ECP_BLUE)?true:false,
		(material.ColorMask & ECP_ALPHA)?true:false);

	// Blend Equation
	if (material.BlendOperation == EBO_NONE)
		CacheHandler->setBlend(false);
	else
	{
		CacheHandler->setBlend(true);

		switch (material.BlendOperation)
		{
		case EBO_SUBTRACT:
				CacheHandler->setBlendEquation(BGFX_STATE_BLEND_EQUATION_SUB);
			break;
		case EBO_REVSUBTRACT:
			CacheHandler->setBlendEquation(BGFX_STATE_BLEND_EQUATION_REVSUB);
			break;
		case EBO_MIN:
			CacheHandler->setBlendEquation(BGFX_STATE_BLEND_EQUATION_MIN);
			break;
		case EBO_MAX:
			CacheHandler->setBlendEquation(BGFX_STATE_BLEND_EQUATION_MAX);
			break;
		case EBO_MIN_FACTOR:
			CacheHandler->setBlendEquation(BGFX_STATE_BLEND_EQUATION_MIN);

			break;
		case EBO_MAX_FACTOR:
			CacheHandler->setBlendEquation(BGFX_STATE_BLEND_EQUATION_MAX);
			break;
		case EBO_MIN_ALPHA:
			CacheHandler->setBlendEquationSeparate(0, BGFX_STATE_BLEND_EQUATION_MIN);
			break;
		case EBO_MAX_ALPHA:
			CacheHandler->setBlendEquationSeparate(0,BGFX_STATE_BLEND_EQUATION_MAX);
			break;
		default:
			CacheHandler->setBlendEquation(BGFX_STATE_BLEND_EQUATION_ADD);
			break;
		}
	}

	// Blend Factor
	if (IR(material.BlendFactor) & 0xFFFFFFFF)
	{
		E_BLEND_FACTOR srcRGBFact = EBF_ZERO;
		E_BLEND_FACTOR dstRGBFact = EBF_ZERO;
		E_BLEND_FACTOR srcAlphaFact = EBF_ZERO;
		E_BLEND_FACTOR dstAlphaFact = EBF_ZERO;
		E_MODULATE_FUNC modulo = EMFN_MODULATE_1X;
		u32 alphaSource = 0;

		unpack_textureBlendFuncSeparate(srcRGBFact, dstRGBFact, srcAlphaFact, dstAlphaFact, modulo, alphaSource, material.BlendFactor);

		if (queryFeature(EVDF_BLEND_SEPARATE))
		{
			CacheHandler->setBlendFuncSeparate(getBgfxBlend(srcRGBFact), getBgfxBlend(dstRGBFact),
					getBgfxBlend(srcAlphaFact), getBgfxBlend(dstAlphaFact));
		}
		else
		{
			CacheHandler->setBlendFunc(getBgfxBlend(srcRGBFact), getBgfxBlend(dstRGBFact));
		}
	}

	// Polygon Offset
	if (queryFeature(EVDF_POLYGON_OFFSET) && (resetAllRenderStates ||
		lastmaterial.PolygonOffsetDirection != material.PolygonOffsetDirection ||
		lastmaterial.PolygonOffsetFactor != material.PolygonOffsetFactor))
	{
		//no polygon offset in BGFX -> use shader
	}

	// thickness
	if (resetAllRenderStates || lastmaterial.Thickness != material.Thickness)
	{

		CacheHandler->addExtraFlag(BGFX_STATE_POINT_SIZE(material.Thickness));
	}

	// Anti aliasing
	if (resetAllRenderStates || lastmaterial.AntiAliasing != material.AntiAliasing)
	{
		if (!(material.AntiAliasing & EAAM_ALPHA_TO_COVERAGE))
			CacheHandler->removeExtraFlag(BGFX_STATE_BLEND_ALPHA_TO_COVERAGE);
		else
			CacheHandler->addExtraFlag(BGFX_STATE_BLEND_ALPHA_TO_COVERAGE);


			if ((AntiAlias >= 2) && (material.AntiAliasing & (EAAM_SIMPLE|EAAM_QUALITY)))
			{
				CacheHandler->addExtraFlag(BGFX_STATE_MSAA);
			}
			else if(!(material.AntiAliasing & (EAAM_SIMPLE|EAAM_QUALITY)))
				CacheHandler->removeExtraFlag(BGFX_STATE_MSAA);

		if ((material.AntiAliasing & EAAM_LINE_SMOOTH) != (lastmaterial.AntiAliasing & EAAM_LINE_SMOOTH))
		{

			if (!(material.AntiAliasing & EAAM_LINE_SMOOTH))
				CacheHandler->removeExtraFlag(BGFX_STATE_LINEAA);
			else if (material.AntiAliasing & EAAM_LINE_SMOOTH)
				CacheHandler->addExtraFlag(BGFX_STATE_LINEAA);
		}
		if ((material.AntiAliasing & EAAM_POINT_SMOOTH) != (lastmaterial.AntiAliasing & EAAM_POINT_SMOOTH))
		{
			//NO POINT SMOOTHING
		}
	}

		// Texture parameters
		setTextureRenderStates(material, resetAllRenderStates);
}

//! Compare in SMaterial doesn't check texture parameters, so we should call this on each OnRender call.
void CBgfxDriver::setTextureRenderStates(const SMaterial& material, bool resetAllRenderstates)
{
	//TODO: Compare to the OpenGL driver and add if necessary

}

//! Enable the 2d override material
void CBgfxDriver::enableMaterial2D(bool enable)
{
	if (!enable)
		CurrentRenderMode = ERM_NONE;
	CNullDriver::enableMaterial2D(enable);
}


//! sets the needed renderstates
void CBgfxDriver::setRenderStates2DMode(bool alpha, bool texture, bool alphaChannel)
{


	if (CurrentRenderMode != ERM_2D || Transformation3DChanged)
	{
		CacheHandler->setDepthMask(false);
		// unset last 3d material
		if (CurrentRenderMode == ERM_3D)
		{
			if (static_cast<u32>(LastMaterial.MaterialType) < MaterialRenderers.size())
				MaterialRenderers[LastMaterial.MaterialType].Renderer->OnUnsetMaterial();
		}
		if (Transformation3DChanged)
		{

			const core::dimension2d<u32>& renderTargetSize = getCurrentRenderTargetSize();
			core::matrix4 m(core::matrix4::EM4CONST_NOTHING);
			m.buildProjectionMatrixOrthoLH(f32(renderTargetSize.Width), f32(-(s32)(renderTargetSize.Height)), -1.0f, 1.0f);
			m.setTranslation(core::vector3df(-1,1,0));

			core::matrix4 mView = core::matrix4();
			mView.setTranslation(core::vector3df(0.375f, 0.375f, 0.0f));

			bgfx::setViewTransform(CacheHandler->get2DView(),mView.pointer(),m.pointer());

			// Make sure we set the first texture matrix
			CacheHandler->setActiveTexture(0);

			Transformation3DChanged = false;
		}
		if (!OverrideMaterial2DEnabled)
		{
			setBasicRenderStates(InitMaterial2D, LastMaterial, true);
			LastMaterial = InitMaterial2D;
		}
	CacheHandler->setBlendFuncIndexed(CacheHandler->get2DView(),BGFX_STATE_BLEND_SRC_ALPHA,BGFX_STATE_BLEND_INV_SRC_ALPHA);
	}
	CacheHandler->setBlend(true);
	if (OverrideMaterial2DEnabled)
	{
		OverrideMaterial2D.Lighting=false;
		setBasicRenderStates(OverrideMaterial2D, LastMaterial, false);
		LastMaterial = OverrideMaterial2D;
	}

	// no alphaChannel without texture
	alphaChannel &= texture;

	if (alphaChannel || alpha)
	{
		CacheHandler->setBlend(true);
		CacheHandler->setAlphaTest(true);
		CacheHandler->setAlphaFunc(true, 0.f);
	}
	else
	{
		CacheHandler->setBlend(false);
		CacheHandler->setAlphaTest(false);
	}

	if (texture)
	{
		if (OverrideMaterial2DEnabled)
			setTextureRenderStates(OverrideMaterial2D, false);
		else
			setTextureRenderStates(InitMaterial2D, false);

		Material.setTexture(0, const_cast<CBgfxTexture*>(CacheHandler->getTextureCache()[0]));
		setTransform(ETS_TEXTURE_0, core::IdentityMatrix);
		// Due to the transformation change, the previous line would call a reset each frame
		// but we can safely reset the variable as it was false before
		Transformation3DChanged=false;
	}
	CurrentRenderMode = ERM_2D;
}

//! \return Returns the name of the video driver.
const wchar_t* CBgfxDriver::getName() const
{
	return Name.c_str();
}


////! deletes all dynamic lights there are
//void CBgfxDriver::deleteAllDynamicLights()
//{
////	for (s32 i=0; i<MaxLights; ++i)
////		glDisable(GL_LIGHT0 + i);
////
////	RequestedLights.clear();
//
//	CNullDriver::deleteAllDynamicLights();
//}


//! adds a dynamic light
//s32 CBgfxDriver::addDynamicLight(const SLight& light)
//{
//	CNullDriver::addDynamicLight(light);
//
//	RequestedLights.push_back(RequestedLight(light));
//
//	u32 newLightIndex = RequestedLights.size() - 1;
//
//	// Try and assign a hardware light just now, but don't worry if I can't
//	assignHardwareLight(newLightIndex);
//
//	return (s32)newLightIndex;
//}


//void COpenGLDriver::assignHardwareLight(u32 lightIndex)
//{
//
//}

void CBgfxDriver::setViewPort(const core::rect<s32>& area)
{
	core::rect<s32> vp = area;
	core::rect<s32> rendert(0, 0, getCurrentRenderTargetSize().Width, getCurrentRenderTargetSize().Height);
	vp.clipAgainst(rendert);

	if (vp.getHeight() > 0 && vp.getWidth() > 0)
		CacheHandler->setViewport(vp.UpperLeftCorner.X, getCurrentRenderTargetSize().Height - vp.UpperLeftCorner.Y - vp.getHeight(), vp.getWidth(), vp.getHeight());

	ViewPort = vp;
}

//! Draws a 3d line.
void CBgfxDriver::draw3DLine(const core::vector3df& start,
				const core::vector3df& end, SColor color)
{
	static uint16_t lastDrawView = 0;
	static bool firstDraw = true;

	setRenderStates3DMode();

	Quad2DVertices[0].Color = color;
	Quad2DVertices[1].Color = color;

	Quad2DVertices[0].Pos = core::vector3df((f32)start.X, (f32)start.Y, (f32)start.Z);
	Quad2DVertices[1].Pos = core::vector3df((f32)end.X, (f32)end.Y, (f32)end.Z);

	const u16 IndxArr[2] = {0,1};


	if((2 == bgfx::getAvailTransientVertexBuffer(2,*irrToBgfx.standardVDecl)) && (2 == bgfx::getAvailTransientIndexBuffer(2)))
	{
		bgfx::TransientVertexBuffer transientVBuffer2DImage;
		bgfx::allocTransientVertexBuffer(&transientVBuffer2DImage, 2, *irrToBgfx.standardVDecl);
		memmove(transientVBuffer2DImage.data,Quad2DVertices,2*sizeof(S3DVertex));
		bgfx::setVertexBuffer(0,&transientVBuffer2DImage);
		bgfx::TransientIndexBuffer transientIndexBuffer;
		bgfx::allocTransientIndexBuffer(&transientIndexBuffer,2);
		memmove(transientIndexBuffer.data,IndxArr,2*sizeof(u16));
		bgfx::setIndexBuffer(&transientIndexBuffer);
		bgfx::setState(0 | BGFX_STATE_PT_LINES | CacheHandler->getBgfxStateFlag(CacheHandler->getActiveView()));

		if (firstDraw || lastDrawView != CacheHandler->getActiveView())
		{
			//std::cerr << "\tCBgfxDriver::draw3DLine  submitting a line to view(" << static_cast<int>(CacheHandler->getActiveView()) << ")" << std::endl;
			firstDraw = false;
			lastDrawView = CacheHandler->getActiveView();
		}
		bgfx::submit(CacheHandler->getActiveView(), BasicProgramHandle);
	}

}


//! Removes a texture from the texture cache and deletes it, freeing lot of memory.
void CBgfxDriver::removeTexture(ITexture* texture)
{
	if (!texture)
		return;

	CNullDriver::removeTexture(texture);
}


//! Only used by the internal engine. Used to notify the driver that
//! the window was resized.
void CBgfxDriver::OnResize(const core::dimension2d<u32>& size)
{
	CNullDriver::OnResize(size);
	CacheHandler->setViewport(0, 0, size.Width, size.Height);
	//We need to atleast make sure to reset the currently active view and the 2D-View
	//std::cerr << "CBgfxDriver::OnResize setting view rect for view(0) to (0, 0, " << size.Width << ", " << size.Height << ")" << std::endl;
	bgfx::setViewRect(0,0,0,size.Width, size.Height);
	//std::cerr << "CBgfxDriver::OnResize setting view rect for 2DView(" << static_cast<int>(CacheHandler->get2DView()) << ") to (0, 0, " << size.Width << ", " << size.Height << ")" << std::endl;
	bgfx::setViewRect(CacheHandler->get2DView(),0,0,size.Width, size.Height);
	bgfx::reset(size.Width,size.Height,BgfxResetOptionFlags);
	Transformation3DChanged = true;
}


//! Returns type of video driver
E_DRIVER_TYPE CBgfxDriver::getDriverType() const
{
	return this->Params.DriverType;
}


//! returns color format
ECOLOR_FORMAT CBgfxDriver::getColorFormat() const
{
	return ColorFormat;
}


//! Get a vertex shader constant index.
s32 CBgfxDriver::getVertexShaderConstantID(const c8* name)
{
	return getPixelShaderConstantID(name);
}

//! Get a pixel shader constant index.
s32 CBgfxDriver::getPixelShaderConstantID(const c8* name)
{
	return CacheHandler->getShaderConstantID(name);
}

//! Sets a vertex shader constant.
void CBgfxDriver::setVertexShaderConstant(const f32* data, s32 startRegister, s32 constantAmount)
{
	//No real Method to set random float array data in bgfx
	os::Printer::log("Error: BGFX doesn't expose the possibility to set random user data.");
}

//! Sets a pixel shader constant.
void CBgfxDriver::setPixelShaderConstant(const f32* data, s32 startRegister, s32 constantAmount)
{
	//No real Method to set random float array data in bgfx
	os::Printer::log("Error: BGFX doesn't expose the possibility to set random user data.");
}

//! Sets a constant for the vertex shader based on an index.
bool CBgfxDriver::setVertexShaderConstant(s32 index, const f32* floats, int count)
{
	//pass this along, as in BGFX the same routine is used for both vertex and fragment shaders
	return setPixelShaderConstant(index, floats, count);
}

//! Int interface for the above.
bool CBgfxDriver::setVertexShaderConstant(s32 index, const s32* ints, int count)
{
	return setPixelShaderConstant(index, ints, count);
}

//! Sets a constant for the pixel shader based on an index.
bool CBgfxDriver::setPixelShaderConstant(s32 index, const f32* floats, int count)
{
    if (index < 0 || index + 1 > this->CacheHandler->UniformInfo.size()) {
        return false;
    }

    CBgfxCacheHandler::SUniformInfo& uniform = this->CacheHandler->UniformInfo[index];
    if (!bgfx::isValid(uniform.handle)) {
        os::Printer::log((core::stringc("Error: bgfx Driver: shader constant with id '") + core::stringc(index) + \
                          core::stringc("' wasn't created. Make sure it is in 'constantNames' when calling VideoDriver->addHighLevelShaderMaterial!")).c_str(), ELL_ERROR);
        return false;
    }

    bgfx::setUniform(uniform.handle, floats, UINT16_MAX);
    return true;
}

//! Int interface for the above.
bool CBgfxDriver::setPixelShaderConstant(s32 index, const s32* ints, int count)
{
	os::Printer::log("Error: the bgfx driver only support float typed shader constants.");
	return false;
}

//! Adds a new material renderer to the VideoDriver, using pixel and/or
//! vertex shaders to render geometry.
s32 CBgfxDriver::addShaderMaterial(const c8* vertexShaderProgram,
	const c8* pixelShaderProgram,
	IShaderConstantSetCallBack* callback,
	E_MATERIAL_TYPE baseMaterial, s32 userData)
{
	os::Printer::log("Error: use the bgfx variant of addHighLevelShaderMaterial to load precompiled bgfx shaders", ELL_ERROR);
    return -1;
}


//! Adds a new material renderer to the VideoDriver, using GLSL to render geometry.
s32 CBgfxDriver::addHighLevelShaderMaterial(
	const c8* vertexShaderProgram,
	const c8* vertexShaderEntryPointName,
	E_VERTEX_SHADER_TYPE vsCompileTarget,
	const c8* pixelShaderProgram,
	const c8* pixelShaderEntryPointName,
	E_PIXEL_SHADER_TYPE psCompileTarget,
	const c8* geometryShaderProgram,
	const c8* geometryShaderEntryPointName,
	E_GEOMETRY_SHADER_TYPE gsCompileTarget,
	scene::E_PRIMITIVE_TYPE inType,
	scene::E_PRIMITIVE_TYPE outType,
	u32 verticesOut,
	IShaderConstantSetCallBack* callback,
	E_MATERIAL_TYPE baseMaterial,
	s32 userData, E_GPU_SHADING_LANGUAGE shadingLang)
{
	os::Printer::log("Error: use the bgfx variant of addHighLevelShaderMaterial to load precompiled bgfx shaders", ELL_ERROR);
    return -1;
}

s32 CBgfxDriver::addHighLevelShaderMaterial(
                const uint8_t* vertexShaderProgram,
                const u32 vertexShaderProgramSize,
                const c8* vertexShaderEntryPointName,
                E_VERTEX_SHADER_TYPE vsCompileTarget,
                const uint8_t* pixelShaderProgram,
                const u32 pixelShaderProgramSize,
                const c8* pixelShaderEntryPointName,
                E_PIXEL_SHADER_TYPE psCompileTarget,
                const uint8_t* geometryShaderProgram,
                const u32 geometryShaderProgramSize,
                const c8* geometryShaderEntryPointName,
                E_GEOMETRY_SHADER_TYPE gsCompileTarget,
                scene::E_PRIMITIVE_TYPE inType,
                scene::E_PRIMITIVE_TYPE outType,
                u32 verticesOut,
                IShaderConstantSetCallBack* callback,
				const core::array<core::stringc> constantNames,
				const core::array<u32> constantSizes,
                E_MATERIAL_TYPE baseMaterial,
                s32 userData,
                E_GPU_SHADING_LANGUAGE shadingLang)
{
    s32 nr = -1;

    if (geometryShaderProgram != nullptr || geometryShaderProgramSize > 0) {
		os::Printer::log("Fatal Error: geometry shaders aren't currently supported with the bgfx driver.", ELL_ERROR);
        return nr;
    }

	//As a quick approach we don't need that many options at the moment
    //TODO: add more options to this

	CBgfxShaderMaterialRenderer* r = new CBgfxShaderMaterialRenderer(
			this, nr, vertexShaderProgram, vertexShaderProgramSize, pixelShaderProgram, pixelShaderProgramSize,
			callback, baseMaterial, userData, constantNames, constantSizes);

	r->drop();

	return nr;
}

//! Returns a pointer to the IVideoDriver interface. (Implementation for
//! IMaterialRendererServices)
IVideoDriver* CBgfxDriver::getVideoDriver()
{
	return this;
}


ITexture* CBgfxDriver::addRenderTargetTexture(const core::dimension2d<u32>& size,
	const io::path& name, const ECOLOR_FORMAT format)
{
	//disable mip-mapping
	bool generateMipLevels = getTextureCreationFlag(ETCF_CREATE_MIP_MAPS);
	setTextureCreationFlag(ETCF_CREATE_MIP_MAPS, false);

	bool supportForFBO = (Feature->ColorAttachment > 0);

	core::dimension2du destSize(size);

	if (!supportForFBO)
	{
		destSize = core::dimension2d<u32>(core::min_(size.Width, ScreenSize.Width), core::min_(size.Height, ScreenSize.Height));
		destSize = destSize.getOptimalSize((size == size.getOptimalSize()), false, false);
	}

	CBgfxTexture* renderTargetTexture = new CBgfxTexture(name, destSize, format, this);
	addTexture(renderTargetTexture);
	renderTargetTexture->drop();

	//restore mip-mapping
	setTextureCreationFlag(ETCF_CREATE_MIP_MAPS, generateMipLevels);

	return renderTargetTexture;
}

ITexture* CBgfxDriver::addRenderTargetTexture(const core::dimension2d<u32>& size,
	const io::path& name, const ECOLOR_FORMAT format, const E_BGFX_TEXTURE_COMPARE_FLAGS compareFlag)
{
	//disable mip-mapping
	bool generateMipLevels = getTextureCreationFlag(ETCF_CREATE_MIP_MAPS);
	setTextureCreationFlag(ETCF_CREATE_MIP_MAPS, false);

	bool supportForFBO = (Feature->ColorAttachment > 0);

	core::dimension2du destSize(size);

	if (!supportForFBO)
	{
		destSize = core::dimension2d<u32>(core::min_(size.Width, ScreenSize.Width), core::min_(size.Height, ScreenSize.Height));
		destSize = destSize.getOptimalSize((size == size.getOptimalSize()), false, false);
	}

	CBgfxTexture* renderTargetTexture = new CBgfxTexture(name, destSize, format, this,compareFlag);
	addTexture(renderTargetTexture);
	renderTargetTexture->drop();

	//restore mip-mapping
	setTextureCreationFlag(ETCF_CREATE_MIP_MAPS, generateMipLevels);

	return renderTargetTexture;

}


//! Returns the maximum amount of primitives (mostly vertices) which
//! the device is able to render with one drawIndexedTriangleList
//! call.
u32 CBgfxDriver::getMaximalPrimitiveCount() const
{
	return 0x7fffffff;
}

bool CBgfxDriver::setRenderTarget(ITexture* texture, u16 clearFlag, SColor clearColor, f32 clearDepth, u8 clearStencil)
{
	if(!texture)
	{
		return setRenderTargetEx(nullptr, clearFlag, clearColor, clearDepth, clearStencil);
	}
	auto ret = textureToRT.find(texture);
	if(ret != textureToRT.end())
	{
		size_t idx = textureToRT[texture];
		auto rtInfo = rtInfoBuffer[idx];
		rtInfo.frameLastUsed = currFrame;

		return setRenderTargetEx(rtInfo.target, clearFlag, clearColor, clearDepth, clearStencil);
	}
	else
	{
		// texture is not bound to a rendertarget yet -> create new rendertarget

		if(unusedRTBufferIdx.empty())
		{
			expandRTInfoBuffer(20);
		}

		auto rtInfo = RenderTargetInfo();
		rtInfo.target = addRenderTarget();
		rtInfo.target->setTexture(texture,nullptr); //since we only passed a texture, set depthstencil texture to nullptr
		rtInfo.frameLastUsed = currFrame;

		auto idx = unusedRTBufferIdx.top();
		unusedRTBufferIdx.pop();
		rtInfoBuffer[idx] = rtInfo;
		textureToRT[texture] = idx;
		
		return setRenderTargetEx(rtInfo.target, clearFlag, clearColor, clearDepth, clearStencil);
	}

}

void CBgfxDriver::expandRTInfoBuffer(size_t additionalSize)
{
	//we will only call this when unusedRTBufferIdx is empty
	auto oldSize = rtInfoBuffer.size();
	rtInfoBuffer.resize(oldSize + additionalSize);
	auto newSize = rtInfoBuffer.size();
	auto val = (oldSize+additionalSize);
	for(auto i = 1; i <= additionalSize; ++i)
	{
		unusedRTBufferIdx.push(additionalSize -i +oldSize);
	}
}

bool CBgfxDriver::setRenderTargetEx(IRenderTarget* target, u16 clearFlag, SColor clearColor, f32 clearDepth, u8 clearStencil)
{

	if (target && !this->isBgfxDriverType(target->getDriverType()))
	{
		os::Printer::log("Fatal Error: Tried to set a render target not owned by this driver.", ELL_ERROR);
		return false;
	}

	bool supportForFBO = true;
	CBgfxRenderTarget* renderTarget = static_cast<CBgfxRenderTarget*>(target);
	core::dimension2d<u32> destRenderTargetSize(0, 0);

	if (target && (renderTarget->getTexture()||renderTarget->getDepthStencil()))
	{
		if (supportForFBO)
		{
			s32 res = RenderTargets.binary_search(target);
			if(res==-1)
			{
				os::Printer::log("Fatal Error: Tried to set a render target not created with addRenderTarget().", ELL_ERROR);
				return false;
			}
            u32 renderTargetView = CacheHandler->getNextRenderTargetView();
			CacheHandler->setActiveView(renderTargetView); // render the targets in the same order as setRenderTarget() is called
            
			// the view might have been used by the user to render sequentially before being used as a rendertarget -> reset if still set.
			bgfx::setViewMode(renderTargetView, bgfx::ViewMode::Default);
			
			renderTarget->setFrameBufferToView(renderTargetView);
			bgfx::touch(CacheHandler->getActiveView());
			
            // prepare the next view
            CacheHandler->setNextRenderTargetView(renderTargetView + 1);
		}

		destRenderTargetSize = renderTarget->getSize();
		CacheHandler->setViewport(0, 0, destRenderTargetSize.Width, destRenderTargetSize.Height);
		
		clearBuffers(clearFlag, clearColor, clearDepth, clearStencil);
	}//if(target)
	else
	{
		if (supportForFBO)
		{
			CacheHandler->setActiveViewToPrevious();
		}

		// set Texture to nullptr to make sure new texture gets set correctly after rendering to
		// rendertarget
		CacheHandler->getTextureCache().set(0, nullptr);


		destRenderTargetSize = core::dimension2d<u32>(0, 0);

		CacheHandler->setViewport(0, 0, ScreenSize.Width, ScreenSize.Height);
	}// else(!target)

	if (CurrentRenderTargetSize != destRenderTargetSize)
	{
		CurrentRenderTargetSize = destRenderTargetSize;

		Transformation3DChanged = true;
	}

	CurrentRenderTarget = target;

	if (!supportForFBO)
	{
		clearFlag |= ECBF_COLOR;
		clearFlag |= ECBF_DEPTH;
	}//TODO: bgfx does not work without fbos - drop checks


	return true;
}


void CBgfxDriver::clearBuffers(u16 flag, SColor color, f32 depth, u8 stencil)
{

	if(flag == ECBF_NONE)
	{
		bgfx::setViewClear(CacheHandler->getActiveView(), BGFX_CLEAR_NONE);
		return;
	}

	u16 bgfxClearFlag = BGFX_CLEAR_NONE;

	if (flag & ECBF_COLOR)
	{
		CacheHandler->setColorMask(true, true, true, true);

		bgfxClearFlag |= BGFX_CLEAR_COLOR;
	}

	if (flag & ECBF_DEPTH)
	{
		CacheHandler->setDepthMask(true);
		bgfxClearFlag |= BGFX_CLEAR_DEPTH;
	}

	if (flag & ECBF_STENCIL)
	{

		bgfxClearFlag |= BGFX_CLEAR_STENCIL;
	}


	u32 clearRGBA = getRgbaFromArgb(color.color);

	if (bgfxClearFlag != BGFX_CLEAR_NONE)
	{
		bgfx::setViewClear(CacheHandler->getActiveView(), bgfxClearFlag, clearRGBA, depth, stencil);
	}
	bgfx::setViewClear(CacheHandler->get2DView(),BGFX_CLEAR_NONE); //resets the clear state of 2DView
}

//! Returns an image created from the last rendered frame.
IImage* CBgfxDriver::createScreenShot(video::ECOLOR_FORMAT format, video::E_RENDER_TARGET target)
{
	if (target != video::ERT_FRAME_BUFFER)
		return nullptr;
 //TODO: Not Implemented yet -> need to write costum callback function
	return nullptr;
}


//! Set/unset a clipping plane.
bool CBgfxDriver::setClipPlane(u32 index, const core::plane3df& plane, bool enable)
{
	//if (index >= MaxUserClipPlanes)
		//return false;

	UserClipPlanes[index].Plane=plane;
	enableClipPlane(index, enable);
	return true;
}


void CBgfxDriver::uploadClipPlane(u32 index)
{
	// opengl needs an array of doubles for the plane equation
/*	GLdouble clip_plane[4];
	clip_plane[0] = UserClipPlanes[index].Plane.Normal.X;
	clip_plane[1] = UserClipPlanes[index].Plane.Normal.Y;
	clip_plane[2] = UserClipPlanes[index].Plane.Normal.Z;
	clip_plane[3] = UserClipPlanes[index].Plane.D;
	glClipPlane(GL_CLIP_PLANE0 + index, clip_plane);*/
	//TODO: check for clipping in bgfx
}


//! Enable/disable a clipping plane.
void CBgfxDriver::enableClipPlane(u32 index, bool enable)
{
	//if (index >= MaxUserClipPlanes)
		return;
	/*if (enable)
	{
		if (!UserClipPlanes[index].Enabled)
		{
			uploadClipPlane(index);
			glEnable(GL_CLIP_PLANE0 + index);
		}
	}
	else
		glDisable(GL_CLIP_PLANE0 + index);

	UserClipPlanes[index].Enabled=enable;*/

}


core::dimension2du CBgfxDriver::getMaxTextureSize() const
{
	return core::dimension2du(bgfx::getCaps()->limits.maxTextureSize, bgfx::getCaps()->limits.maxTextureSize);
}


//! Convert E_PRIMITIVE_TYPE to OpenGL equivalent
uint64_t CBgfxDriver::primitiveTypeToBgfx(scene::E_PRIMITIVE_TYPE type) const
{
	switch (type)
	{
		case scene::EPT_POINTS:
			return BGFX_STATE_PT_POINTS;
		case scene::EPT_LINE_STRIP:
			return BGFX_STATE_PT_LINESTRIP;
		case scene::EPT_LINE_LOOP:
			return BGFX_STATE_PT_LINESTRIP; //There is no LineLoop in BGFX -> deprecated
		case scene::EPT_LINES:
			return BGFX_STATE_PT_LINES;
		case scene::EPT_TRIANGLE_STRIP:
			return BGFX_STATE_PT_TRISTRIP;
		case scene::EPT_TRIANGLE_FAN:
			return BGFX_STATE_PT_TRISTRIP; // no Triangle_Fan in BGFX -> deprecated
		case scene::EPT_TRIANGLES:
			return 0;
		case scene::EPT_QUAD_STRIP:
			return 0;
		case scene::EPT_QUADS:
			return 0;
		case scene::EPT_POLYGON:
			return 0;
		case scene::EPT_POINT_SPRITES:
			return 0;
		default:
			return 0;
	}
}


uint64_t CBgfxDriver::getBgfxBlend(E_BLEND_FACTOR factor) const
{
	uint64_t r = 0;
	switch (factor)
	{
		case EBF_ZERO:			r = BGFX_STATE_BLEND_ZERO; break;
		case EBF_ONE:			r = BGFX_STATE_BLEND_ONE; break;
		case EBF_DST_COLOR:		r = BGFX_STATE_BLEND_DST_COLOR; break;
		case EBF_ONE_MINUS_DST_COLOR:	r = BGFX_STATE_BLEND_INV_DST_COLOR; break;
		case EBF_SRC_COLOR:		r = BGFX_STATE_BLEND_SRC_COLOR; break;
		case EBF_ONE_MINUS_SRC_COLOR:	r = BGFX_STATE_BLEND_INV_SRC_COLOR; break;
		case EBF_SRC_ALPHA:		r = BGFX_STATE_BLEND_SRC_ALPHA; break;
		case EBF_ONE_MINUS_SRC_ALPHA:	r = BGFX_STATE_BLEND_INV_SRC_ALPHA; break;
		case EBF_DST_ALPHA:		r = BGFX_STATE_BLEND_DST_ALPHA; break;
		case EBF_ONE_MINUS_DST_ALPHA:	r = BGFX_STATE_BLEND_INV_DST_ALPHA; break;
		case EBF_SRC_ALPHA_SATURATE:	r = BGFX_STATE_BLEND_SRC_ALPHA_SAT; break;
	}
	return r;

}


const SMaterial& CBgfxDriver::getCurrentMaterial() const
{
	return Material;
}

CBgfxCacheHandler* CBgfxDriver::getCacheHandler() const
{
	return CacheHandler;
}

CBgfxCoreFeature& CBgfxDriver::getFeature() const
{
	return *Feature;
}

void CBgfxDriver::setBgfxDebugMode(uint32_t debugFlag)
{
	uint32_t bgfxFlag = BGFX_DEBUG_NONE;
	bgfxFlag |= (E_BGFX_DEBUG_FLAGS::E_IFH_TEST & debugFlag ? BGFX_DEBUG_IFH : 0) | 
				(E_BGFX_DEBUG_FLAGS::E_STATS & debugFlag ? BGFX_DEBUG_STATS : 0)|
				(E_BGFX_DEBUG_FLAGS::E_TEXT & debugFlag ? BGFX_DEBUG_TEXT : 0)|
				(E_BGFX_DEBUG_FLAGS::E_WIREFRAME & debugFlag ? BGFX_DEBUG_WIREFRAME :0);
	
	bgfx::setDebug(bgfxFlag);
}


IComputeShader* CBgfxDriver::createBgfxComputeShader()
{
	return new CBgfxComputeShader();
}
IBgfxBuffer* CBgfxDriver::createBgfxBuffer(bool dynamic)
{
	return new CBgfxBuffer(dynamic);
}

void CBgfxDriver::setBgfxBuffer(IBgfxBuffer* buffer)
{
	CBgfxBuffer* bgfxBuffer = (CBgfxBuffer*)(buffer);
	//Bgfx requires to select the number of the vertexstream now
	//without any use cases yet we set it to 0 to stay compatible
	uint8_t vertexStream = 0;
	if (bgfxBuffer->isDynamic())
	{
		if(bgfx::isValid(bgfxBuffer->getDynamicVertexBuffer()))
			bgfx::setVertexBuffer(vertexStream,bgfxBuffer->getDynamicVertexBuffer(), 0, bgfxBuffer->getVertexCount());
		if (bgfx::isValid(bgfxBuffer->getDynamicIndexBuffer()))
			bgfx::setIndexBuffer(bgfxBuffer->getDynamicIndexBuffer(), 0, bgfxBuffer->getIndexCount());
	}
	else
	{
		if (bgfx::isValid(bgfxBuffer->getVertexBuffer()))
			bgfx::setVertexBuffer(vertexStream,bgfxBuffer->getVertexBuffer(), 0, bgfxBuffer->getVertexCount());
		if (bgfx::isValid(bgfxBuffer->getIndexBuffer()))
			bgfx::setIndexBuffer(bgfxBuffer->getIndexBuffer(), 0, bgfxBuffer->getIndexCount());
	}
	
}

void CBgfxDriver::setComputeBuffer(unsigned char stage, IBgfxBuffer* buffer, E_BGFX_COMPUTE_ACCESS_FLAGS flag)
{
	CBgfxBuffer* bgfxBuffer = (CBgfxBuffer*)(buffer);
	if (bgfxBuffer->isDynamic())
	{
		if (bgfx::isValid(bgfxBuffer->getDynamicVertexBuffer()))
			bgfx::setBuffer(stage,bgfxBuffer->getDynamicVertexBuffer(), bgfx::Access::Enum(flag));
		if (bgfx::isValid(bgfxBuffer->getDynamicIndexBuffer()))
			bgfx::setBuffer(stage,bgfxBuffer->getDynamicIndexBuffer(), bgfx::Access::Enum(flag));
	}
	else
	{
		if (bgfx::isValid(bgfxBuffer->getVertexBuffer()))
			bgfx::setBuffer(stage,bgfxBuffer->getVertexBuffer(), bgfx::Access::Enum(flag));
		if (bgfx::isValid(bgfxBuffer->getIndexBuffer()))
			bgfx::setBuffer(stage,bgfxBuffer->getIndexBuffer(), bgfx::Access::Enum(flag));
	}
}

void CBgfxDriver::setInstanceBuffer(IBgfxBuffer* buffer, unsigned int startVertex, unsigned int numOfVertices)
{
	CBgfxBuffer* bgfxBuffer = (CBgfxBuffer*)(buffer);
	if (bgfxBuffer->isDynamic())
	{
		if (bgfx::isValid(bgfxBuffer->getDynamicVertexBuffer()))
			bgfx::setInstanceDataBuffer(bgfxBuffer->getDynamicVertexBuffer(),startVertex,numOfVertices);
	}
	else
	{
		if (bgfx::isValid(bgfxBuffer->getVertexBuffer()))
			bgfx::setInstanceDataBuffer(bgfxBuffer->getVertexBuffer(), startVertex, numOfVertices);
	}
}

}//Namespace video
} //Namespace irr

#endif

namespace irr
{
namespace video
{

#if defined(_IRR_COMPILE_WITH_WINDOWS_DEVICE_) || defined(_IRR_COMPILE_WITH_X11_DEVICE_) || defined(_IRR_COMPILE_WITH_OSX_DEVICE_)
	IVideoDriver* createBgfxDriver(const SIrrlichtCreationParameters& params, io::IFileSystem* io, IContextManager* contextManager)
	{
#ifdef _IRR_COMPILE_WITH_BGFX_
		CBgfxDriver* bgfx = new CBgfxDriver(params, io, contextManager);
		if (!bgfx->initDriver())
		{
			bgfx->drop();
			bgfx = nullptr;
		}

		return bgfx;
#else
		return nullptr;
#endif
	}
#endif

} //Namespace video
} //Namespace irr
