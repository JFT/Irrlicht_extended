// Copyright (C) 2017 Julius Tilly
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h
#include "CBgfxComputeShader.h"
#include <iostream>
#include "CBgfxBuffer.h"
namespace irr 
{

namespace video
{

CBgfxComputeShader::CBgfxComputeShader():
	buffers(std::vector<ComputeBuffer>())
{
	
}
CBgfxComputeShader::~CBgfxComputeShader()
{

}

void CBgfxComputeShader::createShaderProgram(const uint8_t * computeShaderCode, const uint32_t csSize)
{
	// add shader code to bgfx memory
	bgfx::ShaderHandle temporaryComputeShader = BGFX_INVALID_HANDLE;
	const bgfx::Memory* computeShaderMemory = bgfx::copy(computeShaderCode, csSize);
	temporaryComputeShader = bgfx::createShader(computeShaderMemory);

	if (!bgfx::isValid(temporaryComputeShader)) {
		std::cerr << "Failed to compile shader!" << std::endl;
		return;
	}

	computeShaderProgram = bgfx::createProgram(temporaryComputeShader, true);
}

void CBgfxComputeShader::addBgfxBuffer(uint8_t stage,IBgfxBuffer *buffer, E_BGFX_COMPUTE_ACCESS_FLAGS accessFlag)
{
	CBgfxBuffer* bgfxBuffer = (CBgfxBuffer*)(buffer);
	if (bgfxBuffer->isDynamic())
	{
		if (bgfx::isValid(bgfxBuffer->getDynamicVertexBuffer()))
		{
			this->addBuffer(stage, bgfxBuffer->getDynamicVertexBuffer(), accessFlag);
			stage++;
		}

		if (bgfx::isValid(bgfxBuffer->getDynamicIndexBuffer()))
			this->addBuffer(stage, bgfxBuffer->getDynamicIndexBuffer(), accessFlag);
	}
	else //static
	{
		if (bgfx::isValid(bgfxBuffer->getVertexBuffer()))
		{
			this->addBuffer(stage, bgfxBuffer->getVertexBuffer(), accessFlag);
			stage++;
		}			
		if (bgfx::isValid(bgfxBuffer->getIndexBuffer()))
			this->addBuffer(stage, bgfxBuffer->getIndexBuffer(), accessFlag);
	}

}

void CBgfxComputeShader::addBuffer(uint8_t stage, bgfx::DynamicIndexBufferHandle bufferHandle, E_BGFX_COMPUTE_ACCESS_FLAGS accessFlag)
{
	if (stage >= buffers.size())
	{
		buffers.resize(stage + 1);
	}
	buffers[stage] = ComputeBuffer(ComputeBuffer::E_BUFFERTYPE::DYNAMIC_INDEX, bufferHandle.idx, accessFlag);
}

void CBgfxComputeShader::addBuffer(uint8_t stage, bgfx::DynamicVertexBufferHandle bufferHandle, E_BGFX_COMPUTE_ACCESS_FLAGS accessFlag)
{
	if (stage >= buffers.size())
	{
		buffers.resize(stage + 1);
	}
	buffers[stage] = ComputeBuffer(ComputeBuffer::E_BUFFERTYPE::DYNAMIC_VERTEX, bufferHandle.idx, accessFlag);
}
void CBgfxComputeShader::addBuffer(uint8_t stage, bgfx::IndexBufferHandle bufferHandle, E_BGFX_COMPUTE_ACCESS_FLAGS accessFlag)
{
	if (stage >= buffers.size())
	{
		buffers.resize(stage + 1);
	}
	buffers[stage] = ComputeBuffer(ComputeBuffer::E_BUFFERTYPE::STATIC_INDEX, bufferHandle.idx, accessFlag);
}
void CBgfxComputeShader::addBuffer(uint8_t stage, bgfx::VertexBufferHandle bufferHandle, E_BGFX_COMPUTE_ACCESS_FLAGS accessFlag)
{
	if (stage >= buffers.size())
	{
		buffers.resize(stage + 1);
	}
	buffers[stage] = ComputeBuffer(ComputeBuffer::E_BUFFERTYPE::STATIC_VERTEX, bufferHandle.idx, accessFlag);
}

void CBgfxComputeShader::addUniform(const char * name, void * const value, uint16_t numOfElements)
{
	uniforms.emplace_back(UniformInformation(name, bgfx::createUniform(name, bgfx::UniformType::Vec4, numOfElements), value));
}

void CBgfxComputeShader::addUniform(const char * name, uint16_t numOfElements)
{
	uniforms.emplace_back(UniformInformation(name, bgfx::createUniform(name, bgfx::UniformType::Vec4, numOfElements), nullptr));
}

void CBgfxComputeShader::setUniform(const char * name, void *const value)
{
	for (size_t i=0; i<uniforms.size(); ++i)
	{
		if (uniforms[i].uniformName == std::string(name))
		{
			uniforms[i].data = value;
		}
	}
}

void CBgfxComputeShader::resetUniformCache()
{
	for (auto& uniform : uniforms)
	{
		if(bgfx::isValid(uniform.uniformHandle))
			bgfx::destroy(uniform.uniformHandle);
	}
	uniforms.clear();
	
}

void CBgfxComputeShader::resetBufferCache()
{
	buffers.clear();
}



void CBgfxComputeShader::run(uint8_t view, uint16_t groupsX, uint16_t groupsY, uint16_t groupsZ)
{
	for(size_t i = 0;i<buffers.size();++i)
	{
		switch (buffers[i].bufferType)
		{
			case ComputeBuffer::E_BUFFERTYPE::DYNAMIC_INDEX:
				bgfx::setBuffer(i, bgfx::DynamicIndexBufferHandle{ buffers[i].handle }, convertIrrAccessFlagToBgfx(buffers[i].computeAccessFlag));
				break;
			case ComputeBuffer::E_BUFFERTYPE::DYNAMIC_VERTEX:
				bgfx::setBuffer(i, bgfx::DynamicVertexBufferHandle{ buffers[i].handle }, convertIrrAccessFlagToBgfx(buffers[i].computeAccessFlag));
				break;
			case ComputeBuffer::E_BUFFERTYPE::STATIC_INDEX:
				bgfx::setBuffer(i, bgfx::IndexBufferHandle{ buffers[i].handle }, convertIrrAccessFlagToBgfx(buffers[i].computeAccessFlag));
				break;
			case ComputeBuffer::E_BUFFERTYPE::STATIC_VERTEX:
				bgfx::setBuffer(i, bgfx::VertexBufferHandle{ buffers[i].handle }, convertIrrAccessFlagToBgfx(buffers[i].computeAccessFlag));
				break;
			default:
				continue;
				break;
		}
	}

	for (auto& uniform : uniforms)
	{
		//passing 'UINT16_MAX' makes sure the same number of Elements is set as in the createUniform call
		bgfx::setUniform(uniform.uniformHandle, uniform.data, UINT16_MAX);
	}

	bgfx::dispatch(view, computeShaderProgram, groupsX, groupsY, groupsZ);
}

CBgfxComputeShader * CBgfxComputeShader::getNewInstance()
{
	return new CBgfxComputeShader(*this);
}

CBgfxComputeShader::CBgfxComputeShader(const CBgfxComputeShader& cS): buffers(std::vector<ComputeBuffer>())
{
	computeShaderProgram = cS.computeShaderProgram;
	for (auto uniform : cS.uniforms)
	{
		uniforms.push_back(uniform);
	}
}

}//video
}//irr
