// Copyright (C) 2017 Julius Tilly
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h
#include "CBgfxBuffer.h"
#include "os.h"
#include <iostream>

namespace irr
{

namespace video
{

CBgfxBuffer::CBgfxBuffer(bool dynamic_): dynamic(dynamic_)
{

}


CBgfxBuffer::~CBgfxBuffer()
{
	destroyBuffers();
}

void CBgfxBuffer::addVertexAttribute(E_BGFX_VERTEX_ATTRIBUTE attrib, uint8_t numberOfElements, E_BGFX_ATTRIBUTE_TYPE attribType, bool normalize, bool asInt)
{
	if (firstAttribute)
	{
		vDecl = vDecl.begin();
		firstAttribute = false;
	}
	
	vDecl = vDecl.add(static_cast<bgfx::Attrib::Enum>(attrib), numberOfElements, static_cast<bgfx::AttribType::Enum>(attribType), normalize, asInt);
	
}

void CBgfxBuffer::processVertexAttributes()
{
	vDecl.end();
}

void CBgfxBuffer::setVertexBuffer(const void * buffer, size_t vertexCount, size_t vertexSize,bool computeBuffer)
{
	if (dynamic)
	{
		if(!validHandle(vHandle))
		{
			bgfx::DynamicVertexBufferHandle vDynHandle = bgfx::createDynamicVertexBuffer(bgfx::copy(buffer, vertexCount*vertexSize), 
																						 vDecl, BGFX_BUFFER_ALLOW_RESIZE | (computeBuffer? BGFX_BUFFER_COMPUTE_READ_WRITE : BGFX_BUFFER_NONE));
			vHandle.idx = vDynHandle.idx;
		}
		else
		{
			bgfx::update(bgfx::DynamicVertexBufferHandle{ vHandle.idx }, 0, bgfx::copy(buffer, vertexCount * vertexSize));
		}
		
	}
	else
	{
		if(validHandle(vHandle))
		{
			os::Printer::log("Not allowed to update a static Vertexbuffer - use Dynamicbuffers!", ELOG_LEVEL::ELL_ERROR);
			return;
		}
		bgfx::VertexBufferHandle vStaticHandle = bgfx::createVertexBuffer(bgfx::copy(buffer, vertexCount*vertexSize), vDecl, (computeBuffer ? BGFX_BUFFER_COMPUTE_READ_WRITE : BGFX_BUFFER_NONE));
		vHandle.idx = vStaticHandle.idx;
	}
	this->vCount = vertexCount;

}

void CBgfxBuffer::setVertexBuffer(const size_t bufferSize, bool computeBuffer)
{
	if (dynamic)
	{
		if(validHandle(vHandle))
		{
			return;
		}
		bgfx::DynamicVertexBufferHandle vDynHandle = bgfx::createDynamicVertexBuffer(bufferSize, vDecl, BGFX_BUFFER_ALLOW_RESIZE | (computeBuffer ? BGFX_BUFFER_COMPUTE_READ_WRITE : BGFX_BUFFER_NONE));
		vHandle.idx = vDynHandle.idx;
		this->vCount = bufferSize;
	}
	else
	{
		os::Printer::log("Not allowed to generate a static Vertex Buffer with uninitialized data - use DynamicBuffers!", ELOG_LEVEL::ELL_ERROR);
	}

}


void CBgfxBuffer::setIndexBuffer(const void * buffer, size_t indexCount, E_INDEX_TYPE idxType, bool computeBuffer)
{
	size_t indexSize = idxType == E_INDEX_TYPE::EIT_16BIT ? sizeof(uint16_t) : sizeof(uint32_t);
	if (dynamic)
	{

		if(!validHandle(iHandle))
		{
			bgfx::DynamicIndexBufferHandle iDynHandle = bgfx::createDynamicIndexBuffer(bgfx::copy(buffer, indexCount*indexSize), BGFX_BUFFER_ALLOW_RESIZE | (idxType == E_INDEX_TYPE::EIT_16BIT? BGFX_BUFFER_NONE : BGFX_BUFFER_INDEX32)| (computeBuffer ? BGFX_BUFFER_COMPUTE_READ_WRITE : BGFX_BUFFER_NONE));
			iHandle.idx = iDynHandle.idx;
		}
		else
		{
			bgfx::update(bgfx::DynamicIndexBufferHandle{iHandle.idx}, 0, bgfx::copy(buffer, indexCount*indexSize));
		}
	}
	else
	{
		if(validHandle(iHandle))
		{
			os::Printer::log("Not allowed to update a static Indexbuffer - use Dynamicbuffers!", ELOG_LEVEL::ELL_ERROR);
			return;
		}
		bgfx::IndexBufferHandle iStaticHandle = bgfx::createIndexBuffer(bgfx::copy(buffer, indexCount*indexSize), (idxType == E_INDEX_TYPE::EIT_16BIT ? BGFX_BUFFER_NONE : BGFX_BUFFER_INDEX32) | (computeBuffer ? BGFX_BUFFER_COMPUTE_READ_WRITE : BGFX_BUFFER_NONE));
		iHandle.idx = iStaticHandle.idx;
	}
	this->iCount = indexCount;
}

void CBgfxBuffer::setIndexBuffer(const size_t bufferSize, E_INDEX_TYPE idxType, bool computeBuffer)
{
	size_t indexSize = idxType == E_INDEX_TYPE::EIT_16BIT ? sizeof(uint16_t) : sizeof(uint32_t);
	if (dynamic)
	{
		if(validHandle(iHandle))
		{
			return;
		}
		bgfx::DynamicIndexBufferHandle iDynHandle = bgfx::createDynamicIndexBuffer(bufferSize, BGFX_BUFFER_ALLOW_RESIZE | (idxType == E_INDEX_TYPE::EIT_16BIT ? BGFX_BUFFER_NONE : BGFX_BUFFER_INDEX32) | (computeBuffer ? BGFX_BUFFER_COMPUTE_READ_WRITE : BGFX_BUFFER_NONE));
		iHandle.idx = iDynHandle.idx;
		
		this->iCount = bufferSize;
	}
	else
	{
		os::Printer::log("Not allowed to generate a static Index Buffer with uninitialized data - use DynamicBuffers!", ELOG_LEVEL::ELL_ERROR);
	}
}

void CBgfxBuffer::destroyVertexBuffer()
{
	if (!isValid(vHandle))
		return;

	if (dynamic)
		bgfx::destroy(bgfx::DynamicVertexBufferHandle{ vHandle.idx });
	else
		bgfx::destroy(bgfx::VertexBufferHandle{ vHandle.idx });
}

void CBgfxBuffer::destroyIndexBuffer()
{
	if (!isValid(iHandle))
		return;

	if (dynamic)
		bgfx::destroy(bgfx::DynamicIndexBufferHandle{ vHandle.idx });
	else
		bgfx::destroy(bgfx::IndexBufferHandle{ vHandle.idx });
}

void CBgfxBuffer::destroyBuffers()
{
	destroyIndexBuffer();
	destroyVertexBuffer();
}

bgfx::DynamicVertexBufferHandle CBgfxBuffer::getDynamicVertexBuffer()
{
	return bgfx::DynamicVertexBufferHandle{vHandle.idx};
}

bgfx::DynamicIndexBufferHandle CBgfxBuffer::getDynamicIndexBuffer()
{
	return bgfx::DynamicIndexBufferHandle{ iHandle.idx };
}

bgfx::VertexBufferHandle CBgfxBuffer::getVertexBuffer()
{
	return bgfx::VertexBufferHandle{ vHandle.idx };
}

bgfx::IndexBufferHandle CBgfxBuffer::getIndexBuffer()
{
	return bgfx::IndexBufferHandle{ iHandle.idx };
}

}//video
}//irr
