OS=$1
SHADERC='./shadercRelease'

PLATFORM="linux"
SHADER_LANGUAGE="glsl"
if [ "x$OS" = "xWindows_NT" ]; then
    echo "building hlsl and glsl shaders on windows!"
    PLATFORM="windows"
fi

if [ -z "$SHADERC" ]; then
    echo "" >&2
    echo "ERROR:" >&2
    echo "\$SHADERC not set!" >&2
    echo "Missing variable" >&2
    echo "" >&2
    exit 1
fi

for SHADER_TYPE in v f; do
	PROFILE="140" # use glsl v1.30
    P="linux"
	for SHADER_FILE in $(find . -name '*_'$SHADER_TYPE's.sc'); do
        echo "compiling shader file = '$SHADER_FILE'"
        TARGET_FILE="$(dirname "$SHADER_FILE")"/"$(basename --suffix=.sc "$SHADER_FILE")"_"$SHADER_LANGUAGE"_bgfx.h
        $SHADERC --profile "$PROFILE" -f "$SHADER_FILE" -o "$TARGET_FILE" --type $SHADER_TYPE --platform $P -O 3 --bin2c $(basename --suffix=.sc "$SHADER_FILE")_"$SHADER_LANGUAGE"_bgfx || exit 2
    done
	
	if [ "x$PLATFORM" = "xwindows" ]; then
		SHADER_LANGUAGE="hlsl"
		if [ "x$SHADER_TYPE" = "xv" ]; then
			PROFILED9="vs_3_0" # use hlsl vertex shader v3.0
			PROFILED11="vs_5_0" # use hlsl vertex shader v5.0
		else
			PROFILED9="ps_3_0" # use hlsl pixel shader v3.0
			PROFILED11="ps_5_0" # use hlsl vertex shader v5.0
		fi
		P="windows"
		for SHADER_FILE in $(find . -name '*_'$SHADER_TYPE's.sc'); do
			echo "compiling shader file = '$SHADER_FILE'"
			TARGET_FILE_D9="$(dirname "$SHADER_FILE")"/"$(basename --suffix=.sc "$SHADER_FILE")"_"$SHADER_LANGUAGE"_DX9_bgfx.h
			TARGET_FILE_D11="$(dirname "$SHADER_FILE")"/"$(basename --suffix=.sc "$SHADER_FILE")"_"$SHADER_LANGUAGE"_DX11_bgfx.h
			$SHADERC --profile "$PROFILED9" -f "$SHADER_FILE" -o "$TARGET_FILE_D9" --type $SHADER_TYPE --platform $P -O 3 --bin2c $(basename --suffix=.sc "$SHADER_FILE")_"$SHADER_LANGUAGE"_DX9_bgfx || exit 2
			$SHADERC --profile "$PROFILED11" -f "$SHADER_FILE" -o "$TARGET_FILE_D11" --type $SHADER_TYPE --platform $P -O 3 --bin2c $(basename --suffix=.sc "$SHADER_FILE")_"$SHADER_LANGUAGE"_DX11_bgfx || exit 2
		done
		PROFILE="140"
		SHADER_LANGUAGE="glsl"
	fi
done
