$input v_color0, v_texcoord0, v_normal

#include "common.sh"

SAMPLER2D(texture0,  0);
uniform vec4 modulate;

void main()
{		
	vec4 texel = texture2D(texture0, v_texcoord0);
	vec4 color = vec4(modulate.x*texel.rgb*v_color0.rgb,1.0);
	color.a = texel.a*v_color0.a;
	gl_FragColor = color;
}
