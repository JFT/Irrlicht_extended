// Copyright (C) 2016 Julius Tilly
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_BGFXCORE_TEXTURE_H_INCLUDED__
#define __C_BGFXCORE_TEXTURE_H_INCLUDED__

#include "IrrCompileConfig.h"


#include "irrArray.h"
#include "SMaterialLayer.h"
#include "ITexture.h"
#include "EDriverFeatures.h"
#include "os.h"
#include "CImage.h"
#include "CColorConverter.h"

#include "CBgfxAndIrrlichtTypes.h"

#include <iostream>

namespace irr
{
namespace video
{

template <class TBgfxDriver>
class CBgfxCoreTexture : public ITexture
{
public:
	struct SStatesCache
	{
		SStatesCache() : WrapU(ETC_REPEAT), WrapV(ETC_REPEAT), WrapW(ETC_REPEAT),
			LODBias(0), AnisotropicFilter(0), BilinearFilter(false), TrilinearFilter(false),
			MipMapStatus(false), IsCached(false)
		{
		}

		u8 WrapU;
		u8 WrapV;
		u8 WrapW;
		s8 LODBias;
		u8 AnisotropicFilter;
		bool BilinearFilter;
		bool TrilinearFilter;
		bool MipMapStatus;
		bool IsCached;
	};

	CBgfxCoreTexture(const io::path& name, const core::array<IImage*>& image, E_TEXTURE_TYPE type, TBgfxDriver* driver) : ITexture(name, type), Driver(driver), BgfxTextureFormat(bgfx::TextureFormat::RGBA8), Converter(0), LockReadOnly(false), LockImage(0), LockLayer(0),
		KeepImage(false), AutoGenerateMipMaps(false)
	{
		_IRR_DEBUG_BREAK_IF(image.size() == 0)

		layerCount = image.size();
		DriverType = Driver->getDriverType();
		TextureType = Type;
		HasMipMaps = Driver->getTextureCreationFlag(ETCF_CREATE_MIP_MAPS); //TODO: implement Features + mipmap creation
		AutoGenerateMipMaps = Driver->queryFeature(EVDF_MIP_MAP_AUTO_UPDATE);
		KeepImage = Driver->getTextureCreationFlag(ETCF_ALLOW_MEMORY_COPY);
		//BGFX UNLOCK/LOCK is too different from irrlichts way:
		KeepImage = true;
		getImageValues(image[0]);

		const core::array<IImage*>* tmpImage = &image;

		if (KeepImage || OriginalSize != Size || OriginalColorFormat != ColorFormat)
		{
			Image.set_used(image.size());

			for (u32 i = 0; i < image.size(); ++i)
			{
				Image[i] = Driver->createImage(ColorFormat, Size);

				if (image[i]->getDimension() == Size)
					image[i]->copyTo(Image[i]);
				else
					image[i]->copyToScaling(Image[i]);
			}

			tmpImage = &Image;
		}

		BgfxTextureFormat = IrrToBgfx::getBgfxTextureFormatFromIrrlichtColorFormat(ColorFormat);

		switch(Type)
		{
		case ETT_2D:
			texHandle = bgfx::createTexture2D(Size.Width, Size.Height, HasMipMaps, 1, BgfxTextureFormat,0,NULL);
			break;
		case ETT_CUBEMAP:
			texHandle = bgfx::createTextureCube(Size.Width,HasMipMaps,1,BgfxTextureFormat,0,NULL);
			break;
		case ETT_ARRAY:
			texHandle = bgfx::createTexture2D(Size.Width,Size.Height,HasMipMaps,(*tmpImage).size(),BgfxTextureFormat,0,NULL);
			break;
		}

		const CBgfxCoreTexture* prevTexture = Driver->getCacheHandler()->getTextureCache().get(0);
		//Driver->getCacheHandler()->getTextureCache().set(0, this);


			for (u32 i = 0; i < (*tmpImage).size(); ++i)
				uploadTexture(i, 0, (*tmpImage)[i]->getData(),(*tmpImage)[i]->getImageDataSizeInBytes());

		//Driver->getCacheHandler()->getTextureCache().set(0, prevTexture);

		for (u32 i = 0; i < (*tmpImage).size(); ++i)
		{
			void* mipmapsData = (*tmpImage)[i]->getMipMapsData();
            if(HasMipMaps)
            {
                regenerateMipMapLevels(mipmapsData, i);
            }
		}

		if (!KeepImage)
		{
			for (u32 i = 0; i < Image.size(); ++i)
				Image[i]->drop();

			Image.clear();
		}

	}

	CBgfxCoreTexture(const io::path& name, const core::dimension2d<u32>& size, ECOLOR_FORMAT format, TBgfxDriver* driver, 
					 const E_BGFX_TEXTURE_COMPARE_FLAGS compareFlag = E_BGFX_TEXTURE_COMPARE_NONE) : 
		ITexture(name, ETT_2D), Driver(driver), BgfxTextureFormat(bgfx::TextureFormat::RGBA8),
		Converter(0), LockReadOnly(false), LockImage(0), LockLayer(0), KeepImage(true),
		AutoGenerateMipMaps(false)
	{
		layerCount = 1;
		DriverType = Driver->getDriverType();
		HasMipMaps = false;
		IsRenderTarget = true;

		OriginalColorFormat = format;

		if (ECF_UNKNOWN == OriginalColorFormat)
			ColorFormat = getBestColorFormat(Driver->getColorFormat());
		else
			ColorFormat = OriginalColorFormat;

		OriginalSize = size;
		Size = OriginalSize;

		Pitch = Size.Width * IImage::getBitsPerPixelFromFormat(ColorFormat) / 8;

		//Driver->getColorFormatParameters(ColorFormat, InternalFormat, PixelFormat, PixelType, &Converter);


		const CBgfxCoreTexture* prevTexture = Driver->getCacheHandler()->getTextureCache().get(0);
		//Driver->getCacheHandler()->getTextureCache().set(0, this);


		StatesCache.WrapU = ETC_CLAMP_TO_EDGE;
		StatesCache.WrapV = ETC_CLAMP_TO_EDGE;
		StatesCache.WrapW = ETC_CLAMP_TO_EDGE;

		BgfxTextureFormat = IrrToBgfx::getBgfxTextureFormatFromIrrlichtColorFormat(ColorFormat);

		texHandle = bgfx::createTexture2D(Size.Width, Size.Height,false,1,BgfxTextureFormat,
										  BGFX_TEXTURE_RT | IrrToBgfx::getBgfxCompareFlag(compareFlag),NULL);

		//Driver->getCacheHandler()->getTextureCache().set(0, prevTexture);
	}

	virtual ~CBgfxCoreTexture()
	{
		Driver->getCacheHandler()->getTextureCache().remove(this);

		if (bgfx::isValid(texHandle))
			bgfx::destroy(texHandle);

		if (LockImage)
			LockImage->drop();

		for (u32 i = 0; i < Image.size(); ++i)
			Image[i]->drop();
	}

	virtual void* lock(E_TEXTURE_LOCK_MODE mode = ETLM_READ_WRITE, u32 layer = 0) _IRR_OVERRIDE_
	{
		if (LockImage)
			return LockImage->getData();

		if (IImage::isCompressedFormat(ColorFormat) || IImage::isRenderTargetOnlyFormat(ColorFormat))
			return 0;

		LockReadOnly |= (mode == ETLM_READ_ONLY);
		LockLayer = layer;

		if (KeepImage)
		{
			_IRR_DEBUG_BREAK_IF(LockLayer > Image.size())

			LockImage = Image[LockLayer];
			LockImage->grab();
		}
		else //TODO: LOCKING a texture without a copy in RAM is not working the same way as before -> look at bgfx::readTexture(..) - figure out if there is a way to lock and unlock safely (queue both maybe?)
		{
			LockImage = Driver->createImage(ColorFormat, Size);

			if (LockImage && mode != ETLM_WRITE_ONLY)
			{

				IImage* tmpImage = Driver->createImage(ECF_A8R8G8B8, Size);
				size_t dataBufferSize = layerCount*tmpImage->getImageDataSizeInBytes();
				dataBuffer = static_cast<u32*>(malloc(dataBufferSize));

				lockFrameNumber = bgfx::readTexture(texHandle,dataBuffer); //is not blocking! - ready after 2 frames

				memmove(tmpImage->getData(),dataBuffer+(LockLayer*tmpImage->getImageDataSizeInBytes()),tmpImage->getImageDataSizeInBytes());

				Driver->getCacheHandler()->addLockTextureToQueue(this);

				void* src = tmpImage->getData();
				void* dest = LockImage->getData();

				bool passed = true;

				switch (ColorFormat)
				{
				case ECF_A1R5G5B5:
					CColorConverter::convert_A8R8G8B8toA1B5G5R5(src, tmpImage->getDimension().getArea(), dest);
					break;
				case ECF_R5G6B5:
					CColorConverter::convert_A8R8G8B8toR5G6B5(src, tmpImage->getDimension().getArea(), dest);
					break;
				case ECF_R8G8B8:
					CColorConverter::convert_A8R8G8B8toB8G8R8(src, tmpImage->getDimension().getArea(), dest);
					break;
				case ECF_A8R8G8B8:
					CColorConverter::convert_A8R8G8B8toA8B8G8R8(src, tmpImage->getDimension().getArea(), dest);
					break;
				default:
					passed = false;
					break;
				}

				tmpImage->drop();

				if (!passed)
				{
					LockImage->drop();
					LockImage = 0;
				}
			}
		}

		return (LockImage) ? LockImage->getData() : nullptr;
	}

	virtual void unlock() _IRR_OVERRIDE_
	{
		if (!LockImage)
			return;

		if (!LockReadOnly)
		{
			const CBgfxCoreTexture* prevTexture = Driver->getCacheHandler()->getTextureCache().get(0);
			//Driver->getCacheHandler()->getTextureCache().set(0, this);

			uploadTexture(LockLayer, 0, LockImage->getData(),LockImage->getImageDataSizeInBytes());

			//Driver->getCacheHandler()->getTextureCache().set(0, prevTexture);

			regenerateMipMapLevels(0, LockLayer);
		}

		LockImage->drop();

		LockReadOnly = false;
		LockImage = nullptr;
		LockLayer = 0;
	}

	bool updateLockData(u32 frame )
	{
		if(LockImage && lockFrameNumber >= frame )
		{
			memmove(LockImage->getData(),dataBuffer+(LockLayer*LockImage->getImageDataSizeInBytes()),LockImage->getImageDataSizeInBytes());
			free(dataBuffer);
			dataBuffer = nullptr;
			return true;
		}
		else if(!LockImage)
		{
			return true;
		}
		return false;
	}

	virtual void regenerateMipMapLevels(void* data = 0, u32 layer = 0) _IRR_OVERRIDE_
	{
		if (!HasMipMaps || (!data && !AutoGenerateMipMaps) || (Size.Width <= 1 && Size.Height <= 1))
			return;

		const CBgfxCoreTexture* prevTexture = Driver->getCacheHandler()->getTextureCache().get(0);
		//Driver->getCacheHandler()->getTextureCache().set(0, this);

        u32 width = Size.Width;
		u32 height = Size.Height;
        void* tempData = nullptr;
        u32 dataSize;
			u32 level = 1; // starting with mip level 1 because level 0 was already uploaded (done is constructor itself)

        if (data)
        {
			tempData = data;
        }

        do
        {
            if (width > 1)
                width >>= 1;

            if (height > 1)
                height >>= 1;

            dataSize = IImage::getDataSizeFromFormat(ColorFormat, width, height);

            if (data == nullptr)
            {
                tempData = malloc(dataSize);
                if (LockImage) {
                    LockImage->copyToScaling(tempData, width, height, ColorFormat);
                }
                else if  (Image.size()) {
                    Image[layer]->copyToScaling(tempData, width, height, ColorFormat);
                }
            }

//            std::cout << "layer = " << layer << " level = " << level << " size = " << width << "x" << height << std::endl;
            uploadTexture(layer, level, tempData, dataSize);
            if (data)
            {
                u8* tempPtr = static_cast<u8*>(tempData);
                tempPtr += dataSize;
                tempData = tempPtr;
            }
            else {
                free(tempData);
            }

            ++level;
        }
        while (width > 2 && height > 2);

		//Driver->getCacheHandler()->getTextureCache().set(0, prevTexture);
	}

	u32 getBgfxTextureType() const
	{
		return TextureType;
	}

	bgfx::TextureHandle getTextureHandle() const
	{
		return texHandle;
	}


	SStatesCache& getStatesCache() const
	{
		return StatesCache;
	}

	bgfx::TextureFormat::Enum getBgfxTextureFormat() const
	{
		return BgfxTextureFormat;
	}


protected:
	ECOLOR_FORMAT getBestColorFormat(ECOLOR_FORMAT format)
	{
		ECOLOR_FORMAT destFormat = format;

		switch (format)
		{
		case ECF_A1R5G5B5:
			if (!Driver->getTextureCreationFlag(ETCF_ALWAYS_32_BIT))
				destFormat = ECF_A1R5G5B5;
			break;
		case ECF_R5G6B5:
			if (!Driver->getTextureCreationFlag(ETCF_ALWAYS_32_BIT))
				destFormat = ECF_A1R5G5B5;
			break;
		case ECF_A8R8G8B8:
			if (Driver->getTextureCreationFlag(ETCF_ALWAYS_16_BIT) ||
				Driver->getTextureCreationFlag(ETCF_OPTIMIZED_FOR_SPEED))
				destFormat = ECF_A1R5G5B5;
			break;
		case ECF_R8G8B8:
			if (Driver->getTextureCreationFlag(ETCF_ALWAYS_16_BIT) || Driver->getTextureCreationFlag(ETCF_OPTIMIZED_FOR_SPEED))
				destFormat = ECF_A1R5G5B5;
			break;
		case ECF_A32B32G32R32F:
			destFormat = ECF_A32B32G32R32F;
			break;
		default:
			break;
		}

		if (Driver->getTextureCreationFlag(ETCF_NO_ALPHA_CHANNEL))
		{
			switch (destFormat)
			{
			case ECF_A1R5G5B5:
				destFormat = ECF_R5G6B5;
				break;
			case ECF_A8R8G8B8:
				destFormat = ECF_R8G8B8;
				break;
			default:
				break;
			}
		}

		return destFormat;
	}

	void getImageValues(const IImage* image)
	{
		OriginalColorFormat = image->getColorFormat();
		ColorFormat = getBestColorFormat(OriginalColorFormat);

		//Driver->getColorFormatParameters(ColorFormat, InternalFormat, PixelFormat, PixelType, &Converter);

		if (IImage::isCompressedFormat(image->getColorFormat()))
		{
            //std::cerr << "compressed format !! " << std::endl;
            //std::exit(1);
			KeepImage = false;
			AutoGenerateMipMaps = false;
		}

		OriginalSize = image->getDimension();
		Size = OriginalSize;

		if (Size.Width == 0 || Size.Height == 0)
		{
			os::Printer::log("Invalid size of image for texture.", ELL_ERROR);
			return;
		}

		const f32 ratio = (f32)Size.Width / (f32)Size.Height;

		if ((Size.Width > Driver->getMaxTextureSize().Width) && (ratio >= 1.f))
		{
			Size.Width = Driver->getMaxTextureSize().Width;
			Size.Height = (u32)(Driver->getMaxTextureSize().Width / ratio);
		}
		else if (Size.Height > Driver->getMaxTextureSize().Height)
		{
			Size.Height = Driver->getMaxTextureSize().Height;
			Size.Width = (u32)(Driver->getMaxTextureSize().Height * ratio);
		}

		bool needSquare = (!Driver->queryFeature(EVDF_TEXTURE_NSQUARE) || Type == ETT_CUBEMAP);

		Size = Size.getOptimalSize(!Driver->queryFeature(EVDF_TEXTURE_NPOT), needSquare, true, Driver->getMaxTextureSize().Width);

		Pitch = Size.Width * IImage::getBitsPerPixelFromFormat(ColorFormat) / 8;
	}

	void uploadTexture(u32 layer, u32 level, void* data,u32 dataSize)
	{
		if (!data)
			return;

		u32 width = Size.Width >> level;
		u32 height = Size.Height >> level;


		if (!IImage::isCompressedFormat(ColorFormat))
		{
			CImage* tmpImage = nullptr;
			void* tmpData = malloc(dataSize);
			convertColor(static_cast<u32*>(data),static_cast<u32*>(tmpData),dataSize);
            //delete data;

            //void* tmpData = data;
			/*if (Converter)
			{
				const core::dimension2d<u32> tmpImageSize(width, height);

				tmpImage = new CImage(ColorFormat, tmpImageSize);
				tmpData = tmpImage->getData();

				Converter(data, tmpImageSize.getArea(), tmpData);
			}*/
			if(!tmpData)
				return;
			switch (TextureType)
			{
			case ETT_2D:
			case ETT_ARRAY:
				bgfx::updateTexture2D(texHandle,layer,level,0,0,width,height,bgfx::copy(tmpData,dataSize));
				break;
			case ETT_CUBEMAP:
				_IRR_DEBUG_BREAK_IF(layer > 5)
				bgfx::updateTextureCube(texHandle,0,layer,level,0,0,width,height,bgfx::copy(tmpData,dataSize));
				break;
			default:
				break;
			}

			free(tmpData);
		}
		else
		{
			u32 compDataSize = IImage::getDataSizeFromFormat(ColorFormat, width, height);
			switch (TextureType)
			{
			case ETT_2D:
			case ETT_ARRAY:
				bgfx::updateTexture2D(texHandle,layer,level,0,0,width,height,bgfx::copy(data,compDataSize));
			break;
			case ETT_CUBEMAP:
				_IRR_DEBUG_BREAK_IF(layer > 5)
				bgfx::updateTextureCube(texHandle,0,layer,level,0,0,width,height,bgfx::copy(data,compDataSize));
				break;
			default:
				break;
			}
		}
	}
	//TODO: support all kinds of formats
	inline void convertColor(void* src, void* dest, u32 datasize)
	{
        //std::cerr << this->getName().getPath().c_str() << ": input = " << static_cast<uint32_t>(ColorFormat) << std::endl;
        switch(ColorFormat) {
            case ECF_A8R8G8B8:
		{	u32* tmpdest = static_cast<u32*>(dest);
			u32* tmpsrc = static_cast<u32*>(src);
			for(u32 i = 0; i < datasize/4; ++i)
			{
				*tmpdest = getAbgrFromArgb32(*tmpsrc);
				++tmpdest;
				++tmpsrc;
			}

        }
        break;
    case ECF_A1R5G5B5:
		{
			u16* tmpdest = static_cast<u16*>(dest);
			u16* tmpsrc = static_cast<u16*>(src);

			for(u32 i = 0; i < datasize/2; ++i)
			{
				*tmpdest = getAbgrFromArgb16(*tmpsrc);
				++tmpdest;
				++tmpsrc;
			}

		}
        break;
    default:
		u8* tmpdest = static_cast<u8*>(dest);
		u8* tmpsrc = static_cast<u8*>(src);

		for(u32 i = 0; i < datasize; ++i)
		{
			*tmpdest = *tmpsrc;
			++tmpdest;
			++tmpsrc;
		}
        //os::Printer::log("unknown color format", irr::core::stringc(static_cast<uint32_t>(ColorFormat)));
        }
	}

	inline u32 getAbgrFromArgb32(u32 argb)
	{
		return
			// Source is in format: 0xAARRGGBB
				((argb & 0xFF000000) >> 0) |  //AA______
				((argb & 0x000000FF) << 16) | //__BB____
				((argb & 0x0000FF00) <<  0) | //____GG__
				((argb & 0x00FF0000) >> 16);   //______RR
			// Return value is in format:  0xAABBGGRR
	}

	inline u16 getAbgrFromArgb16(u16 argb)
		{
			return
				// Source is in format: A1R5G5B5
					((argb & 1<<15) >> 0) |  //A1______
					((argb & 31 ) << 10 ) |  //__B5____
					((argb & 31<<5) <<  0) | //____G5__
					((argb & 31<<10) >> 10); //______R5
				// Return value is in format:  A1B5G5R5
		}

	TBgfxDriver* Driver;

	u32 TextureType;
	bgfx::TextureFormat::Enum BgfxTextureFormat;
	int InternalFormat;
	u32 PixelFormat;
	u32 PixelType;

	void (*Converter)(const void*, s32, void*);

	bool LockReadOnly;
	IImage* LockImage;
	u32 LockLayer;

	bool KeepImage;
	core::array<IImage*> Image;
	u32 layerCount;

	bool AutoGenerateMipMaps;

	mutable SStatesCache StatesCache;

	//BGFX
	bgfx::TextureHandle texHandle;

	bgfx::TextureHandle clearTexHandle;
	u32* dataBuffer=nullptr;
	u32 lockFrameNumber = 0;


};

}
}


#endif
