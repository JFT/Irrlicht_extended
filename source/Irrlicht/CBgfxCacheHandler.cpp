// Copyright (C) 2016 Julius Tilly
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in Irrlicht.h

#include "CBgfxCacheHandler.h"

#ifdef _IRR_COMPILE_WITH_BGFX_

#include "CBgfxDriver.h"

namespace irr
{
namespace video
{

/* CBgfxCacheHandler */

CBgfxCacheHandler::CBgfxCacheHandler(CBgfxDriver* driver) :
	CBgfxCoreCacheHandler<CBgfxDriver, CBgfxTexture>(driver), AlphaMode(false), AlphaRef(0.f), AlphaTest(false),
	ClientStateVertex(false), ClientStateNormal(false), ClientStateColor(false), ClientStateTexCoord0(false)
{

}

CBgfxCacheHandler::~CBgfxCacheHandler()
{
}

void CBgfxCacheHandler::setAlphaFunc(bool mode, f32 ref)
{
	if (AlphaMode != mode || AlphaRef != ref)
	{
		AlphaMode = mode;
		AlphaRef = ref;
	}
}

void CBgfxCacheHandler::setAlphaTest(bool enable)
{
	AlphaTest = enable;
}


//void CBgfxCacheHandler::setMatrixMode(GLenum mode)
//{
//	if (MatrixMode != mode)
//	{
//		glMatrixMode(mode);
//		MatrixMode = mode;
//	}
//}

//void CBgfxCacheHandler::setClientActiveTexture(GLenum texture)
//{
//	if (ClientActiveTexture != texture)
//	{
//		Driver->irrGlClientActiveTexture(texture);
//		ClientActiveTexture = texture;
//	}
//}

uint64_t CBgfxCacheHandler::getBgfxStateFlag(u32 viewIndx) const
{
	if(FrameBufferCount <= viewIndx)
		return 0;
	///NOTE: IF YOU CHANGE FLAG HERE - ALSO CHECK IF YOU NEED TO CHANGE THE FLAG IN CBgfxCoreCacheHandler.h!!
	return 	UINT64_C(0) | (DepthMask ? BGFX_STATE_WRITE_Z : UINT64_C(0)) | (ColorMask[viewIndx][3] ? BGFX_STATE_WRITE_A : UINT64_C(0))
		| (ColorMask[viewIndx][0] ? BGFX_STATE_WRITE_RGB : UINT64_C(0)) | (Blend[viewIndx] ? (UINT64_C(0) | BlendEquation[viewIndx] | BlendFunc[viewIndx]) : UINT64_C(0)) 
		| (CullFace[viewIndx] ? CullFaceMode[viewIndx] : UINT64_C(0)) | (DepthTest ? DepthFunc : UINT64_C(0)) | (AlphaMode ? BGFX_STATE_ALPHA_REF(AlphaRef) : UINT64_C(0)) | extraFlag;
}

void CBgfxCacheHandler::setActiveView(u8 view)
{
	//std::cerr << "CBgfxCacheHandler::setActiveView: active view(" << static_cast<int>(activeView) << ")->" << static_cast<int>(view) << ", prev(" << static_cast<int>(prevView) << ")" << std::endl;
	if(view != activeView)
	{
		prevView = activeView;
		activeView = view;
	}
}

void CBgfxCacheHandler::setActiveViewToPrevious()
{
	//std::cerr << "CBgfxCacheHandler::setActiveViewToPrevious: active view(" << static_cast<int>(activeView) << ") -> prev(" << static_cast<int>(prevView) << ")" << std::endl;
	activeView = prevView;
}

void CBgfxCacheHandler::updateQueuedObjects(u32 frame)
{
	if(queuedLockUpdates.empty())
	{
		return;
	}

	for(auto it = queuedLockUpdates.begin(); it != queuedLockUpdates.end(); ++it)
	{
		if((*it)->updateLockData(frame))
		{
			it = queuedLockUpdates.erase(it);
		}
	}

}

void CBgfxCacheHandler::addLockTextureToQueue(CBgfxCoreTexture<CBgfxDriver>* texture)
{
	if(texture)
		queuedLockUpdates.push_back(texture);
}

} // end namespace
} // end namespace

#endif //#ifdef _IRR_COMPILE_WITH_BGFX_
