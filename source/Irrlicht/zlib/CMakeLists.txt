cmake_minimum_required(VERSION 3.7.2)

set(libzlib_src
    adler32.c 
    compress.c 
    crc32.c 
    deflate.c 
    inffast.c 
    inflate.c 
    inftrees.c 
    trees.c 
    uncompr.c 
    zutil.c)

add_library(libzlib ${libzlib_src})