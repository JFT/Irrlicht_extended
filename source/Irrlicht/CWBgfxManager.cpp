// Copyright (C) 2013 Julius Tilly
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in Irrlicht.h

#include "CWBgfxManager.h"

#ifdef _IRR_COMPILE_WITH_WBGFX_MANAGER_


#include "os.h"

#include <bgfx/bgfx.h>


namespace irr
{
namespace video
{

CWBgfxManager::CWBgfxManager()
	: PrimaryContext(SExposedVideoData(0)), PixelFormat(0)
{
	#ifdef _DEBUG
	setDebugName("CWBgfxManager");
	#endif
}

CWBgfxManager::~CWBgfxManager()
{
}

bool CWBgfxManager::initialize(const SIrrlichtCreationParameters& params, const SExposedVideoData& videodata)
{
	// store params, videoData is set later as it would be overwritten else
	Params=params;

	// Create a window to test antialiasing support
	const fschar_t* ClassName = __TEXT("CWBgfxManager");
	HINSTANCE lhInstance = GetModuleHandle(0);

	// Register Class
	WNDCLASSEX wcex;
	wcex.cbSize        = sizeof(WNDCLASSEX);
	wcex.style         = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc   = (WNDPROC)DefWindowProc;
	wcex.cbClsExtra    = 0;
	wcex.cbWndExtra    = 0;
	wcex.hInstance     = lhInstance;
	wcex.hIcon         = NULL;
	wcex.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName  = 0;
	wcex.lpszClassName = ClassName;
	wcex.hIconSm       = 0;
	wcex.hIcon         = 0;
	RegisterClassEx(&wcex);

	RECT clientSize;
	clientSize.top = 0;
	clientSize.left = 0;
	clientSize.right = Params.WindowSize.Width;
	clientSize.bottom = Params.WindowSize.Height;

	DWORD style = WS_POPUP;
	if (!Params.Fullscreen)
		style = WS_SYSMENU | WS_BORDER | WS_CAPTION | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;

	AdjustWindowRect(&clientSize, style, FALSE);

	const s32 realWidth = clientSize.right - clientSize.left;
	const s32 realHeight = clientSize.bottom - clientSize.top;

	const s32 windowLeft = (GetSystemMetrics(SM_CXSCREEN) - realWidth) / 2;
	const s32 windowTop = (GetSystemMetrics(SM_CYSCREEN) - realHeight) / 2;

	HWND temporary_wnd=CreateWindow(ClassName, __TEXT(""), style, windowLeft,
			windowTop, realWidth, realHeight, NULL, NULL, lhInstance, NULL);

	if (!temporary_wnd)
	{
		os::Printer::log("Cannot create a temporary window.", ELL_ERROR);
		UnregisterClass(ClassName, lhInstance);
		return false;
	}

	CurrentContext.Bgfx.HDc = NULL;
	CurrentContext.Bgfx.HRc = NULL;
	CurrentContext.Bgfx.HWnd = temporary_wnd;

	if (!activateContext(CurrentContext))
	{
		os::Printer::log("Cannot activate a temporary Bgfx rendering context.", ELL_ERROR);
		DestroyWindow(temporary_wnd);
		UnregisterClass(ClassName, lhInstance);
		return false;
	}

	Params.AntiAlias=0;

	// this only terminates the temporary HRc
	destroyContext();
	destroySurface();
	terminate();
	DestroyWindow(temporary_wnd);
	UnregisterClass(ClassName, lhInstance);

	// now get new window
	CurrentContext.Bgfx.HWnd=videodata.Bgfx.HWnd;
	// get hdc
//	if (!(CurrentContext.Bgfx.HDc=GetDC((HWND)videodata.Bgfx.HWnd)))
//	{
//		os::Printer::log("Cannot create a Bgfx device context.", ELL_ERROR);
//		return false;
//	}
	if (!PrimaryContext.Bgfx.HWnd)
	{
		PrimaryContext.Bgfx.HWnd=CurrentContext.Bgfx.HWnd;
		//PrimaryContext.Bgfx.HDc=CurrentContext.Bgfx.HDc;
	}

	return true;
}

void CWBgfxManager::terminate()
{
	if (CurrentContext.Bgfx.HDc)
		ReleaseDC((HWND)CurrentContext.Bgfx.HWnd, (HDC)CurrentContext.Bgfx.HDc);
	if (PrimaryContext.Bgfx.HDc && PrimaryContext.Bgfx.HDc == CurrentContext.Bgfx.HDc)
		memset(&PrimaryContext, 0, sizeof(PrimaryContext));
	memset(&CurrentContext, 0, sizeof(CurrentContext));
}

bool CWBgfxManager::generateSurface()
{
//	HDC HDc = (HDC)CurrentContext.Bgfx.HDc;
//	// search for pixel format the simple way
//	if (PixelFormat==0 || (!SetPixelFormat(HDc, PixelFormat, &pfd)))
//	{
//		for (u32 i=0; i<5; ++i)
//		{
//			if (i == 1)
//			{
//				if (Params.Stencilbuffer)
//				{
//					os::Printer::log("Cannot create a GL device with stencil buffer, disabling stencil shadows.", ELL_WARNING);
//					Params.Stencilbuffer = false;
//					pfd.cStencilBits = 0;
//				}
//				else
//					continue;
//			}
//			else
//			if (i == 2)
//			{
//				pfd.cDepthBits = 24;
//			}
//			if (i == 3)
//			{
//				if (Params.Bits!=16)
//					pfd.cDepthBits = 16;
//				else
//					continue;
//			}
//			else
//			if (i == 4)
//			{
//				os::Printer::log("Cannot create a GL device context", "No suitable format.", ELL_ERROR);
//				return false;
//			}
//
//			// choose pixelformat
//			PixelFormat = ChoosePixelFormat(HDc, &pfd);
//			if (PixelFormat)
//				break;
//		}
//
//		// set pixel format
//		if (!SetPixelFormat(HDc, PixelFormat, &pfd))
//		{
//			os::Printer::log("Cannot set the pixel format.", ELL_ERROR);
//			return false;
//		}
//	}
//
//	if (pfd.cAlphaBits != 0)
//	{
//		if (pfd.cRedBits == 8)
//			ColorFormat = ECF_A8R8G8B8;
//		else
//			ColorFormat = ECF_A1R5G5B5;
//	}
//	else
//	{
//		if (pfd.cRedBits == 8)
//			ColorFormat = ECF_R8G8B8;
//		else
//			ColorFormat = ECF_R5G6B5;
//	}
//	os::Printer::log("Pixel Format", core::stringc(PixelFormat).c_str(), ELL_DEBUG);
	return true;
}

void CWBgfxManager::destroySurface()
{
}

bool CWBgfxManager::generateContext()
{
	bgfxPlatformData.nwh = CurrentContext.Bgfx.HWnd;
	bgfxPlatformData.context = NULL;
	bgfxPlatformData.ndt = NULL;
	bgfxPlatformData.backBuffer = NULL;
	bgfxPlatformData.backBufferDS = NULL;

    bgfx::setPlatformData(this->bgfxPlatformData);
	return true;
}

const SExposedVideoData& CWBgfxManager::getContext() const
{
	return CurrentContext;
}

bool CWBgfxManager::activateContext(const SExposedVideoData& videoData)
{
	return true;
}

void CWBgfxManager::destroyContext()
{

}

bool CWBgfxManager::swapBuffers()
{
	return true;//SwapBuffers((HDC)CurrentContext.Bgfx.HDc) == TRUE;
}

}
}
#endif // _IRR_COMPILE_WITH_WBGFX_MANAGER_
