// Copyright (C) 2016 Julius Tilly
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_BGFXCORE_RENDER_TARGET_H_INCLUDED__
#define __C_BGFXCORE_RENDER_TARGET_H_INCLUDED__

#include "IrrCompileConfig.h"


#include "IRenderTarget.h"

namespace irr
{
namespace video
{

template <class TBgfxDriver, class TBgfxTexture>
class CBgfxCoreRenderTarget : public IRenderTarget
{
public:
	CBgfxCoreRenderTarget(TBgfxDriver* driver) : AssignedDepth(false), AssignedStencil(false), RequestTextureUpdate(false), RequestDepthStencilUpdate(false),
		BufferID(bgfx::kInvalidHandle), ColorAttachment(0), MultipleRenderTarget(0), FrameBufferHandle(BGFX_INVALID_HANDLE), Driver(driver)
	{
#ifdef _DEBUG
		setDebugName("CBgfxCoreRenderTarget");
#endif

		DriverType = Driver->getDriverType();

		Size = Driver->getScreenSize();

		ColorAttachment = Driver->getFeature().ColorAttachment;
		MultipleRenderTarget = Driver->getFeature().MultipleRenderTarget;

		AssignedTexture.set_used(static_cast<u32>(ColorAttachment));

		for (u32 i = 0; i < AssignedTexture.size(); ++i)
			AssignedTexture[i].idx = bgfx::kInvalidHandle;
	}

	virtual ~CBgfxCoreRenderTarget()
	{
		if (ColorAttachment > 0 && BufferID != bgfx::kInvalidHandle)
			bgfx::destroy(FrameBufferHandle);

		for (u32 i = 0; i < Texture.size(); ++i)
		{
			if (Texture[i])
				Texture[i]->drop();
		}

		if (DepthStencil)
			DepthStencil->drop();
	}

	virtual void setTexture(const core::array<ITexture*>& texture, ITexture* depthStencil) _IRR_OVERRIDE_
	{
		bool textureUpdate = (Texture != texture) ? true : false;
		bool depthStencilUpdate = (DepthStencil != depthStencil) ? true : false;

        /*std::cerr << "CBgfxCoreRenderTarget.h::setTexture with textureUpdate = " << textureUpdate << " depthUpdate = " << depthStencilUpdate << std::endl;
        if (texture.size() > 0) {
            std::cerr << "texture[0] = " << static_cast<void*>(texture[0]) << std::endl;
        }
        if (Texture.size() > 0) {
            std::cerr << "Texture[0] = " << static_cast<void*>(Texture[0]) << std::endl;
        }*/

		if (textureUpdate || depthStencilUpdate)
		{
			// Set color attachments.

			if (textureUpdate)
			{
				for (u32 i = 0; i < Texture.size(); ++i)
				{
					if (Texture[i])
						Texture[i]->drop();
				}

				if (texture.size() > static_cast<u32>(ColorAttachment))
				{
					core::stringc message = "This GPU supports up to ";
					message += static_cast<u32>(ColorAttachment);
					message += " textures per render target.";

					os::Printer::log(message.c_str(), ELL_WARNING);
				}

				Texture.set_used(core::min_(texture.size(), static_cast<u32>(ColorAttachment)));

				for (u32 i = 0; i < Texture.size(); ++i)
				{
					TBgfxTexture* currentTexture = (texture[i] && texture[i]->getDriverType() == DriverType) ? static_cast<TBgfxTexture*>(texture[i]) : nullptr;

					u32 textureID = bgfx::kInvalidHandle;

					if (currentTexture)
					{
						if (currentTexture->getType() == ETT_2D)
							{
								textureID = currentTexture->getTextureHandle().idx;
							}
						else
							os::Printer::log("This driver doesn't support render to cubemaps.", ELL_WARNING);
					}

					if (textureID != bgfx::kInvalidHandle)
					{
						Texture[i] = texture[i];
						Texture[i]->grab();
					}
					else
					{
						Texture[i] = nullptr;
					}
				}

				RequestTextureUpdate = true;
			}

			// Set depth and stencil attachments.

			if (depthStencilUpdate)
			{
				TBgfxTexture* currentTexture = (depthStencil && depthStencil->getDriverType() == DriverType) ? static_cast<TBgfxTexture*>(depthStencil) : nullptr;

				u32 textureID = 0;

				if (currentTexture)
				{
					if (currentTexture->getType() == ETT_2D)
						textureID = currentTexture->getTextureHandle().idx;
					else
						os::Printer::log("This driver doesn't support render to cubemaps.", ELL_WARNING);
				}

				const ECOLOR_FORMAT textureFormat = (textureID != 0) ? depthStencil->getColorFormat() : ECF_UNKNOWN;

				if (IImage::isDepthFormat(textureFormat))
				{
					DepthStencil = depthStencil;
					DepthStencil->grab();
				}
				else
				{
					if (DepthStencil)
						DepthStencil->drop();

					DepthStencil = 0;
				}

				RequestDepthStencilUpdate = true;
			}

			// Set size required for a viewport.

			ITexture* firstTexture = getTexture();

			if (firstTexture)
				Size = firstTexture->getSize();
			else
			{
				if (DepthStencil)
					Size = DepthStencil->getSize();
				else
					Size = Driver->getScreenSize();
			}
		}
		update();
	}


	void update()
	{
		if (RequestTextureUpdate || RequestDepthStencilUpdate)
		{
			// Set color attachments.

			if (RequestTextureUpdate)
			{
				// Set new color textures.

				core::array<bgfx::TextureHandle> tempTexArray;

				const u32 textureSize = core::min_(Texture.size(), AssignedTexture.size());

				for (u32 i = 0; i < textureSize; ++i)
				{
					u32 textureID = (Texture[i]) ? static_cast<TBgfxTexture*>(Texture[i])->getTextureHandle().idx : bgfx::kInvalidHandle;

					if (textureID != bgfx::kInvalidHandle)
					{
						AssignedTexture[i] = static_cast<TBgfxTexture*>(Texture[i])->getTextureHandle();
						tempTexArray.push_back(AssignedTexture[i]);
					}
					else if (bgfx::isValid(AssignedTexture[i]))
					{
						AssignedTexture[i].idx = bgfx::kInvalidHandle;

						os::Printer::log("Error: Could not set render target.", ELL_ERROR);
					}
				}

				// Reset other render target channels.

				for (u32 i = textureSize; i < AssignedTexture.size(); ++i)
				{
					if (bgfx::isValid(AssignedTexture[i]))
					{
						AssignedTexture[i].idx = bgfx::kInvalidHandle;
					}
				}

				if(bgfx::isValid(FrameBufferHandle))
				{
                    //std::cerr << "CBgfxCoreRenderTarget::update() destroying fb " << static_cast<int>(FrameBufferHandle.idx) << std::endl;
					bgfx::destroy(FrameBufferHandle);
					FrameBufferHandle.idx = bgfx::kInvalidHandle;
				}
				if(AssignedDepth || RequestDepthStencilUpdate)
				{
					tempTexArray.push_back(static_cast<TBgfxTexture*>(DepthStencil)->getTextureHandle());
					RequestDepthStencilUpdate = false;
					AssignedDepth = true;
				}

				FrameBufferHandle = bgfx::createFrameBuffer(tempTexArray.size(),tempTexArray.pointer(),false);
                //std::cerr << "CBgfxCoreRenderTarget::update() created new fb with handle " << static_cast<int>(FrameBufferHandle.idx) << std::endl;

				RequestTextureUpdate = false;
			}

			// Set depth and stencil attachments.

			if (RequestDepthStencilUpdate) // only true if now color texture update was done
			{
				const ECOLOR_FORMAT textureFormat = (DepthStencil) ? DepthStencil->getColorFormat() : ECF_UNKNOWN;

				if (IImage::isDepthFormat(textureFormat))
				{

					core::array<bgfx::TextureHandle> tempTexArray;

					const u32 textureSize = core::min_(Texture.size(), AssignedTexture.size());

					for(u32 i = 0; i < textureSize; ++i)
					{
						if(bgfx::isValid(AssignedTexture[i]))
						{
							tempTexArray.push_back(AssignedTexture[i]);
						}
					}

					if(bgfx::isValid(FrameBufferHandle))
					{
						bgfx::destroy(FrameBufferHandle);
						FrameBufferHandle.idx = bgfx::kInvalidHandle;
					}

					tempTexArray.push_back(static_cast<TBgfxTexture*>(DepthStencil)->getTextureHandle());

					FrameBufferHandle = bgfx::createFrameBuffer(tempTexArray.size(),tempTexArray.pointer(),false);
					AssignedDepth = true;
				}
				else
				{
					AssignedDepth = false;
					AssignedStencil = false;
				}

				RequestDepthStencilUpdate = false;
			}
		} //if (RequestTextureUpdate || RequestDepthStencilUpdate)

        // Configure drawing operation. (the binding view->frameBuffer always has to be set because the view the framebuffer uses can change

	}

	void setFrameBufferToView(const bgfx::ViewId viewId)
	{
		if (ColorAttachment > 0 && bgfx::isValid(FrameBufferHandle))
        {
			if(viewId < bgfx::getCaps()->limits.maxViews)
				bgfx::setViewFrameBuffer(viewId,FrameBufferHandle);
			else
			{
				os::Printer::log("Error: Rendertarget can not be set to invalid viewId ", ELL_ERROR);
			}
		}
	}

	u32 getBufferID() const
	{
		return FrameBufferHandle.idx;
	}

	const core::dimension2d<u32>& getSize() const
	{
		return Size;
	}

	ITexture* getTexture() const
	{
		for (u32 i = 0; i < Texture.size(); ++i)
		{
			if (Texture[i])
				return Texture[i];
		}

		return 0;
	}

protected:

	core::array<bgfx::TextureHandle> AssignedTexture;
	bool AssignedDepth;
	bool AssignedStencil;

	bool RequestTextureUpdate;
	bool RequestDepthStencilUpdate;

	u32 BufferID;

	core::dimension2d<u32> Size;

	u32 ColorAttachment;
	u32 MultipleRenderTarget;

	bgfx::FrameBufferHandle FrameBufferHandle;

	TBgfxDriver* Driver;
};

}
}

#endif
