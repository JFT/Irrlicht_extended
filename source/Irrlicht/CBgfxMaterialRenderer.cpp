// Copyright (C) 2016 Julius Tilly, Hannes Franke
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h
#include "CBgfxMaterialRenderer.h"

#ifdef _IRR_COMPILE_WITH_BGFX_
#include <bgfx/bgfx.h>
#include "bgfx_shader/bgfx_shader.h"

#include <iostream> //TODO: remove. only used for debug output

namespace irr
{
namespace video
{



CBgfxMaterialRenderer_BASE::CBgfxMaterialRenderer_BASE(video::CBgfxDriver* d) : Driver(d), CompiledShader(BGFX_INVALID_HANDLE) {};

bool CBgfxMaterialRenderer_BASE::compileShaders(const uint8_t* vertexShaderCode, const uint32_t vsSize, const uint8_t* fragmentShaderCode, const uint32_t fsSize) {
    if (vertexShaderCode == nullptr || vsSize == 0) {
        return false;
    }

    bgfx::ShaderHandle vertexShader = BGFX_INVALID_HANDLE;
    bgfx::ShaderHandle fragmentShader = BGFX_INVALID_HANDLE;

    // add shader code to bgfx memory
    const bgfx::Memory* vertexShaderMemory  = bgfx::copy(vertexShaderCode, vsSize);

    vertexShader = bgfx::createShader(vertexShaderMemory);
    if (!bgfx::isValid(vertexShader)) {
        return false;
    }

    if (fragmentShaderCode != nullptr && fsSize > 0) {
        const bgfx::Memory* fragmentShaderMemory = bgfx::copy(fragmentShaderCode, fsSize);
        fragmentShader = bgfx::createShader(fragmentShaderMemory);
        if (!bgfx::isValid(fragmentShader)) {
            return false;
        }
    }

    // add command to compile the shaders to bgfx command buffer
    this->CompiledShader = bgfx::createProgram(vertexShader, fragmentShader);

    // bgfx docs state it is safe to destroy shader handles once they are compiled into a program handle
    bgfx::destroy(vertexShader);
    if (bgfx::isValid(fragmentShader)) {
        bgfx::destroy(fragmentShader);
    }
    if (!bgfx::isValid(this->CompiledShader)) {
        return false;
    }
    return true;
}

CBgfxMaterialRenderer_SOLID::CBgfxMaterialRenderer_SOLID(video::CBgfxDriver* d) : CBgfxMaterialRenderer_BASE(d)
{
    switch (d->getDriverType()) {
        case EDT_BGFX_OPENGL:
            if(!this->compileShaders(standard_vs_glsl_bgfx, sizeof(standard_vs_glsl_bgfx), standard_fs_glsl_bgfx, sizeof(standard_fs_glsl_bgfx)))
			{
				std::cerr<<"Failed to compile shader!"<<std::endl;
				std::exit(22);
			}

            break;
        case EDT_BGFX_D3D9:
			if (!this->compileShaders(standard_vs_hlsl_DX9_bgfx, sizeof(standard_vs_hlsl_DX9_bgfx), standard_fs_hlsl_DX9_bgfx, sizeof(standard_fs_hlsl_DX9_bgfx)))
			{
				std::cerr << "Failed to compile shader!" << std::endl;
				std::exit(22);
			}

			break;
        case EDT_BGFX_D3D11:
        	if(!this->compileShaders(standard_vs_hlsl_DX11_bgfx, sizeof(standard_vs_hlsl_DX11_bgfx), standard_fs_hlsl_DX11_bgfx, sizeof(standard_fs_hlsl_DX11_bgfx)))
			{
				std::cerr<<"Failed to compile shader!"<<std::endl;
				std::exit(22);
			}

        	break;
        default:
            std::cerr << "not implemented: case for bgfx renderer type" << static_cast<int>(d->getDriverType()) << std::endl; //TODO: add proper error handling/printnig
            std::exit(2);
    }
}

CBgfxMaterialRenderer_BASIC::CBgfxMaterialRenderer_BASIC(video::CBgfxDriver* d) : CBgfxMaterialRenderer_BASE(d)
{
	switch (d->getDriverType()) {
	        case EDT_BGFX_OPENGL:
	            if(!this->compileShaders(basic_vs_glsl_bgfx, sizeof(basic_vs_glsl_bgfx), basic_fs_glsl_bgfx, sizeof(basic_fs_glsl_bgfx)))
				{
					std::cerr<<"Failed to compile shader!"<<std::endl;
					std::exit(22);
				}

	            break;
	        case EDT_BGFX_D3D9:
				if (!this->compileShaders(basic_vs_hlsl_DX9_bgfx, sizeof(basic_vs_hlsl_DX9_bgfx), basic_fs_hlsl_DX9_bgfx, sizeof(basic_fs_hlsl_DX9_bgfx)))
				{
					std::cerr << "Failed to compile shader!" << std::endl;
					std::exit(22);
				}
				break;
	        case EDT_BGFX_D3D11:
				if(!this->compileShaders(basic_vs_hlsl_DX11_bgfx, sizeof(basic_vs_hlsl_DX11_bgfx), basic_fs_hlsl_DX11_bgfx, sizeof(basic_fs_hlsl_DX11_bgfx)))
				{
					std::cerr<<"Failed to compile shader!"<<std::endl;
					std::exit(22);
				}
				break;
	        default:
	            std::cerr << "not implemented: case for bgfx renderer type" << static_cast<int>(d->getDriverType()) << std::endl; //TODO: add proper error handling/printnig
	            std::exit(2);
	    }
}

CBgfxMaterialRenderer_ONETEXTURE_BLEND::CBgfxMaterialRenderer_ONETEXTURE_BLEND(video::CBgfxDriver* d) : CBgfxMaterialRenderer_BASE(d)
{
        // there is no float type because internally glsl and hlsl pack floats in vec4 - so pack everything you have in vec4s
		uniformModulate = bgfx::createUniform("modulate",bgfx::UniformType::Vec4);

		switch (d->getDriverType())
		{
		        case EDT_BGFX_OPENGL:
		            if(!this->compileShaders(onetextureblend_vs_glsl_bgfx, sizeof(onetextureblend_vs_glsl_bgfx), onetextureblend_fs_glsl_bgfx, sizeof(onetextureblend_fs_glsl_bgfx)))
					{
						std::cerr<<"Failed to compile shader!"<<std::endl;
						std::exit(22);
					}

		            break;

		        case EDT_BGFX_D3D9:
					if (!this->compileShaders(onetextureblend_vs_hlsl_DX9_bgfx, sizeof(onetextureblend_vs_hlsl_DX9_bgfx), onetextureblend_fs_hlsl_DX9_bgfx, sizeof(onetextureblend_fs_hlsl_DX9_bgfx)))
					{
						std::cerr << "Failed to compile shader!" << std::endl;
						std::exit(22);
					}
					break;
		        case EDT_BGFX_D3D11:
					if(!this->compileShaders(onetextureblend_vs_hlsl_DX11_bgfx, sizeof(onetextureblend_vs_hlsl_DX11_bgfx), onetextureblend_fs_hlsl_DX11_bgfx, sizeof(onetextureblend_fs_hlsl_DX11_bgfx)))
					{
						std::cerr<<"Failed to compile shader!"<<std::endl;
						std::exit(22);
					}
					break;

		        default:
		            std::cerr << "not implemented: case for bgfx renderer type" << static_cast<int>(d->getDriverType()) << std::endl; //TODO: add proper error handling/printnig
		            std::exit(2);
		}
}

CBgfxMaterialRenderer_TRANSPARENT_ADD_COLOR::CBgfxMaterialRenderer_TRANSPARENT_ADD_COLOR(video::CBgfxDriver* d) : CBgfxMaterialRenderer_BASE(d)
{

	switch (d->getDriverType())
	{
		case EDT_BGFX_OPENGL:
			if (!this->compileShaders(transparentAddColor_vs_glsl_bgfx, sizeof(transparentAddColor_vs_glsl_bgfx), transparentAddColor_fs_glsl_bgfx, sizeof(transparentAddColor_fs_glsl_bgfx)))
			{
				std::cerr << "Failed to compile shader!" << std::endl;
				std::exit(22);
			}

			break;

		case EDT_BGFX_D3D9:
			if (!this->compileShaders(transparentAddColor_vs_hlsl_DX9_bgfx, sizeof(transparentAddColor_vs_hlsl_DX9_bgfx), transparentAddColor_fs_hlsl_DX9_bgfx, sizeof(transparentAddColor_fs_hlsl_DX9_bgfx)))
			{
				std::cerr << "Failed to compile shader!" << std::endl;
				std::exit(22);
			}
			break;
		case EDT_BGFX_D3D11:
			if (!this->compileShaders(transparentAddColor_vs_hlsl_DX11_bgfx, sizeof(transparentAddColor_vs_hlsl_DX11_bgfx), transparentAddColor_fs_hlsl_DX11_bgfx, sizeof(transparentAddColor_fs_hlsl_DX11_bgfx)))
			{
				std::cerr << "Failed to compile shader!" << std::endl;
				std::exit(22);
			}
			break;

		default:
			std::cerr << "not implemented: case for bgfx renderer type" << static_cast<int>(d->getDriverType()) << std::endl; //TODO: add proper error handling/printnig
			std::exit(2);
	}

}

}//video
}//irr

#endif //_IRR_COMPILE_WITH_BGFX_
