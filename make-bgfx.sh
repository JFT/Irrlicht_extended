#!/bin/bash

# exit with error if any command fails
set -e

OS="$(uname -m)"
target=""
if [ "$OS" = "x86_64" ]; then
	target="64"
else
	target="32"
fi

PARALLEL_JOBS=`nproc 2>/dev/null`
if [ "x$?" != "x0" ]; then
    echo "Couldn't determine # of available processors using 'nproc' -> using default (1)"
    PARALLEL_JOBS=1
fi
[ -z "$PARALLEL_JOBS" ] && PARALLEL_JOBS=1
#pwd
chmod +x bx/tools/bin/linux/genie
set -x
cd bgfx
# calling genie ourselves, because we don't need tools or examples
make clean # in case of new files need to be generated
make linux-release64 -j "$PARALLEL_JOBS" || exit 2
make linux-debug64 -j "$PARALLEL_JOBS" || exit 2

cd ..
mkdir -p lib/Linux
cp bgfx/.build/linux"$target"_gcc/bin/* lib/Linux/

