

if(UNIX)
    set(BGFX_LIB_DIR "${CMAKE_SOURCE_DIR}/lib/Linux")
endif()
if(WIN32)
    if(MSVC)
        set(BGFX_LIB_DIR "lib/Win32-visualstudio")
    else()
        set(BGFX_LIB_DIR "lib/Win32-gcc")
    endif()
endif()

message(STATUS "Looking for Bgfx header and library in ${CMAKE_SOURCE_DIR}/bgfx/...")

set(BGFX_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/bgfx/include")
set(BX_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/bx/include")
set(BIMG_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/bimg/include")

find_library(BGFX_LIB_DEBUG
             NAMES bgfxDebug bgfx_d
             HINTS ${BGFX_LIB_DIR})

find_library(BGFX_LIB_RELEASE
             NAMES bgfxRelease
             HINTS ${BGFX_LIB_DIR})

find_library(BX_LIB_DEBUG
             NAMES bxDebug bx_d
             HINTS ${BGFX_LIB_DIR}
             )

find_library(BX_LIB_RELEASE
            NAMES bxRelease
            HINTS  ${BGFX_LIB_DIR}
            )

find_library(BIMG_LIB_DEBUG
            NAMES bimgDebug bimg_d
            HINTS  ${BGFX_LIB_DIR}
            )


find_library(BIMG_LIB_RELEASE
            NAMES bimgRelease
            HINTS  ${BGFX_LIB_DIR}
            )

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(bgfx DEFAULT_MSG BGFX_LIB_DEBUG BGFX_LIB_RELEASE BX_LIB_DEBUG BX_LIB_RELEASE BIMG_LIB_DEBUG BIMG_LIB_RELEASE)

if (bgfx_FOUND)
    set(BGFX_LIBRARIES ${BGFX_LIB_DEBUG} ${BGFX_LIB_RELEASE} ${BX_LIB_DEBUG} ${BX_LIB_RELEASE} ${BIMG_LIB_DEBUG} ${BIMG_LIB_RELEASE})
endif()

if (bgfx_FOUND AND NOT TARGET bgfx::bgfx)
    # warning: the order of libraries is important here
    # bimg must be linked in BEFORE bx otherwise some stuff
    # out of bx will be thrown away because it is only used by
    # bimg
    add_library(bgfx INTERFACE)
    target_link_libraries(bgfx
        INTERFACE
            optimized "${BGFX_LIB_RELEASE}"
            debug "${BGFX_LIB_DEBUG}"
            optimized "${BIMG_LIB_RELEASE}"
            debug "${BIMG_LIB_DEBUG}"
            optimized "${BX_LIB_RELEASE}"
            debug "${BX_LIB_DEBUG}"
    )
    if (UNIX)
        target_link_libraries(bgfx
            INTERFACE
                dl
                pthread
        )
    endif()
    set_target_properties(bgfx
        PROPERTIES INTERFACE_INCLUDE_DIRECTORIES
            "${BGFX_INCLUDE_DIR};${BX_INCLUDE_DIR};${BIMG_INCLUDE_DIR}"
    )
    add_library(bgfx::bgfx ALIAS bgfx)
    
    if(UNIX)
        set(IRRLICHT_DEPENDENCY_LIBRARIES
            ${IRRLICHT_DEPENDENCY_LIBRARIES}
            bgfx::bgfx
           )
    endif()
    
endif()
