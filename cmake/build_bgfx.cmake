if(UNIX)
    message(STATUS "Building Bgfx Dependency")
    set(MAKE_BGFX_CMD "${CMAKE_SOURCE_DIR}/make-bgfx.sh")
    # BGFX uses a GENIE build system so we need to run an external script to build it
    execute_process(COMMAND ${MAKE_BGFX_CMD} WORKING_DIRECTORY ${CMAKE_SOURCE_DIR} RESULT_VARIABLE CMD_ERROR)
    message(STATUS "Creating BGFX Library Result:" ${CMD_ERROR})
endif()
